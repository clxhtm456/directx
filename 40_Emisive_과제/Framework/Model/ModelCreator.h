#pragma once

struct ModelInstance
{
public:
	ModelAnimator* modelAnimator;
	UINT index;
};

class ModelCreator
{
public:
	static ModelCreator* Create(Shader* shader, wstring dir);

	static void Update();
	static void Render();
protected:
	bool Init(Shader* shader, wstring dir);
public :
	void AddAnimation(wstring dir);
	void PlayClip(UINT clip, float speed = 1.0f, float takeTime = 1.0f);

	Transform* GetTransform() { return transform; }
private:
	UINT index;
	Transform* transform;
	ModelAnimator* animator;
private:
	static vector<wstring> modelDirList;
	static vector<ModelInstance*> modelAnimList;
};