#pragma once

class Mesh
{
public:
	typedef VertexTextureNormalTangent MeshVertex;

public:
	Mesh();
	virtual ~Mesh();

	void SetShader(Shader* shader);
	void Pass(UINT val) { pass = val; }

	void Update();
	void Render(UINT drawCount);

protected:
	virtual void Create() = 0;
protected:
	Shader * shader;
	UINT pass = 0;

	PerFrame* perFrame = nullptr;

	VertexBuffer* vertexBuffer = nullptr;
	IndexBuffer* indexBuffer = nullptr;

	MeshVertex* vertices;
	UINT* indices;

	UINT vertexCount, indexCount;
};
