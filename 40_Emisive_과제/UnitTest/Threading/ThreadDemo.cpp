#include "stdafx.h"
#include "ThreadDemo.h"

//#pragma comment(linker,"/entry:WinMainCRTStartup /subsystem:console")//콘솔창 띄우기

void ThreadDemo::Initialize()
{
//	Thread();
	//Thread2();
	//Join();
	//Mutex();

	timer.Start([]()->void
	{
		printf("Timer\n");
	}, 2000, 2);

	timer2.Start([]()->void
	{
		printf("Timer2\n");
	}, 3000);

	Performance performance;
	int arr[10000];

	performance.Start();
	{
		for (int i = 0; i < 10000; i++)
		{
			arr[i] = Math::Random(0, 10000);
		}

		sort(arr, arr + 10000);
	}
	auto time = performance.Stop();

	printf("Running : %f\n", time);

}

void ThreadDemo::Destroy()
{
	
}

void ThreadDemo::Update()
{
	//progress += Time::Delta();
	//ImGui::ProgressBar(progress / 10.0f);
	m.lock();//lock 기능
	progress += 0.01f;
	m.unlock();
	
	
	ImGui::ProgressBar(progress / 1000.0f);
}

void ThreadDemo::Render()
{

}

void ThreadDemo::Thread()
{
	for (int i = 0; i < 100; i++)
	{
		printf("1 : %d\n", i);
	}
	printf("반복문1 종료\n");

	for (int i = 0; i < 100; i++)
	{
		printf("2 : %d\n", i);
	}
	printf("반복문2 종료\n");
}

void ThreadDemo::Thread2()
{
	thread t(CallBack0(ThreadDemo::Thread2_1, this));
	thread t2(CallBack0(ThreadDemo::Thread2_2, this));

	t2.join();
	t.join();
}

void ThreadDemo::Thread2_1()
{
	for (int i = 0; i < 100; i++)
	{
		printf("1 : %d\n", i);
	}
	printf("반복문1 종료\n");
}

void ThreadDemo::Thread2_2()
{
	for (int i = 0; i < 100; i++)
	{
		printf("2 : %d\n", i);
	}
	printf("반복문2 종료\n");
}

void ThreadDemo::Join()
{
	thread t([]()->void
	{
		for (int i = 0; i < 100; i++)
		{
			printf("1 : %d\n", i);
		}
		printf("반복문1 종료\n");
	});

	thread t2([]()->void
	{
		int a = 0;
		while (true)
		{
			a++;
			printf("A : %d\n", a);

			Sleep(100);

			if (a > 30)
				break;
		}
	});

	t2.join();//t2 종료까지 기다림
	printf("Thread2 종료");

	t.join();
	printf("Thread1 종료");
}

void ThreadDemo::Mutex()
{
	thread t([=]()
	{
		while (true)
		{
			Sleep(1000);
			printf("Progress : %f\n", progress);
		}
	}
	);

	t.detach();//t가 끝나던 말던 놓아둠
}


