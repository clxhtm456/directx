
struct LightDesc
{
    float4 Ambient;
    float4 Specular;
    float3 Direction;
    float Padding;
    float3 Position;
};


struct MaterialDesc
{
    float4 Ambient;
    float4 Diffuse;
    float4 Specular;
    float4 Emissive;
};

cbuffer CB_Material
{
    MaterialDesc Material;
};

cbuffer CB_Light
{
    LightDesc GlobalLight;
};

float3 MaterialToColor(MaterialDesc result)
{
    return (result.Ambient + result.Diffuse + result.Specular + result.Emissive).rgb;
}

MaterialDesc MakeMaterial()
{
    MaterialDesc output;
    output.Ambient = float4(0, 0, 0, 0);
    output.Diffuse = float4(0, 0, 0, 0);
    output.Specular = float4(0, 0, 0, 0);
    output.Emissive = float4(0, 0, 0, 0);

    return output;
}

////////////////////////////////////////////////////////////////////////////////////////////

void Texture(inout float4 color, Texture2D t, float2 uv, SamplerState sample)
{
    float4 sampling = t.Sample(sample, uv);

    if (any(sampling.rgb))
    {
        color = color * sampling;
    }
}

void Texture(inout float4 color, Texture2D t, float2 uv)
{
    Texture(color, t, uv,LinearSampler);
}

////////////////////////////////////////////////////////////////////////////////////////////

void ComputeLight(out MaterialDesc output, float3 normal, float3 wPosition)
{
    output.Ambient = 0;
    output.Diffuse = 0;
    output.Specular = 0;

    float3 direction = -GlobalLight.Direction;
    float NdotL = dot(direction, normalize(normal));

    output.Ambient = GlobalLight.Ambient*Material.Ambient;
    float3 E = normalize(ViewPosition() - wPosition);

    [flatten]
    if(NdotL > 0.0f)
    {
        output.Diffuse = NdotL * Material.Diffuse;

        [flatten]
        if (any(Material.Specular.rgb))//rgb중 하나라도 0이 아니라면
        {
            float3 R = normalize(reflect(GlobalLight.Direction, normal));
            
            float RdotE = saturate(dot(R, E));

            float specular = pow(RdotE, Material.Specular.a);
            output.Specular = specular * Material.Specular * GlobalLight.Specular;
        }
    }

    [flatten]
    if (any(Material.Emissive.rgb))
    {
        float NdotE = dot(E, normalize(normal));

        float emissive = smoothstep(1.0f - Material.Emissive.a, 1.0f, 1.0f - saturate(NdotE));

        output.Emissive = Material.Emissive * emissive;

    }

}

void NormalMapping(float2 uv, float3 normal, float3 tangent, SamplerState samp)
{
    float4 map = NormalMap.Sample(samp, uv);

    [flatten]
    if (any(map) == false)
        return;

    float3 N = normalize(normal);
    float3 T = normalize(tangent - dot(tangent, N) * N);
    float3 B = cross(N, T);
    float3x3 TBN = float3x3(T, B, N);

    float3 coord = map.rgb * 2.0f - 1.0f;

    coord = mul(coord, TBN);
    
    Material.Diffuse *= saturate(dot(coord, -GlobalLight.Direction));
}

void NormalMapping(float2 uv, float3 normal, float3 tangent)
{
    NormalMapping(uv, normal, tangent, LinearSampler);

}
