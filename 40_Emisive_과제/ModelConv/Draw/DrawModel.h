#pragma once

#include "Systems/IExecute.h"

class DrawModel : public IExecute
{
public:
	virtual void Initialize() override;
	virtual void Ready() {}
	virtual void Destroy() {}
	virtual void Update()  override;
	virtual void PreRender() {}
	virtual void Render()  override;
	virtual void PostRender() {}
	virtual void ResizeScreen() {}

private:
	void Airplane();
	void Tower();
	void Tank();
	void KachujinModel();
	void Kachujin();
private:
	Shader * shader;
	Shader* animationShader;

	ModelRender* airPlane = NULL;
	ModelRender* tower = NULL;
	ModelRender* tank = NULL;

	ModelRender* kachujinModel = nullptr;
	ModelAnimator* kachujin = nullptr;
};