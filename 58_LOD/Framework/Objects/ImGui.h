#pragma once

class ImGui_F
{
public:


	static void Begin_InputInt(const char* title, const char* title2, int Int, int step = 1, int stepfast = 100);
	static void Begin_InputInt3(const char* title, const char* title2, int Int[][3],UINT count);

	static void Begin_InputVector3(const char* title, const char* title2, Vector3 vector[], UINT count);
	static void Begin_InputFloat(const char* title, const char* title2, float Float);
	static void Begin_InputFloat3(const char* title, const char* title2, float Float[3]);
	static void Begin_SliderFloat(const char* title, const char* title2, float Float, float min, float max);
	static void Begin_SliderFloat3(const char* title, const char* title2, float Float[3], float min, float max);
	static void End();
	static void Run();

	static void Begin_InpugInttest(const char** Title, int* INT,UINT& count);


	static UINT Inputint(const char* title, int Int, UINT count);

	UINT count;
	const char* title[10];
	int Int[];
};

