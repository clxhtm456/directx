#include "Framework.h"
#include "ImGui.h"

void ImGui_F::Begin_InputInt(const char* title, const char* title2, int Int, int step, int stepfast)
{
	ImGui::Begin(title);
	ImGui::InputInt(title2, &Int, step, stepfast);
	ImGui::End();
}

void ImGui_F::Begin_InputInt3(const char* title, const char* title2, int Int[][3], UINT count)
{
	ImGui::Begin(title);
	for (int i = 0; i < count; i++)
	{
		ImGui::InputInt3(title2, Int[i]);
	}
	ImGui::End();
}

void ImGui_F::Begin_InputVector3(const char* title, const char* title2, Vector3 vector[], UINT count)
{
	ImGui::Begin(title);
	for (int i = 0; i < count; i++)
	{
		int temp[3];
		temp[0] = vector[i].x;
		temp[1] = vector[i].y;
		temp[2] = vector[i].z;

		ImGui::InputInt3(title2, temp);
	}
	ImGui::End();
}

void ImGui_F::Begin_InputFloat(const char* title, const char* title2, float Float)
{
	ImGui::InputFloat(title2, &Float);
}

void ImGui_F::Begin_InputFloat3(const char* title, const char* title2, float Float[3])
{
}

void ImGui_F::Begin_SliderFloat(const char* title, const char* title2, float Float, float min, float max)
{
}

void ImGui_F::Begin_SliderFloat3(const char* title, const char* title2, float Float[3], float min, float max)
{
}

void ImGui_F::End()
{
}

void ImGui_F::Run()
{
	ImGuiIO& io = ImGui::GetIO();
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
	io.ConfigWindowsResizeFromEdges = true;
	ImGui_ImplWin32_Init(D3D::GetHandle());
	ImGui_ImplDX11_Init(D3D::GetDevice(), D3D::GetDC());
}

void ImGui_F::Begin_InpugInttest(const char** Title, int* INT, UINT& count)
{
	ImGui::Begin("Test");
	for (size_t i = 0; i < count; i++)
	{
	/*	Title[i] = temp->title[i];
		INT[i] = temp->Int[i];*/

		ImGui::InputInt(Title[i], &INT[i]);
	}
	ImGui::End();

}

UINT ImGui_F::Inputint(const char * title, int Int, UINT count)
{
	ImGui::InputInt(title, &Int);

	return count;
}
