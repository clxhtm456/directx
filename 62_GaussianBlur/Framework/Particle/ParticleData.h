#pragma once

#include "Framework.h"

class ParticleData
{
public:
	enum class BlendType
	{
		Opaque,
		Additive,
		AlphaBlend
	};
	
	BlendType Type = BlendType::Opaque;

	bool bLoop = false;

	wstring textureFile = L"";

	UINT maxParticles = 100;

	float readyTime = 1.0f;
	float readyRandomTime = 0.0f;
	float startVelocity = 1.0f;
	float endVelocity = 1.0f;

	float minHorizontalVelocity = 0.0f;
	float maxHorizontalVelocity = 0.0f;

	float minVerticalVelocity = 0.0f;
	float maxVerticalVelocity = 0.0f;

	Vector3 gravity = Vector3(0, 0, 0);
	Color minColor = Color(1, 1, 1, 1);
	Color maxColor = Color(1, 1, 1, 1);

	float minRotateSpeed = 0;
	float maxRotateSpeed = 0;

	float minStartSize = 100;
	float maxStartSize = 100;

	float minEndSize = 100;
	float maxEndSize = 100;
};