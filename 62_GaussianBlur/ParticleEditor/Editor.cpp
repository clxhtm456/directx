#include "stdafx.h"
#include "Editor.h"
#include "Environment/Sky/Sky.h"
#include "Utilities/Xml.h"

void Editor::Initialize()
{
	Context::Get()->GetCamera()->RotationDegree(20, 0, 0);
	Context::Get()->GetCamera()->Position(1, 25, -50);

	shader = new Shader(L"57_ParticleViewer.fxo");
	shadow = new Shadow(shader, Vector3(0, 0, 0), 65);

	sky = new Sky(shader);
	sky->ScatteringPass(3);
	sky->RealTime(false, Math::PI - 1e-6f, 0.5f);

	particleSystem = new ParticleSystem(L"Explosion");
	particleSystem->Pass(0);

	Mesh();

	UpdateParticleList();
	UpdateTextureList();
}

void Editor::Destroy()
{
	delete shader;
	delete shadow;

	delete sky;

	delete floor;
	delete stone;

	delete sphere;
	delete grid;

	delete particleSystem;
}

void Editor::Update()
{
	OnGui();
	sky->Update();
	grid->Update();
	sphere->Update();

	Vector3 p;
	sphere->GetTransform(0)->Position(&p);
	float moveSpeed = 20.0f;

	if (Mouse::Get()->Press(1) == false)
	{
		const Vector3& F =Context::Get()->GetCamera()->Foward();
		const Vector3& R = Context::Get()->GetCamera()->Right();
		const Vector3& U = Context::Get()->GetCamera()->Up();

		Vector3 tempForward = F;
		tempForward.y = 0;
		D3DXVec3Normalize(&tempForward, &tempForward);
		if (Keyboard::Get()->Press('W'))
			p += tempForward * moveSpeed*Time::Delta();
		else if (Keyboard::Get()->Press('S'))
			p -= tempForward * moveSpeed*Time::Delta();

		if (Keyboard::Get()->Press('A'))
			p -= R * moveSpeed*Time::Delta();
		else if (Keyboard::Get()->Press('D'))
			p += R * moveSpeed*Time::Delta();

	}

	sphere->GetTransform(0)->Position(p);
	sphere->UpdateTransforms();

	if (particleSystem != NULL)
	{
		particleSystem->GetTransform()->Position(p);
		particleSystem->Update();
	}
}

void Editor::PreRender()
{
	sky->PreRender();

	//Depth
	{
		shadow->Set();
		Pass(0);
		sphere->Render();

	}
}

void Editor::Render()
{
	sky->Pass(4, 5, 6);
	sky->Render();

	Pass(7);
	stone->Render();
	sphere->Render();

	floor->Render();

	grid->Render();

	if(particleSystem != NULL)
		particleSystem->Render();

}

void Editor::Mesh()
{
	//CreateMaterial
	floor = new Material(shader);
	floor->DiffuseMap("Floor.png");
	floor->SpecularMap("Floor_Specular.png");
	floor->NormalMap("Floor_Normal.png");
	floor->Specular(1, 1, 1, 20);
	floor->Emissive(0.3f, 0.3f, 0.3f, 0.3f);

	stone = new Material(shader);
	stone->DiffuseMap("Bricks.png");
	stone->SpecularMap("Bricks_Specular.png");
	stone->NormalMap("Bricks_Normal.png");
	stone->Specular(1, 0, 0, 20);
	stone->Emissive(0.3f, 0.3f, 0.3f, 0.3f);

	//CreateMesh
	Transform* transform = NULL;

	grid = new MeshRender(shader, new MeshGrid(10, 10));
	transform = grid->AddTransform();
	transform->Position(0, 0, 0);
	transform->Scale(20, 1, 20);

	sphere = new MeshRender(shader, new MeshSphere(0.5f, 20, 20));
	transform = sphere->AddTransform();
	transform->Position(0, 5, 0);
	transform->Scale(5, 5,5);

	sphere->UpdateTransforms();
	grid->UpdateTransforms();

	meshes.push_back(sphere);
	meshes.push_back(grid);
}

void Editor::Pass(UINT meshPass)
{
	for (MeshRender* temp : meshes)
		temp->Pass(meshPass);
}

void Editor::UpdateParticleList()
{
	particleList.clear();
	Path::GetFiles(&particleList, L"../../_Textures/Particles/", L"*.xml",false);

	for (wstring& file : particleList)
	{
		file = Path::GetFileNameWithoutExtension(file);
	}
}

void Editor::UpdateTextureList()
{
	textureList.clear();

	vector<wstring> files;
	Path::GetFiles(&files, L"../../_Textures/Particles/", L"*", false);

	for (wstring file : files)
	{
		wstring ext = Path::GetExtension(file);
		transform(ext.begin(), ext.end(), ext.begin(), toupper);//�빮�ڷ� �ٲ���

		file = Path::GetFileName(file);
		if (ext == L"PNG" || ext == L"JPG" || ext == L"TGA")
			textureList.push_back(file);
	}
}

void Editor::OnGui()
{
	float width = D3D::Width();
	float height = D3D::Height();

	//ImGuiWindowFlags windowFlags =
	//	ImGuiWindowFlags_MenuBar |
	//	ImGuiWindowFlags_NoDocking |
	//	ImGuiWindowFlags_NoTitleBar |
	//	ImGuiWindowFlags_NoCollapse |
	//	//ImGuiWindowFlags_NoResize |
	//	ImGuiWindowFlags_NoBringToFrontOnFocus |
	//	ImGuiWindowFlags_NoNavFocus;

	bool bOpen = true;
	bOpen = ImGui::Begin("Particle",&bOpen);
	//ImGui::SetWindowPos(ImVec2(width - windowWidth, 0));
	ImGui::SetWindowSize(ImVec2(windowWidth, height));
	{
		OnGui_List();
		OnGui_Settings();
	}
	ImGui::End();
}

void Editor::OnGui_List()
{
	if (ImGui::CollapsingHeader("Particle List",ImGuiTreeNodeFlags_DefaultOpen))
	{
		for (UINT i = 0; i < particleList.size(); i++)
		{
			if (ImGui::Button(String::ToString(particleList[i]).c_str(), ImVec2(200, 0)))
			{
				SafeDelete(particleSystem);

				file = particleList[i];
				particleSystem = new ParticleSystem(particleList[i]);

				bLoop = particleSystem->GetData().bLoop;
				maxParticle = particleSystem->GetData().maxParticles;
			}
		}
	}
}

void Editor::OnGui_Settings()
{
	if (particleSystem == NULL)
		return;

	ImGui::Spacing();

	if (ImGui::CollapsingHeader("Particle Settings", ImGuiTreeNodeFlags_DefaultOpen))
	{
		ImGui::Separator();

		ImGui::SliderInt("Max Particles", (int*)&maxParticle, 1, 1000);
		ImGui::Checkbox("Loop", &bLoop);

		if (ImGui::Button("Apply"))
		{
			particleSystem->GetData().bLoop = bLoop;
			particleSystem->GetData().maxParticles = maxParticle;
			particleSystem->Reset();
		}

		ImGui::Separator();

		const char* types[] = { "Opaque","Additive", "AlphaBlend" };
		ImGui::Combo("BlendType", (int*)&particleSystem->GetData().Type,types,3);

		ImGui::SliderFloat("ReadyTime", &particleSystem->GetData().readyTime, 0.1f, 10.0f);
		ImGui::SliderFloat("ReadyRandomTime", &particleSystem->GetData().readyRandomTime, 0.1f, 10.0f);

		ImGui::SliderFloat("StartVelocity", &particleSystem->GetData().startVelocity, 0.0f, 10.0f);
		ImGui::SliderFloat("EndVelocity", &particleSystem->GetData().endVelocity, -100.0f, 100.0f);

		ImGui::SliderFloat("MinHorizontal", &particleSystem->GetData().minHorizontalVelocity, -100.0f, 100.0f);
		ImGui::SliderFloat("MaxHorizontal", &particleSystem->GetData().maxHorizontalVelocity, -100.0f, 100.0f);

		ImGui::SliderFloat("MinVertical", &particleSystem->GetData().minVerticalVelocity, -100.0f, 100.0f);
		ImGui::SliderFloat("MaxVertical", &particleSystem->GetData().maxVerticalVelocity, -100.0f, 100.0f);

		ImGui::SliderFloat3("Gravity", particleSystem->GetData().gravity, -100.0f, 100.0f);

		ImGui::ColorEdit4("MinColor", particleSystem->GetData().minColor);
		ImGui::ColorEdit4("MaxColor", particleSystem->GetData().maxColor);

		ImGui::SliderFloat("MinRotateSpeed", &particleSystem->GetData().minRotateSpeed, -10.0f, 10.0f);
		ImGui::SliderFloat("MaxRotateSpeed", &particleSystem->GetData().maxRotateSpeed, -10.0f, 10.0f);

		ImGui::SliderFloat("MinStartSize", &particleSystem->GetData().minStartSize, 0.0f, 500.0f);
		ImGui::SliderFloat("MaxStartSize", &particleSystem->GetData().maxStartSize, 0.0f, 500.0f);

		ImGui::SliderFloat("MinEndSize", &particleSystem->GetData().minEndSize, 0.0f, 500.0f);
		ImGui::SliderFloat("MaxEndSize", &particleSystem->GetData().maxEndSize, 0.0f, 500.0f);

		ImGui::Spacing();
		OnGui_Write();

		ImGui::Spacing();
		ImGui::Separator();

		if (ImGui::CollapsingHeader("TextureList", ImGuiTreeNodeFlags_DefaultOpen))
		{
			for (wstring textureFile : textureList)
			{
				if (ImGui::Button(String::ToString(textureFile).c_str(), ImVec2(200, 0)))
				{
					particleSystem->GetData().textureFile = textureFile;
					particleSystem->SetTexture(L"Particles/" + textureFile);
				}
			}
		}
	}
}

void Editor::OnGui_Write()
{
	ImGui::Separator();

	if (ImGui::Button("SaveParticle"))
	{
		D3DDesc desc = D3D::GetDesc();

		Path::SaveFileDialog
		(
			file,
			L"Particle file\0*.xml",
			L"../../_Textures/Particles",
			bind(&Editor::WriteFile, this, placeholders::_1),
			desc.Handle
		);
	}
}

void Editor::WriteFile(wstring file)
{
	Xml::XMLDocument* document = new Xml::XMLDocument();

	Xml::XMLDeclaration* decl = document->NewDeclaration();
	document->LinkEndChild(decl);

	Xml::XMLElement* root = document->NewElement("Particle");
	root->SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
	root->SetAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
	document->LinkEndChild(root);


	Xml::XMLElement* node = NULL;

	node = document->NewElement("BlendState");
	node->SetText((int)particleSystem->GetData().Type);
	root->LinkEndChild(node);


	string textureFile = String::ToString(particleSystem->GetData().textureFile);
	String::Replace(&textureFile, "Particles/", "");

	node = document->NewElement("Loop");
	node->SetText(particleSystem->GetData().bLoop);
	root->LinkEndChild(node);

	node = document->NewElement("TextureFile");
	node->SetText(textureFile.c_str());
	root->LinkEndChild(node);


	node = document->NewElement("MaxParticles");
	node->SetText(particleSystem->GetData().maxParticles);
	root->LinkEndChild(node);


	node = document->NewElement("ReadyTime");
	node->SetText(particleSystem->GetData().readyTime);
	root->LinkEndChild(node);

	node = document->NewElement("ReadyRandomTime");
	node->SetText(particleSystem->GetData().readyRandomTime);
	root->LinkEndChild(node);

	node = document->NewElement("StartVelocity");
	node->SetText(particleSystem->GetData().startVelocity);
	root->LinkEndChild(node);

	node = document->NewElement("EndVelocity");
	node->SetText(particleSystem->GetData().endVelocity);
	root->LinkEndChild(node);


	node = document->NewElement("MinHorizontalVelocity");
	node->SetText(particleSystem->GetData().minHorizontalVelocity);
	root->LinkEndChild(node);

	node = document->NewElement("MaxHorizontalVelocity");
	node->SetText(particleSystem->GetData().maxHorizontalVelocity);
	root->LinkEndChild(node);

	node = document->NewElement("MinVerticalVelocity");
	node->SetText(particleSystem->GetData().minVerticalVelocity);
	root->LinkEndChild(node);

	node = document->NewElement("MaxVerticalVelocity");
	node->SetText(particleSystem->GetData().maxVerticalVelocity);
	root->LinkEndChild(node);


	node = document->NewElement("Gravity");
	node->SetAttribute("X", particleSystem->GetData().gravity.x);
	node->SetAttribute("Y", particleSystem->GetData().gravity.y);
	node->SetAttribute("Z", particleSystem->GetData().gravity.z);
	root->LinkEndChild(node);


	node = document->NewElement("MinColor");
	node->SetAttribute("R", particleSystem->GetData().minColor.r);
	node->SetAttribute("G", particleSystem->GetData().minColor.g);
	node->SetAttribute("B", particleSystem->GetData().minColor.b);
	node->SetAttribute("A", particleSystem->GetData().minColor.a);
	root->LinkEndChild(node);

	node = document->NewElement("MaxColor");
	node->SetAttribute("R", particleSystem->GetData().maxColor.r);
	node->SetAttribute("G", particleSystem->GetData().maxColor.g);
	node->SetAttribute("B", particleSystem->GetData().maxColor.b);
	node->SetAttribute("A", particleSystem->GetData().maxColor.a);
	root->LinkEndChild(node);


	node = document->NewElement("MinRotateSpeed");
	node->SetText(particleSystem->GetData().minRotateSpeed);
	root->LinkEndChild(node);

	node = document->NewElement("MaxRotateSpeed");
	node->SetText(particleSystem->GetData().maxRotateSpeed);
	root->LinkEndChild(node);

	node = document->NewElement("MinStartSize");
	node->SetText((int)particleSystem->GetData().minStartSize);
	root->LinkEndChild(node);

	node = document->NewElement("MaxStartSize");
	node->SetText((int)particleSystem->GetData().maxStartSize);
	root->LinkEndChild(node);

	node = document->NewElement("MinEndSize");
	node->SetText((int)particleSystem->GetData().minEndSize);
	root->LinkEndChild(node);

	node = document->NewElement("MaxEndSize");
	node->SetText((int)particleSystem->GetData().maxEndSize);
	root->LinkEndChild(node);

	wstring folder = Path::GetDirectoryName(file);
	wstring fileName = Path::GetFileNameWithoutExtension(file);

	document->SaveFile(String::ToString(folder + fileName + L".xml").c_str());
	SafeDelete(document);

	UpdateParticleList();
}


