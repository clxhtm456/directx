#include "Framework.h"
#include "ParticleSystem.h"
#include "Utilities/Xml.h"


ParticleSystem::ParticleSystem(wstring file) : 
	Renderer(L"58_Particle.fxo")
{
	ReadFile(L"../../_Textures/Particles/" + file + L".xml");

	buffer = new ConstantBuffer(&desc, sizeof(Desc));
	sBuffer = shader->AsConstantBuffer("CB_Particle");

	sMap = shader->AsSRV("ParticleMap");

	Reset();
}

ParticleSystem::~ParticleSystem()
{
	delete buffer;
	delete map;

	SafeDeleteArray(vertices);
	SafeDeleteArray(indices);

	SafeDelete(vertexBuffer);
	SafeDelete(indexBuffer);
}

void ParticleSystem::Reset()
{
	currentTime = 0.0f;
	lastAddTime = Time::Get()->Running();
	leadCount = gpuCount = activeCount = deactiveCount = 0;

	SafeDeleteArray(vertices);
	SafeDeleteArray(indices);

	SafeDelete(vertexBuffer);
	SafeDelete(indexBuffer);

	vertices = new VertexParticle[data.maxParticles * 4];
	for (UINT i = 0; i < data.maxParticles; i++)
	{
		vertices[i * 4 + 0].Corner = Vector2(-1, -1);
		vertices[i * 4 + 1].Corner = Vector2(-1, +1);
		vertices[i * 4 + 2].Corner = Vector2(+1, -1);
		vertices[i * 4 + 3].Corner = Vector2(+1, +1);
	}

	indices = new UINT[data.maxParticles * 6];
	for (UINT i = 0; i < data.maxParticles; i++)
	{
		indices[i * 6 + 0] = i*4+0;
		indices[i * 6 + 1] = i*4+1;
		indices[i * 6 + 2] = i*4+2;
		indices[i * 6 + 3] = i*4+2;
		indices[i * 6 + 4] = i*4+1;
		indices[i * 6 + 5] = i*4+3;
	}

	vertexBuffer = new VertexBuffer(vertices, data.maxParticles * 4, sizeof(VertexParticle), 0, true);
	indexBuffer = new IndexBuffer(indices, data.maxParticles * 6);
}

void ParticleSystem::Add(Vector3 position)
{
	if (Time::Get()->Running() - lastAddTime < 60.0f / 1000.0f)
		return;

	lastAddTime = Time::Get()->Running();

	UINT next = leadCount + 1;
	if (next >= data.maxParticles)
	{
		if (data.bLoop == true)
		{
			next = 0;
		}
		else
		{
			next = data.maxParticles;
			return;
		}
	}
	if (next == deactiveCount)
		return;

	Vector3 velocity = Vector3(1, 1, 1);
	velocity *= data.startVelocity;

	float horizontalVelocity = Math::Lerp(data.minHorizontalVelocity, data.maxHorizontalVelocity,
		Math::Random(0.0f, 1.0f));

	float horizontalAngle = Math::PI * 2.0f * Math::Random(0.0f, 1.0f);

	velocity.x += horizontalVelocity * cos(horizontalAngle);
	velocity.y += horizontalVelocity * sin(horizontalAngle);
	velocity.z += Math::Lerp(data.minVerticalVelocity, data.maxVerticalVelocity, Math::Random(0.0f, 1.0f));

	Vector4 random = Math::RandomVec4(0.0f, 1.0f);

	for (UINT i = 0; i < 4; i++)
	{
		int index = leadCount * 4 + i;

		vertices[index].Position = position;
		vertices[index].Velocity = velocity;
		vertices[index].Random = random;
		vertices[index].Time = currentTime;
	}

	leadCount = next;
}

void ParticleSystem::ReadFile(wstring file)
{
	Xml::XMLDocument* document = new Xml::XMLDocument();
	Xml::XMLError error = document->LoadFile(String::ToString(file).c_str());
	assert(error == Xml::XML_SUCCESS);

	Xml::XMLElement* root = document->FirstChildElement();

	Xml::XMLElement* node = root->FirstChildElement();
	data.Type = (ParticleData::BlendType)node->IntText();//<BlendState>

	node = node->NextSiblingElement();
	data.bLoop = node->BoolText();//<Loop>

	node = node->NextSiblingElement();
	wstring textureFile = String::ToWString(node->GetText());
	data.textureFile = L"Particles/" + textureFile;//<TextureFile>
	map = new Texture(data.textureFile);

	node = node->NextSiblingElement();
	data.maxParticles = node->IntText();//<MaxParticles>

	node = node->NextSiblingElement();
	data.readyTime = node->FloatText();//<ReadyTime>

	node = node->NextSiblingElement();
	data.readyRandomTime = node->FloatText();

	node = node->NextSiblingElement();
	data.startVelocity = node->FloatText();

	node = node->NextSiblingElement();
	data.endVelocity = node->FloatText();

	node = node->NextSiblingElement();
	data.minHorizontalVelocity = node->FloatText();

	node = node->NextSiblingElement();
	data.maxHorizontalVelocity = node->FloatText();

	node = node->NextSiblingElement();
	data.minVerticalVelocity = node->FloatText();

	node = node->NextSiblingElement();
	data.maxVerticalVelocity = node->FloatText();

	node = node->NextSiblingElement();
	data.gravity.x = node->FloatAttribute("X");
	data.gravity.y = node->FloatAttribute("Y");
	data.gravity.z = node->FloatAttribute("Z");

	node = node->NextSiblingElement();
	data.minColor.r = node->FloatAttribute("R");
	data.minColor.g = node->FloatAttribute("G");
	data.minColor.b = node->FloatAttribute("B");
	data.minColor.a = node->FloatAttribute("A");

	node = node->NextSiblingElement();
	data.maxColor.r = node->FloatAttribute("R");
	data.maxColor.g = node->FloatAttribute("G");
	data.maxColor.b = node->FloatAttribute("B");
	data.maxColor.a = node->FloatAttribute("A");

	node = node->NextSiblingElement();
	data.minRotateSpeed = node->FloatText();

	node = node->NextSiblingElement();
	data.maxRotateSpeed = node->FloatText();

	node = node->NextSiblingElement();
	data.minStartSize = node->FloatText();

	node = node->NextSiblingElement();
	data.maxStartSize = node->FloatText();

	node = node->NextSiblingElement();
	data.minEndSize = node->FloatText();

	node = node->NextSiblingElement();
	data.maxEndSize = node->FloatText();

	delete document;
}

void ParticleSystem::Update()
{
	Vector3 pos;
	transform->Position(&pos);
	Add(pos);

	Super::Update();

	currentTime += Time::Delta();

	MapVertices();
	Activate();
	Deactivate();

	if (activeCount == leadCount)
		currentTime = 0.0f;

	desc.MinColor = data.minColor;
	desc.MaxColor = data.maxColor;
	desc.Gravity = data.gravity;
	desc.EndVelocity = data.endVelocity;
	desc.RotateSpeed = Vector2(data.minRotateSpeed, data.maxRotateSpeed);
	desc.StartSize = Vector2(data.minStartSize, data.maxStartSize);
	desc.EndSize = Vector2(data.minEndSize, data.maxEndSize);
	desc.ReadyTime = data.readyTime;
	desc.ReadyRandomTime = data.readyRandomTime;
}

void ParticleSystem::Render()
{
	Super::Render();

	desc.CurrentTime = currentTime;

	buffer->Apply();
	sBuffer->SetConstantBuffer(buffer->Buffer());

	sMap->SetResource(map->SRV());

	UINT pass = (UINT)data.Type;
	if (activeCount != leadCount)
	{
		if (leadCount > activeCount)
		{
			shader->DrawIndexed(0, pass, (leadCount - activeCount) * 6, activeCount * 6);
		}
		else
		{
			shader->DrawIndexed(0, pass, (data.maxParticles - activeCount) * 6, activeCount * 6);

			if (leadCount > 0)
				shader->DrawIndexed(0, pass, leadCount * 6);
		}
	}
}

void ParticleSystem::MapVertices()
{
	if (gpuCount == leadCount)
		return;
	D3D11_MAPPED_SUBRESOURCE subResource;

	D3D::GetDC()->Map(vertexBuffer->Buffer(), 0, D3D11_MAP_WRITE_NO_OVERWRITE, 0, &subResource);
	{
		UINT start;
		UINT size;
		UINT offset;
		if (leadCount > gpuCount)
		{
			start = gpuCount * 4;
			size = (leadCount - gpuCount) * sizeof(VertexParticle) * 4;
			offset = gpuCount * sizeof(VertexParticle) * 4;

			BYTE* p = (BYTE*)subResource.pData + offset;

			memcpy(p, vertices + start, size);
		}
		else
		{
			start = gpuCount * 4;
			size = (data.maxParticles - gpuCount) * sizeof(VertexParticle) * 4;
			offset = gpuCount * sizeof(VertexParticle) * 4;

			BYTE* p = (BYTE*)subResource.pData + offset;

			memcpy(p, vertices + start, size);

			if (leadCount > 0)
			{
				UINT size2 = leadCount * sizeof(VertexParticle) * 4;

				memcpy(subResource.pData, vertices, size2);
			}
		}
	}
	D3D::GetDC()->Unmap(vertexBuffer->Buffer(), 0);

	gpuCount = leadCount;
}

void ParticleSystem::Activate()
{
	while (activeCount != gpuCount)
	{
		float age = currentTime - vertices[activeCount * 4].Time;

		if (age < data.readyTime)
		{
			return;
		}

		vertices[activeCount * 4].Time = currentTime;
		activeCount++;

		if (activeCount >= data.maxParticles)
		{
			activeCount = (data.bLoop == true ? 0 : data.maxParticles);
		}
	}
}

void ParticleSystem::Deactivate()
{
	while (deactiveCount != activeCount)
	{
		float age = currentTime - vertices[deactiveCount * 4].Time;

		if (age > data.readyTime)
		{
			return;
		}
		deactiveCount++;

		if (deactiveCount >= data.maxParticles)
		{
			deactiveCount = (data.bLoop == true ? 0 : data.maxParticles);
		}
	}
}
