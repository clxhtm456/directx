#include "stdafx.h"
#include "TerrainLodDemo.h"
#include "Environment/Terrain/TerrainLod.h"

void TerrainLodDemo::Initialize()
{
	Context::Get()->GetCamera()->Position(0, 155, -210);
	Context::Get()->GetCamera()->RotationDegree(41, 0, 0);
	dynamic_cast<Freedom*>(Context::Get()->GetCamera())->Speed(50, 5);

	shader = new Shader(L"55_LoD.fxo");

	//Terrain
	{
		TerrainLod::InitialDesc desc =
		{
			L"Terrain/Gray512.png",
			1.0f,
			16,
			10
		};

		terrain = new TerrainLod(shader, desc);
		terrain->BaseMap(L"Terrain/Dirt.png");
		terrain->LayerMap(L"Terrain/Dirt3.png", L"Terrain/Gray512.png");
		terrain->NormalMap(L"Terrain/Dirt_Normal.png");
		//terrain->Pass(1);
	}
}

void TerrainLodDemo::Destroy()
{
	SafeDelete(shader);
	SafeDelete(terrain);
}

void TerrainLodDemo::Update()
{
	terrain->Update();
}

void TerrainLodDemo::Render()
{
	//terrain->Pass(1);
	terrain->Render();

}


