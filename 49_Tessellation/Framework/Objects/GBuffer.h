#pragma once

class GBuffer
{
public:
	GBuffer(Shader* shader, UINT width = 0, UINT height = 0);
	~GBuffer();

	void PreRender();
	void Render(bool bVisibleRender2D = false);
private:
	struct Desc
	{
		Vector4 Perspective;
		Vector2 PowerRange = Vector2(0.1f, 100.0f);
		float Padding[2];

	}desc;
private:
	Shader * shader;
	UINT width, height;

	RenderTarget* diffuseRTV;
	RenderTarget* specularRTV;
	RenderTarget* emissiveRTV;
	RenderTarget* normalRTV;
	RenderTarget* tangentRTV;
	DepthStencil* depthStencil;

	Render2D* debug2D[6];

	Viewport* viewport;

	ConstantBuffer* buffer;
	ID3DX11EffectConstantBuffer* sBuffer;


	VertexBuffer* vertexBuffer;

	ID3DX11EffectShaderResourceVariable* sSrvs;
};