#include "00_Global.fx"
#include "00_Light.fx"
#include "00_Model.fx"

Texture2D DefferedMaps[6];

struct DefferedDesc
{
    //Specular
    float4 Perspective;
    float2 SpecularPowerRange;
};

cbuffer CB_Deffered
{
    DefferedDesc Deffered;
};

struct PixelOutput_Deffered
{
    float4 Diffuse : SV_Target0;
    float4 Specular : SV_Target1;
    float4 Emissive : SV_Target2;
    float4 Normal : SV_Target3;
    float4 Tangent : SV_Target4;
};


PixelOutput_Deffered PackGBuffer(float3 diffuse, float4 specular, float4 emissive ,float3 normal, float3 tangent)
{
    PixelOutput_Deffered output;
    
    output.Diffuse = float4(diffuse.rgb, 1);
    output.Specular = specular;
    output.Specular.a = max(1e-6, (specular.a - Deffered.SpecularPowerRange.x) / Deffered.SpecularPowerRange.y);
    output.Emissive = emissive;
    output.Normal = float4(normal, 1); //-1~1사이 값을 다시 0~1사이 값으로 변환
    output.Tangent = float4(tangent, 1);

    return output;
}

PixelOutput_Deffered PS_Deffered_PreRender(MeshOutput input)
{
   // NormalMapping(input.Uv, input.Normal, input.Tangent);
    Texture(Material.Diffuse, DiffuseMap, input.Uv);
    Texture(Material.Specular, DiffuseMap, input.Uv);

    PixelOutput_Deffered output;

    //output.Color = Material.Diffuse;
    //output.Normal = float4(normalize(input.Normal),1);
    //output.Specular = Material.Specular;

    return PackGBuffer(Material.Diffuse.rgb, Material.Specular, Material.Emissive, normalize(input.Normal), normalize(input.Tangent));

   // return output;
}

struct VertexOutput_Deffered
{
    float4 Position : SV_Position;
    float2 Screen : Position1;
};


static const float2 arr[4] ={ float2(-1, +1), float2(+1, +1),  float2(-1, -1), float2(+1, -1)};

VertexOutput_Deffered VS_Deffered(uint VertexID : SV_VertexID)
{
    VertexOutput_Deffered output;
    
    output.Position = float4(arr[VertexID].xy, 0, 1);
    output.Screen = output.Position.xy;

    return output;
}

float4 PS_Deffered(VertexOutput_Deffered input) : SV_Target0
{
    float depth = DefferedMaps[0].Load(int3(input.Position.xy,0)).r;
    float linearDepth = Deffered.Perspective.z / (depth + Deffered.Perspective.w);

    MaterialDesc material;
    material.Ambient = float4(0, 0, 0, 1);
    material.Diffuse = DefferedMaps[1].Load(int3(input.Position.xy, 0));
    material.Specular = DefferedMaps[2].Load(int3(input.Position.xy, 0));
    material.Specular.a = Deffered.SpecularPowerRange.x + Deffered.SpecularPowerRange.y * material.Specular.a;
    material.Emissive = DefferedMaps[3].Load(int3(input.Position.xy, 0));
    float3 normal = DefferedMaps[4].Load(int3(input.Position.xy, 0)).rgb;
    float3 tangent = DefferedMaps[5].Load(int3(input.Position.xy, 0)).rgb;

    float4 position;
    position.xy = input.Screen * Deffered.Perspective.xy * linearDepth;
    position.z = linearDepth;
    position.w = 1.0f;
    position = mul(position, ViewInverse);

    //float4 color = material.Diffuse * dot(-GlobalLight.Direction, normalize(normal));//램버트 조명공식

    MaterialDesc result = MakeMaterial();
    ComputeLight(result, material, normal, position.xyz);
    float3 color = MaterialToColor(result);

    
    return float4(color,1);

}


//PixelOutput_Deffered PS_DefferedSky(MeshOutput input)
//{
//    float4 color = PS_Sky(input);

//    return PackBuffer(color.rgb, normalize(input.Normal));

//}



technique11 T0
{
    //Sky
    P_RS_DSS_VP(P0,FrontCounterClockwise_True,DepthEnable_False,VS_Mesh,PS_Sky)

    //Deffered
    P_VP(P1, VS_Mesh, PS_Deffered_PreRender)
    P_VP(P2, VS_Model, PS_Deffered_PreRender)
    P_VP(P3, VS_Animation, PS_Deffered_PreRender)

    //Deffered-Render
    P_VP(P4, VS_Deffered, PS_Deffered)
}