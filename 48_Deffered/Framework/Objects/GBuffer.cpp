#include "Framework.h"
#include "GBuffer.h"

GBuffer::GBuffer(Shader * shader, UINT width, UINT height):
	shader(shader)
{
	this->width = width < 1 ? (UINT)D3D::Width() : width;
	this->height = height < 1 ? (UINT)D3D::Height() : height;

	diffuseRTV = new RenderTarget(this->width, this->height,DXGI_FORMAT_R8G8B8A8_UNORM);
	specularRTV = new RenderTarget(this->width, this->height, DXGI_FORMAT_R8G8B8A8_UNORM);
	emissiveRTV= new RenderTarget(this->width, this->height, DXGI_FORMAT_R8G8B8A8_UNORM);
	normalRTV = new RenderTarget(this->width, this->height, DXGI_FORMAT_R32G32B32A32_FLOAT);
	tangentRTV = new RenderTarget(this->width, this->height, DXGI_FORMAT_R32G32B32A32_FLOAT);
	depthStencil = new DepthStencil(this->width, this->height, true);


	viewport = new Viewport((float)this->width, (float)this->height);

	buffer = new ConstantBuffer(&desc, sizeof(Desc));
	sBuffer = shader->AsConstantBuffer("CB_Deffered");
	sSrvs = shader->AsSRV("DefferedMaps");

	VertexTexture vertices[6] = 
	{
		VertexTexture(Vector3(-1,-1,0),Vector2(0,1)),
		VertexTexture(Vector3(-1,+1,0),Vector2(0,0)),
		VertexTexture(Vector3(+1,-1,0),Vector2(1,1)),
		VertexTexture(Vector3(+1,-1,0),Vector2(1,1)),
		VertexTexture(Vector3(-1,+1,0),Vector2(0,0)),
		VertexTexture(Vector3(+1,+1,0),Vector2(1,0)),
	};
	vertexBuffer = new VertexBuffer(vertices, 6, sizeof(VertexTexture));

	for (UINT i = 0; i < 6; i++)
	{
		debug2D[i] = new Render2D();
		debug2D[i]->GetTransform()->Position(100 + i * 200, 100, 0);
		debug2D[i]->GetTransform()->Scale(200, 200, 1);
	}

	debug2D[0]->SRV(diffuseRTV->SRV());
	debug2D[1]->SRV(specularRTV->SRV());
	debug2D[2]->SRV(emissiveRTV->SRV());
	debug2D[3]->SRV(normalRTV->SRV());
	debug2D[4]->SRV(tangentRTV->SRV());
	debug2D[5]->SRV(depthStencil->SRV());
}

GBuffer::~GBuffer()
{
	delete diffuseRTV;
	delete normalRTV;
	delete specularRTV;
	delete depthStencil;
	delete emissiveRTV;
	delete tangentRTV;

	delete viewport;

	delete buffer;

	delete vertexBuffer;

	for (int i = 0; i < 6; i++)
	{
		delete debug2D[i];
	}
}

void GBuffer::PreRender()
{
	RenderTarget* rtvs[5];
	rtvs[0] = diffuseRTV;
	rtvs[1] = specularRTV;
	rtvs[2] = emissiveRTV;
	rtvs[3] = normalRTV;
	rtvs[4] = tangentRTV;

	RenderTarget::Sets(rtvs, 5, depthStencil);
	viewport->RSSetViewport();

	Matrix projection = Context::Get()->Projection();
	desc.Perspective.x = 1.0f / projection._11;
	desc.Perspective.y = 1.0f / projection._22;

	desc.Perspective.z = projection._43;
	desc.Perspective.w = -projection._33;

	buffer->Apply();
	sBuffer->SetConstantBuffer(buffer->Buffer());
}

void GBuffer::Render(bool bVisibleRender2D)
{
	ID3D11ShaderResourceView* srvs[6] =
	{
		depthStencil->SRV(),
		diffuseRTV->SRV(),
		specularRTV->SRV(),
		emissiveRTV->SRV(),
		normalRTV->SRV(),
		tangentRTV->SRV()
	};
	sSrvs->SetResourceArray(srvs, 0, 6);

	/*vertexBuffer->Render();
	shader->Draw(0, 4, 6);*/
	D3D::GetDC()->IASetVertexBuffers(NULL, NULL, NULL, NULL, NULL);
	D3D::GetDC()->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	shader->Draw(0, 4,4);

	if (bVisibleRender2D == true)
	{
		for (int i = 0; i < 6; i++)
		{
			debug2D[i]->Update();
			debug2D[i]->Render();
		}
	}
}
