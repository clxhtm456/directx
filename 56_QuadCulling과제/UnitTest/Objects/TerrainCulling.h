#pragma once

#include "Systems/IExecute.h"

class TerrainCulling : public IExecute
{
public:
	virtual void Initialize() override;
	virtual void Ready() override {};
	virtual void Destroy() override;
	virtual void Update() override;
	virtual void PreRender() override {};
	virtual void Render() override;
	virtual void PostRender() override {};
	virtual void ResizeScreen() override {};
private:
	vector<Transform*> transforms;
	float fov = 0.25f;
	float zFar = 500.0f;

	class Camera* camera;
	Perspective* perspective;

	Shader* terrainShader;
	class Terrain* terrain;


	//ModelCreator* model;
};
