#include "stdafx.h"

#include "TerrainCulling.h"
#include "Viewer/Freedom.h"
#include "Objects/Frustum.h"
#include "Viewer/Fixity.h"

#include "Environment/Terrain.h"
#include "Environment/QuadTerrain.h"

#define CONVP2ARRAY(__array__,__width__,__x__,__y__)	 __array__[__y__*__width__+__x__]

void TerrainCulling::Initialize()
{
	Context::Get()->GetCamera()->RotationDegree(40, 0, 0);
	Context::Get()->GetCamera()->Position(128, 150, -100);
	dynamic_cast<Freedom*>(Context::Get()->GetCamera())->Speed(0, 0);


	camera = new Freedom();
	camera->Position(128, 0, -100);
	dynamic_cast<Freedom*>(camera)->Speed(20, 2);
	perspective = new Perspective(1024, 768, 1, zFar, Math::PI * fov);

	terrainShader = new Shader(L"23_TerrainSpatting.fx");

	terrain = new QuadTerrain(terrainShader, L"Terrain/Gray256.png");
	terrain->BaseMap(L"Terrain/Rock (Basic).jpg");
	terrain->LayerMap(L"Terrain/Path (Rocky).jpg", L"Terrain/Splatting.png");
	dynamic_cast<QuadTerrain*>(terrain)->SetCamera(camera, perspective);
	//terrain->Pass(1);


}

void TerrainCulling::Destroy()
{
	delete camera;
	delete perspective;

	for (auto transform : transforms)
		delete transform;

	delete terrain;
	delete terrainShader;

}

void TerrainCulling::Update()
{
	/*Vector3 pos;
	camera->Position(&pos);
	Vector3 rot;
	camera->RotationDegree(&rot);

	if (ImGui::SliderFloat3("pos", pos,-200,200) ||
		ImGui::SliderFloat3("rot", rot,-180,180))
	{
		camera->Position(pos);
		camera->RotationDegree(rot);
	}*/

	terrain->Update();
	camera->Update();
		

	if (ImGui::InputFloat("zFar", &zFar, 1.0f) ||
		ImGui::InputFloat("fov", &fov, 1e-3f))
		perspective->Set(1024, 768, 1, zFar, Math::PI * fov);
}

void TerrainCulling::Render()
{

	terrain->Render();

}
