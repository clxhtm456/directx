
#include "Framework.h"
#include "QuadTerrain.h"
#include "Objects/Frustum.h"


QuadTerrain::QuadTerrain(Shader* shader, wstring heightFile, int divideCount) :
	Terrain(shader,heightFile),
	divideCount(divideCount)
{
	camera = Context::Get()->GetCamera();

	frustum = new Frustum(camera, perspective);

	int semiWidth = pow(2, divideCount);
	semiTerrain = new SemiTerrain[semiWidth* semiWidth];

	CreateSemiVertex();
}

QuadTerrain::~QuadTerrain()
{
	delete frustum;
	delete[] semiTerrain;
}

void QuadTerrain::Update()
{
	Super::Update();
	frustum->Update();
}

void QuadTerrain::Render()
{
	Renderer::Render();

	if (baseMap != NULL)
		sBaseMap->SetResource(baseMap->SRV());

	brushBuffer->Apply();
	sBrushBuffer->SetConstantBuffer(brushBuffer->Buffer());

	lineBuffer->Apply();
	sLineBuffer->SetConstantBuffer(lineBuffer->Buffer());

	if (layerMap != NULL && alphaMap != NULL)
	{
		sAlphaMap->SetResource(alphaMap->SRV());
		sLayerMap->SetResource(layerMap->SRV());
	}

	CheckQuadTerrain(0, width-1, divideCount);
	//DrawSemiVertex(0);
	//DrawSemiVertex(1);
}

void QuadTerrain::SetCamera(Camera* cam, Perspective* perspective)
{
	delete frustum;
	frustum = new Frustum(cam, perspective);
}

void QuadTerrain::CheckQuadTerrain(UINT start, UINT end, int count)
{
	if (count-- <= 0)
		return;


	UINT jumpCount = (end - start) * 0.5f;//127/63/31
	UINT startIndex = start;

	for (int y = 0; y < 2; y++)
	{
		for (int x = 0; x < 2; x++)
		{
			vector<UINT> lineDrawer;
			startIndex = start + (y * (width) + x) * (jumpCount+1);

			int leftbottomX = startIndex % 256;
			int leftbottomY = startIndex / 256;
			int rightbottomX = (startIndex + jumpCount) % 256;
			int rightbottomY = (startIndex + jumpCount) / 256;
			int lefttopX = (startIndex + width * jumpCount) % 256;
			int lefttopY = (startIndex + width * jumpCount) / 256;
			int righttopX = (startIndex + width * jumpCount + jumpCount) % 256;
			int righttopY = (startIndex + width * jumpCount + jumpCount) / 256;


			lineDrawer.push_back(startIndex);
			lineDrawer.push_back(startIndex + jumpCount);
			lineDrawer.push_back(startIndex + width * jumpCount);
			lineDrawer.push_back(startIndex + width * jumpCount + jumpCount);

			if (CheckVector(lineDrawer, count + 1))
			{
				DrawLine(lineDrawer);
			}
			else
			{
				CheckQuadTerrain(startIndex, startIndex + jumpCount, count);
			}
		}

	}

}

bool QuadTerrain::CheckVector(vector<UINT> vectorarray, int count)
{
	bool result = true;
	Vector3 position;
	for (auto index : vectorarray)
	{
		position = vertices[index].Position;
		//transforms[index]->Position(&position);

		if (frustum->CheckPoint(position) == false)
		{
			result = false;
		}
	}
	
	if (!result)
		return false;
	int leftbottomX = vectorarray[0] % 256;
	int leftbottomY = vectorarray[0] / 256;
	int rightbottomX = vectorarray[1] % 256;
	int rightbottomY = vectorarray[1] / 256;
	int lefttopX = vectorarray[2] % 256;
	int lefttopY = vectorarray[2] / 256;
	int righttopX = vectorarray[3] % 256;
	int righttopY = vectorarray[3] / 256;

	int semiCount = (width / pow(2, divideCount));
	int semiWidth = (rightbottomX - leftbottomX + 1)/ semiCount;
	int semiHeight = (righttopY - rightbottomY  + 1)/ semiCount;

	UINT startPosX = (leftbottomX + 1) / 32;
	UINT startPosY = ((leftbottomY + 1) / 4);

	UINT startPos = startPosX + startPosY;

	for (int z = 0; z < semiHeight; z++)
	{
		for (int x = 0; x < semiWidth; x++)
		{
			int index = startPos + (z * pow(2, divideCount) + x);
			DrawSemiVertex(index);
		}
	}

	

	
	
	/*for (auto index : vectorarray)
	{
		transforms[index]->Update();
		transforms[index]->Render();

		shader->DrawIndexed(0, 0, 36);
	}*/
	return true;
}

void QuadTerrain::DrawLine(vector<UINT> vectorarray)
{
	Vector3 position;
	Vector3 dest[4];
	UINT index = 0;
	for (auto vectorindex : vectorarray)
	{
		position = vertices[vectorindex].Position;
		//transforms[vectorindex]->Position(&position);
		dest[index++] = position;
	}
	Color color = Color(1, 0, 0, 1);

	//Front
	DebugLine::Get()->RenderLine(dest[0], dest[1], color);
	DebugLine::Get()->RenderLine(dest[1], dest[3], color);
	DebugLine::Get()->RenderLine(dest[3], dest[2], color);
	DebugLine::Get()->RenderLine(dest[2], dest[0], color);
}

void QuadTerrain::CreateSemiVertex()
{
	int divideWidth = pow(2, divideCount);
	int size = divideWidth * divideWidth;
	for (int i = 0; i < size; i++)
	{
		semiTerrain[i].vertices = new TerrainVertex[vertexCount/ size];
		semiTerrain[i].indices = new UINT[((width / divideWidth)-1)* ((height/divideWidth) -1)*6];

		//CreateIndices
		int index = 0;
		for (UINT y = 0; y < (height)/ divideWidth -1; y++)
		{
			for (UINT x = 0; x < (width)/ divideWidth -1; x++)
			{
				semiTerrain[i].indices[index + 0] = ((width)/ divideWidth) * y + x;
				semiTerrain[i].indices[index + 1] = ((width)/ divideWidth) * (y + 1) + x;
				semiTerrain[i].indices[index + 2] = ((width)/ divideWidth) * (y + 1) + (x + 1);
				semiTerrain[i].indices[index + 3] = ((width)/ divideWidth) * y + x;
				semiTerrain[i].indices[index + 4] = ((width)/ divideWidth) * (y + 1) + (x + 1);
				semiTerrain[i].indices[index + 5] = ((width)/ divideWidth) * y + (x + 1);

				index += 6;
			}
		}
	}

	int semiTIndex = -1;
	int semiVIndex = 0;
	int lawCount = 0;
	int semiWidth = (width / divideWidth);
	int heightCount = -1;
	for (UINT z = 0; z < height; z++)
	{
		if (z % semiWidth == 0)
		{
			lawCount = 0;
			heightCount++;
		}
		semiTIndex = -1 + (heightCount * divideWidth);
		for (UINT x = 0; x < width; x++)
		{
			UINT index = z * width + x;
			if (index % semiWidth == 0)
			{
				semiTIndex++;
				semiVIndex = lawCount;
			}
			semiTerrain[semiTIndex].vertices[semiVIndex++] = vertices[index];

			vertices[index];
		}
		lawCount += semiWidth;
	}
	for (int i = 0; i < size; i++)
	{
		semiTerrain[i].vertexBuffer = new VertexBuffer(semiTerrain[i].vertices, vertexCount / size,sizeof(TerrainVertex));
		semiTerrain[i].indexBuffer = new IndexBuffer(semiTerrain[i].indices, indexCount / size);
	}
}

void QuadTerrain::DrawSemiVertex(UINT startPos)
{
	semiTerrain[startPos].vertexBuffer->Render();
	semiTerrain[startPos].indexBuffer->Render();
	shader->DrawIndexed(0, Pass(), indexCount / (width/pow(2,divideCount)));
	
}

