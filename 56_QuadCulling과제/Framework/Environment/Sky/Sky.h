#pragma once

class Sky : public Renderer
{
public:
	Sky(Shader* shader);
	~Sky();
public:
	void Update() override;
	void Render() override;
	void PreRender();
	void PostRender();

	void Pass(UINT scatteringPass);
private:
	struct ScatterDesc
	{
		Vector3 WaveLength = Vector3(0.65f,0.57f,0.475f);
		int SampleCount = 8;

		Vector3 InvWaveLength;
		float StarIntensity;

		Vector3 WaveLengthMie;
		float MoonAlpha;
	}scatterDesc;
private:
	bool bRealTime = false;

	float timeFactor = 1.0f;
	float theta = 0.0f, prevTheta = 1.0f;

	class Scattering * scattering;

private:
	ConstantBuffer* scatterBuffer;
	ID3DX11EffectConstantBuffer* sScatterBuffer;
};