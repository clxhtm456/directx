#pragma once

#include "Terrain.h"


#define DIVIDECOUNT	3

class QuadTerrain : public Terrain
{
public:
	QuadTerrain(Shader* shader, wstring heightFile,int divideCount = DIVIDECOUNT);
	~QuadTerrain();

	void Update() override;
	void Render() override;

public:
	void SetCamera(Camera* cam,Perspective* perspective);
private:
	void CheckQuadTerrain(UINT start, UINT end, int count);
	bool CheckVector(vector<UINT> vectorarray, int count);
	void DrawLine(vector<UINT> vectorarray);

	void CreateSemiVertex();
	void DrawSemiVertex(UINT startPos);
public:
	int divideCount;
private:
	struct SemiTerrain
	{
		TerrainVertex* vertices;
		UINT* indices;

		VertexBuffer* vertexBuffer;
		IndexBuffer* indexBuffer;
		~SemiTerrain()
		{
			delete vertexBuffer;
			delete indexBuffer;
		}
	};

	SemiTerrain* semiTerrain;
private:
	Camera* camera;
	Perspective* perspective;
	class Frustum* frustum;
};