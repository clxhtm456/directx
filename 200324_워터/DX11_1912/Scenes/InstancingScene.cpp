#include "Framework.h"

InstancingScene::InstancingScene()
{
	material = new Material(L"Instancing");
	material->SetDiffuseMap(L"Textures/Landscape/Box.png");

	for (UINT i = 0; i < COUNT; i++)
	{
		transforms[i] = new Transform();
		transforms[i]->position = RandomVec3(-30, 30);
		transforms[i]->scale = RandomVec3(1.0f, 3.0f);
		transforms[i]->rotation = RandomVec3(0, D3DX_PI * 2.0f);

		transforms[i]->UpdateWorld();

		worlds[i] = transforms[i]->GetWorld();
		D3DXMatrixTranspose(&worlds[i], &worlds[i]);
	}

	CreateCube();

	instanceBuffer = new VertexBuffer(worlds, sizeof(Matrix), COUNT);
}

InstancingScene::~InstancingScene()
{
	delete material;
	delete vertexBuffer;
	delete instanceBuffer;
	delete indexBuffer;

	for (Transform* transform : transforms)
		delete transform;
}

void InstancingScene::Update()
{
}

void InstancingScene::PreRender()
{
}

void InstancingScene::Render()
{
	vertexBuffer->Set();
	instanceBuffer->Set(1);
	indexBuffer->Set();

	IASetPT();

	material->Set();	

	/*
	for (UINT i = 0; i < COUNT; i++)
	{
		transforms[i]->SetVS();
		DC->DrawIndexed(indices.size(), 0, 0);
	}*/

	DC->DrawIndexedInstanced(indices.size(), COUNT, 0, 0, 0);
}

void InstancingScene::PostRender()
{
}

void InstancingScene::CreateCube()
{
	float w, h, d;
	w = h = d = 0.5f;

	//Front
	vertices.push_back(VertexUVNormal(-w, -h, -d, 0, 1, 0, 0, -1));
	vertices.push_back(VertexUVNormal(-w, +h, -d, 0, 0, 0, 0, -1));
	vertices.push_back(VertexUVNormal(+w, +h, -d, 1, 0, 0, 0, -1));
	vertices.push_back(VertexUVNormal(+w, -h, -d, 1, 1, 0, 0, -1));

	//Back
	vertices.push_back(VertexUVNormal(-w, -h, +d, 1, 1, 0, 0, 1));
	vertices.push_back(VertexUVNormal(+w, -h, +d, 0, 1, 0, 0, 1));
	vertices.push_back(VertexUVNormal(+w, +h, +d, 0, 0, 0, 0, 1));
	vertices.push_back(VertexUVNormal(-w, +h, +d, 1, 0, 0, 0, 1));

	//Top
	vertices.push_back(VertexUVNormal(-w, +h, -d, 0, 1, 0, 1, 0));
	vertices.push_back(VertexUVNormal(-w, +h, +d, 0, 0, 0, 1, 0));
	vertices.push_back(VertexUVNormal(+w, +h, +d, 1, 0, 0, 1, 0));
	vertices.push_back(VertexUVNormal(+w, +h, -d, 1, 1, 0, 1, 0));

	//Bottom
	vertices.push_back(VertexUVNormal(-w, -h, -d, 1, 1, 0, -1, 0));
	vertices.push_back(VertexUVNormal(+w, -h, -d, 0, 1, 0, -1, 0));
	vertices.push_back(VertexUVNormal(+w, -h, +d, 0, 0, 0, -1, 0));
	vertices.push_back(VertexUVNormal(-w, -h, +d, 1, 0, 0, -1, 0));

	//Left
	vertices.push_back(VertexUVNormal(-w, -h, +d, 0, 1, -1, 0, 0));
	vertices.push_back(VertexUVNormal(-w, +h, +d, 0, 0, -1, 0, 0));
	vertices.push_back(VertexUVNormal(-w, +h, -d, 1, 0, -1, 0, 0));
	vertices.push_back(VertexUVNormal(-w, -h, -d, 1, 1, -1, 0, 0));

	//Right
	vertices.push_back(VertexUVNormal(+w, -h, -d, 0, 1, 1, 0, 0));
	vertices.push_back(VertexUVNormal(+w, +h, -d, 0, 0, 1, 0, 0));
	vertices.push_back(VertexUVNormal(+w, +h, +d, 1, 0, 1, 0, 0));
	vertices.push_back(VertexUVNormal(+w, -h, +d, 1, 1, 1, 0, 0));

	indices =
	{
		0, 1, 2, 0, 2, 3,
		4, 5, 6, 4, 6, 7,
		8, 9, 10, 8, 10, 11,
		12, 13, 14, 12, 14, 15,
		16, 17, 18, 16, 18, 19,
		20, 21, 22, 20, 22, 23
	};

	vertexBuffer = new VertexBuffer(vertices.data(), sizeof(VertexUVNormal), vertices.size());
	indexBuffer = new IndexBuffer(indices.data(), indices.size());
}
