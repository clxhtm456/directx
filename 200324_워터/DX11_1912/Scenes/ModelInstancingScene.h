#pragma once

class ModelInstancingScene : public Scene
{
private:
	//AModelRender* model;
	AModelAnimator* model;

public:
	ModelInstancingScene();
	~ModelInstancingScene();

	virtual void Update() override;
	virtual void PreRender() override;
	virtual void Render() override;
	virtual void PostRender() override;
};