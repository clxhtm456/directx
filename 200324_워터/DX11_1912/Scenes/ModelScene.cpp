#include "Framework.h"

ModelScene::ModelScene()
{	
	string name = "pikachu";

	reader = new AssimpReader("FbxData/Models/" + name + ".fbx");
	reader->ExporterMaterial(name + "/" + name);
	reader->ExporterMesh(name + "/" + name);
	delete reader;
	
	reader = new AssimpReader("FbxData/Animations/" + name + "/Idle.fbx");
	reader->ExporterAnimClip(0, name + "/Idle");	
	delete reader;
	
	reader = new AssimpReader("FbxData/Animations/" + name + "/Run.fbx");
	reader->ExporterAnimClip(0, name + "/Run");
	delete reader;
	reader = new AssimpReader("FbxData/Animations/" + name + "/Attack.fbx");
	reader->ExporterAnimClip(0, name + "/Attack");
	delete reader;
	
	player = new AModelAnimator(Shader::Add(L"AModelAnimation"));
	player->ReadMaterial(name + ".mat", name + "/");
	player->ReadMesh(name + ".mesh", name + "/");
	player->ReadClip("Idle.clip", name + "/");
	player->ReadClip("Run.clip", name + "/");
	player->ReadClip("Attack.clip", name + "/");

	player->PlayClip(0, 0);
}

ModelScene::~ModelScene()
{
	delete player;
}

void ModelScene::Update()
{
	if (KEYDOWN(VK_F1))
		player->PlayClip(0, 0);
	if (KEYDOWN(VK_F2))
		player->PlayClip(0, 1);
	if (KEYDOWN(VK_F3))
		player->PlayClip(0, 2);

	player->Update();
}

void ModelScene::PreRender()
{
}

void ModelScene::Render()
{
	player->Render();
}

void ModelScene::PostRender()
{
	
}
