#include "Framework.h"

ShadowScene::ShadowScene()
{
	//Export();

	pikachu = new Pikachu();
	terrain = new Terrain();
	pikachu->SetTerrain(terrain);
	plane = new GameModel("Plane");
	plane->GetModel()->SetDiffuseMap(L"Textures/Landscape/Floor.png");
	plane->scale = Vector3(10, 10, 10);
	plane->SetShadow(true);
	pikachu->SetShader(L"Shadow");

	shadow = new Shadow();
	shadow->Add(pikachu);
	shadow->Add(plane);
}

ShadowScene::~ShadowScene()
{
	delete pikachu;
	delete plane;
	delete terrain;

	delete shadow;
}

void ShadowScene::Update()
{
	//pikachu->Update();
	//terrain->Update();
	//plane->Update();

	shadow->Update();	
}

void ShadowScene::PreRender()
{
	shadow->PreRender();

	terrain->Render();
}

void ShadowScene::Render()
{
	//pikachu->Render();
	//terrain->Render();
	//shadow->Render();
	terrain->Render();
	//plane->Render();
}

void ShadowScene::PostRender()
{
	shadow->PostRender();
}

void ShadowScene::Export()
{	
	//string name = "Pikachu";
	string name = "Plane";
	
	Exporter* reader = new Exporter("FbxData/Models/" + name + ".fbx");
	reader->ExporterMaterial(name, name + "/");
	reader->ExporterMesh(name, name + "/");
	delete reader;
	/*
	reader = new Exporter("FbxData/Animations/" + name + "/Idle.fbx");
	reader->ExporterAnimation(0, "Idle.clip", name + "/");
	delete reader;
	reader = new Exporter("FbxData/Animations/" + name + "/Run.fbx");
	reader->ExporterAnimation(0, "Run.clip", name + "/");
	delete reader;
	reader = new Exporter("FbxData/Animations/" + name + "/Attack.fbx");
	reader->ExporterAnimation(0, "Attack.clip", name + "/");
	delete reader;
	*/
}

