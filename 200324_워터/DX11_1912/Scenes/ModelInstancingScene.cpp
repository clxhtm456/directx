#include "Framework.h"

ModelInstancingScene::ModelInstancingScene()
{
	string name = "pikachu";

	
	AssimpReader* reader = new AssimpReader("FbxData/Models/" + name + ".fbx");
	reader->ExporterMaterial(name + "/" + name);
	reader->ExporterMesh(name + "/" + name);
	delete reader;
	
	/*
	reader = new AssimpReader("FbxData/Animations/" + name + "/Idle.fbx");
	reader->ExporterAnimClip(0, name + "/Idle");
	delete reader;
	reader = new AssimpReader("FbxData/Animations/" + name + "/Run.fbx");
	reader->ExporterAnimClip(0, name + "/Run");
	delete reader;
	reader = new AssimpReader("FbxData/Animations/" + name + "/Attack.fbx");
	reader->ExporterAnimClip(0, name + "/Attack");
	delete reader;
	*/

	model = new AModelAnimator(Shader::Add(L"ModelAnimationInstancing"));
	model->ReadMaterial(name + ".mat", name + "/");
	model->ReadMesh(name + ".mesh", name + "/");
	model->ReadClip("Idle.clip", name + "/");
	model->ReadClip("Run.clip", name + "/");
	model->ReadClip("Attack.clip", name + "/");

	vector<AModelClip*>* clips = model->GetModel()->GetClips();
	
	UINT instanceNum = 0;
	for (float z = 0; z < 10; z++)
	{
		for (float x = 0; x < 10; x++)
		{
			Transform* transform = model->AddTransform();
			transform->position = { x * 10, 0, z * 10 };
			transform->scale = { 0.1f, 0.1f, 0.1f };
			transform->UpdateWorld();

			model->PlayClip(instanceNum, Random(0, clips->size()), Random(0.5f, 10.0f));
			instanceNum++;
		}
	}

	model->UpdateTransforms();
}

ModelInstancingScene::~ModelInstancingScene()
{
	delete model;
}

void ModelInstancingScene::Update()
{
	model->Update();
}

void ModelInstancingScene::PreRender()
{
}

void ModelInstancingScene::Render()
{
	model->Render();
}

void ModelInstancingScene::PostRender()
{
}
