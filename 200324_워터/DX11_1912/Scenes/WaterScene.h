#pragma once

class WaterScene : public Scene
{
private:
	SkyCube* sky;

	Terrain* terrain;
	Water* water;

public:
	WaterScene();
	~WaterScene();

	virtual void Update() override;
	virtual void PreRender() override;
	virtual void Render() override;
	virtual void PostRender() override;
};