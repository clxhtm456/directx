#pragma once

class LightScene : public Scene
{
private:
	GameModel* plane;
	GameModel* bunny;

	AmbientBuffer* ambientBuffer;
	PointBuffer* pointBuffer;
	SpotBuffer* spotBuffer;
	CapsuleBuffer* capsuleBuffer;	
public:
	LightScene();
	~LightScene();

	// Scene을(를) 통해 상속됨
	virtual void Update() override;
	virtual void PreRender() override;
	virtual void Render() override;
	virtual void PostRender() override;

	void Export();
};