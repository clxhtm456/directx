#pragma once

class InstancingScene : public Scene
{
private:
	static const UINT COUNT = 1000;

	Material* material;

	vector<VertexUVNormal> vertices;
	vector<UINT> indices;

	VertexBuffer* vertexBuffer;
	VertexBuffer* instanceBuffer;
	IndexBuffer* indexBuffer;

	Transform* transforms[COUNT];
	Matrix worlds[COUNT];
public:
	InstancingScene();
	~InstancingScene();

	virtual void Update() override;
	virtual void PreRender() override;
	virtual void Render() override;
	virtual void PostRender() override;

	void CreateCube();
};