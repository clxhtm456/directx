#include "Framework.h"

CharacterScene::CharacterScene()
{
	//Export();

	player = new Pikachu();
	terrain = new Terrain();
	sky = new SkyCube();
	aStar = new AStar();

	CAMERA->SetTarget(player);
	player->SetTerrain(terrain);	
	aStar->SetUp(terrain);
	player->SetAStar(aStar);
}

CharacterScene::~CharacterScene()
{
	delete player;
	delete terrain;
	delete sky;
	delete aStar;
}

void CharacterScene::Update()
{
	player->Update();
	terrain->Update();
	sky->Update();
	aStar->Update();	
}

void CharacterScene::PreRender()
{
}

void CharacterScene::Render()
{
	sky->Render();

	player->Render();
	terrain->Render();
	aStar->Render();
}

void CharacterScene::PostRender()
{
}

void CharacterScene::Export()
{
	string name = "Pikachu";

	Exporter* reader = new Exporter("FbxData/Models/" + name + ".fbx");
	reader->ExporterMaterial(name + "/" + name);
	reader->ExporterMesh(name + "/" + name);
	delete reader;

	reader = new Exporter("FbxData/Animations/" + name + "/Idle.fbx");
	reader->ExporterAnimation(0, "Idle.clip", name + "/");
	delete reader;
	reader = new Exporter("FbxData/Animations/" + name + "/Run.fbx");
	reader->ExporterAnimation(0, "Run.clip", name + "/");
	delete reader;
	reader = new Exporter("FbxData/Animations/" + name + "/Attack.fbx");
	reader->ExporterAnimation(0, "Attack.clip", name + "/");
	delete reader;
}
