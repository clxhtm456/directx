#pragma once

class CharacterScene : public Scene
{
private:
	Pikachu* player;
	Terrain* terrain;
	SkyCube* sky;
	AStar* aStar;
public:
	CharacterScene();
	~CharacterScene();
	
	virtual void Update() override;
	virtual void PreRender() override;
	virtual void Render() override;
	virtual void PostRender() override;

	void Export();
};