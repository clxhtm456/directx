#pragma once

class TextureScene : public Scene
{
private:
	Shader* shader;

	WorldBuffer* worldBuffer;
	ColorBuffer* colorBuffer;

	VertexBuffer* vertexBuffer;
	IndexBuffer* indexBuffer;

	ID3D11ShaderResourceView* textureView;
	ID3D11SamplerState* sampler;
public:
	TextureScene();
	~TextureScene();

	virtual void Update() override;
	virtual void PreRender() override;
	virtual void Render() override;
	virtual void PostRender() override;
};