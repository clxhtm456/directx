#include "Framework.h"

GridScene::GridScene()
	: width(10), height(10)
{
	//shader = new Shader(L"Grid");
	shader = Shader::Add(L"Grid");
	heightMap = Texture::Add(L"Textures/HeightMaps/ColorMap256.png");

	width = heightMap->GetWidth() - 1;
	height = heightMap->GetHeight() - 1;

	vector<Color> pixels = heightMap->ReadPixels();

	{//Vertices
		for (UINT z = 0; z <= height; z++)
		{
			for (UINT x = 0; x <= width; x++)
			{
				VertexType vertex;
				vertex.position = Vector3(x, rand() % 2, z);
				vertex.uv = Vector2(1.0f - x / (float)width, 1.0f - z / (float)height);

				UINT index = (width + 1) * z + x;
				vertex.position.y = pixels[index].r * 20.0f;

				vertices.push_back(vertex);
			}
		}
	}

	{//Indices
		for (UINT z = 0; z < height; z++)
		{
			for (UINT x = 0; x < width; x++)
			{
				indices.push_back((width + 1) * z + x);//0
				indices.push_back((width + 1) * (z + 1) + x);//1
				indices.push_back((width + 1) * (z + 1) + x + 1);//2

				indices.push_back((width + 1) * z + x);//0				
				indices.push_back((width + 1) * (z + 1) + x + 1);//2
				indices.push_back((width + 1) * z + x + 1);//3
			}
		}
	}

	CreateNormal();

	vertexBuffer = new VertexBuffer(vertices.data(), sizeof(VertexType), vertices.size());
	indexBuffer = new IndexBuffer(indices.data(), indices.size());

	worldBuffer = new WorldBuffer();

	D3D11_RASTERIZER_DESC rsDesc = {};
	rsDesc.AntialiasedLineEnable = false;
	rsDesc.CullMode = D3D11_CULL_BACK;
	rsDesc.FillMode = D3D11_FILL_WIREFRAME;

	DEVICE->CreateRasterizerState(&rsDesc, &state);
	
	diffuseMap = Texture::Add(L"Textures/Landscape/Dirt2.png");
}

GridScene::~GridScene()
{
	//delete shader;
	delete vertexBuffer;
	delete indexBuffer;
	delete worldBuffer;

	state->Release();
}

void GridScene::Update()
{
}

void GridScene::PreRender()
{
}

void GridScene::Render()
{
	//DC->RSSetState(state);

	vertexBuffer->Set();
	indexBuffer->Set();
	IASetPT();

	worldBuffer->SetVSBuffer(1);

	diffuseMap->Set(0);
	shader->Set();

	DC->DrawIndexed(indices.size(), 0, 0);
}

void GridScene::PostRender()
{
}

void GridScene::CreateNormal()
{
	for (UINT i = 0; i < indices.size() / 3; i++)
	{
		UINT index0 = indices[i * 3 + 0];
		UINT index1 = indices[i * 3 + 1];
		UINT index2 = indices[i * 3 + 2];

		VertexType v0 = vertices[index0];
		VertexType v1 = vertices[index1];
		VertexType v2 = vertices[index2];

		Vector3 A = v1.position - v0.position;
		Vector3 B = v2.position - v0.position;

		Vector3 normal;
		D3DXVec3Cross(&normal, &A, &B);

		vertices[index0].normal += normal;
		vertices[index1].normal += normal;
		vertices[index2].normal += normal;
	}

	for (VertexType& vertex : vertices)
		D3DXVec3Normalize(&vertex.normal, &vertex.normal);
}
