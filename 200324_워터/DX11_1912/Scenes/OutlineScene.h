#pragma once

class OutlineScene : public Scene
{
private:
	Pikachu* player;
	Scattering* sky;

	RenderTarget* playerTarget;
	RenderTarget* renderTarget;

	Render2D* screen2D;
	BlurEffect* player2D;

	BlendState* blendState[2];
public:
	OutlineScene();
	~OutlineScene();
	
	virtual void Update() override;
	virtual void PreRender() override;
	virtual void Render() override;
	virtual void PostRender() override;

};