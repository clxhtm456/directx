#include "Framework.h"

TextureScene::TextureScene()
{
	//shader = new Shader(L"Texture");
	shader = Shader::Add(L"Texture");

	worldBuffer = new WorldBuffer();
	colorBuffer = new ColorBuffer();

	VertexUV vertices[4];
	vertices[0].position = Vector3(-1, 1, 0);	
	vertices[1].position = Vector3(1, 1, 0);
	vertices[2].position = Vector3(-1, -1, 0);
	vertices[3].position = Vector3(1, -1, 0);

	vertices[0].uv = Vector2(0, 0);
	vertices[1].uv = Vector2(1, 0);
	vertices[2].uv = Vector2(0, 1);
	vertices[3].uv = Vector2(1, 1);

	vertexBuffer = new VertexBuffer(vertices, sizeof(VertexUV), 4);

	UINT indices[6] = {
		0, 1, 2,
		1, 3, 2
	};

	indexBuffer = new IndexBuffer(indices, 6);

	D3DX11CreateShaderResourceViewFromFile(DEVICE,
		L"Textures/test.jpg", nullptr, nullptr, &textureView, nullptr);

	D3D11_SAMPLER_DESC sampDesc = {};
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;

	DEVICE->CreateSamplerState(&sampDesc, &sampler);
}

TextureScene::~TextureScene()
{
	//delete shader;
	delete worldBuffer;
	delete vertexBuffer;
	delete indexBuffer;
	delete colorBuffer;

	textureView->Release();
	sampler->Release();
}

void TextureScene::Update()
{
}

void TextureScene::PreRender()
{
}

void TextureScene::Render()
{
	vertexBuffer->Set();
	indexBuffer->Set();
	IASetPT();

	worldBuffer->SetVSBuffer(1);
	colorBuffer->SetPSBuffer(0);

	shader->Set();

	DC->PSSetShaderResources(0, 1, &textureView);
	DC->PSSetSamplers(0, 1, &sampler);

	DC->DrawIndexed(6, 0, 0);
}

void TextureScene::PostRender()
{
	ImGui::ColorEdit4("color", colorBuffer->data.color);
}
