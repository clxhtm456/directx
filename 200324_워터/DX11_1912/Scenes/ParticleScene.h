#pragma once

class ParticleScene : public Scene
{
private:
	vector<Sphere*> spheres;
	ParticleRain* rain;
	Scattering* sky;
public:
	ParticleScene();
	~ParticleScene();
	
	virtual void Update() override;
	virtual void PreRender() override;
	virtual void Render() override;
	virtual void PostRender() override;
};