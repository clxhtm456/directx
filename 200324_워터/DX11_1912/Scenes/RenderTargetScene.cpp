#include "Framework.h"

RenderTargetScene::RenderTargetScene()
{
	terrain = new Terrain();
	sky = new SkyCube();

	renderTarget = new RenderTarget();

	targetTexture = new Render2D();
	targetTexture->scale = Vector2(WIN_WIDTH * 0.5f, WIN_HEIGHT * 0.5f);
	targetTexture->position = Vector2(WIN_WIDTH * 0.25f, WIN_HEIGHT * 0.25f);
	targetTexture->SetSRV(renderTarget->GetSRV());

	monoEffect = new MonoEffect();
	monoEffect->scale = Vector2(WIN_WIDTH * 0.5f, WIN_HEIGHT * 0.5f);
	monoEffect->position = Vector2(WIN_WIDTH * 0.75f, WIN_HEIGHT * 0.25f);
	monoEffect->SetSRV(renderTarget->GetSRV());

	blurEffect = new BlurEffect();
	blurEffect->scale = Vector2(WIN_WIDTH * 0.5f, WIN_HEIGHT * 0.5f);
	blurEffect->position = Vector2(WIN_WIDTH * 0.75f, WIN_HEIGHT * 0.75f);
	blurEffect->SetSRV(renderTarget->GetSRV());
}

RenderTargetScene::~RenderTargetScene()
{
	delete terrain;
	delete sky;

	delete renderTarget;
	delete targetTexture;
	delete monoEffect;
	delete blurEffect;
}

void RenderTargetScene::Update()
{
	terrain->Update();
	sky->Update();

	targetTexture->Update();
	monoEffect->Update();
	blurEffect->Update();
}

void RenderTargetScene::PreRender()
{
	renderTarget->Set(Color(0, 0, 0, 0));

	//sky->Render();
	terrain->Render();
}

void RenderTargetScene::Render()
{
	//sky->Render();
}

void RenderTargetScene::PostRender()
{		
	targetTexture->Render();
	monoEffect->Render();
	blurEffect->Render();
}
