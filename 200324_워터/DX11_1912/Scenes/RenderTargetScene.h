#pragma once

class RenderTargetScene : public Scene
{
private:
	RenderTarget* renderTarget;
	Render2D* targetTexture;
	MonoEffect* monoEffect;
	BlurEffect* blurEffect;

	Terrain* terrain;
	SkyCube* sky;

public:
	RenderTargetScene();
	~RenderTargetScene();

	// Scene을(를) 통해 상속됨
	virtual void Update() override;
	virtual void PreRender() override;
	virtual void Render() override;
	virtual void PostRender() override;
};