#pragma once

class BillboardScene : public Scene
{
private:
	struct VertexTest
	{
		Vector3 position;
		Vector2 size;
		Vector3 dir;
	};

	Terrain* terrain;
	//vector<Billboard*> trees;
	Shader* shader;
	VertexBuffer* vertexBuffer;
	UINT count;
	Texture* texture;

	FloatBuffer* floatBuffer;

	BlendState* blendState[2];
public:
	BillboardScene();
	~BillboardScene();

	// Scene을(를) 통해 상속됨
	virtual void Update() override;
	virtual void PreRender() override;
	virtual void Render() override;
	virtual void PostRender() override;

	static bool Compare(Billboard* a, Billboard* b);
};