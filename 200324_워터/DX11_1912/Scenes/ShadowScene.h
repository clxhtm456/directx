#pragma once

class ShadowScene : public Scene
{
private:
	Pikachu* pikachu;
	Terrain* terrain;
	GameModel* plane;

	Shadow* shadow;
	
public:
	ShadowScene();
	~ShadowScene();
	
	virtual void Update() override;
	virtual void PreRender() override;
	virtual void Render() override;
	virtual void PostRender() override;

	void Export();
};