#include "Framework.h"
#include "WaterScene.h"

WaterScene::WaterScene()
{
	sky = new SkyCube();

	Texture* texture = Texture::Add(L"Textures/HeightMaps/ColorMap256.png");

	water = new Water(256, 256, 1, texture);
	terrain = new Terrain();
}

WaterScene::~WaterScene()
{
	delete water;
	delete terrain;

	delete sky;
}

void WaterScene::Update()
{
	sky->Update();
	water->Update();
	terrain->Update();
}

void WaterScene::PreRender()
{
}

void WaterScene::Render()
{
	sky->Render();	
	terrain->Render();
	water->Render();
}

void WaterScene::PostRender()
{
	water->PostRender();
}
