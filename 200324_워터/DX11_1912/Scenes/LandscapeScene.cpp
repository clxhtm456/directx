#include "Framework.h"

LandscapeScene::LandscapeScene()
{
	sphere = new Sphere(5);
	terrain = new TerrainEditor(300, 300);	
}

LandscapeScene::~LandscapeScene()
{
	delete sphere;
	delete terrain;
}

void LandscapeScene::Update()
{
	if (KEYPRESS(VK_UP))
		sphere->position.z += 100 * DELTA;
	if (KEYPRESS(VK_DOWN))
		sphere->position.z -= 100 * DELTA;
	if (KEYPRESS(VK_RIGHT))
		sphere->position.x += 100 * DELTA;
	if (KEYPRESS(VK_LEFT))
		sphere->position.x -= 100 * DELTA;

	//sphere->position.y = terrain->GetPosY(sphere->position) + sphere->GetRadius();

	//sphere->Update();
	terrain->Update();
}

void LandscapeScene::PreRender()
{
}

void LandscapeScene::Render()
{
	//sphere->Render();
	terrain->Render();
}

void LandscapeScene::PostRender()
{
	ImGui::ColorEdit4("MDiffuse", sphere->GetMaterial()->GetBuffer()->data.diffuse);
	ImGui::ColorEdit4("MSpeuclar", sphere->GetMaterial()->GetBuffer()->data.specular);
	ImGui::ColorEdit4("MAmbient", sphere->GetMaterial()->GetBuffer()->data.ambient);

	terrain->PostRender();
}
