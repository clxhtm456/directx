#include "Framework.h"

ParticleScene::ParticleScene()
{
	ParticleManager::Create();

	for (int y = 0; y < 5; y++)
	{
		for (int x = 0; x < 5; x++)
		{
			Sphere* sphere = new Sphere(1.0f);
			sphere->position = Vector3(x * 2, y * 2, 0);
			spheres.push_back(sphere);
		}
	}

	rain = new ParticleRain();

	sky = new Scattering();
}

ParticleScene::~ParticleScene()
{	
	ParticleManager::Delete();
	delete rain;
	delete sky;
}

void ParticleScene::Update()
{
	if (KEYDOWN(VK_LBUTTON))
	{
		Ray ray = CAMERA->GetPickingRay();

		for (Sphere* sphere : spheres)
		{
			Vector3 contact;
			if (sphere->GetCollision()->IsCollision(ray, nullptr, &contact))
			{
				ParticleManager::Get()->Play("Exp", contact);
			}
		}
	}

	for (Sphere* sphere : spheres)
	{
		sphere->Update();
	}

	ParticleManager::Get()->Update();

	rain->Update();
	sky->Update();
}

void ParticleScene::PreRender()
{
	sky->PreRender();
}

void ParticleScene::Render()
{
	sky->Render();

	for (Sphere* sphere : spheres)
	{
		sphere->Render();
	}

	ParticleManager::Get()->Render();

	rain->Render();	
}

void ParticleScene::PostRender()
{
	sky->PostRender();
}
