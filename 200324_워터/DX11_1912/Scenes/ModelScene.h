#pragma once

class ModelScene : public Scene
{
private:
	AssimpReader* reader;

	//AModel* model;
	//AModelRender* modelRender;

	AModelAnimator* player;
public:
	ModelScene();
	~ModelScene();

	virtual void Update() override;
	virtual void PreRender() override;
	virtual void Render() override;
	virtual void PostRender() override;
};