#include "Framework.h"

BillboardScene::BillboardScene()
	: count(1000)
{
	terrain = new Terrain();
	/*
	for (UINT i = 0; i < 1000; i++)
	{
		Billboard* tree = new Billboard(L"Textures/Landscape/Tree.png");

		Vector3 pos;
		pos.x = Random(0.0f, 256.0f);
		pos.z = Random(0.0f, 256.0f);

		Vector3 scale;
		scale.x = Random(3.0f, 10.0f);
		scale.y = Random(3.0f, 10.0f);
		scale.z = 1.0f;

		pos.y = scale.y * 0.5f + terrain->GetPosY(pos);

		tree->position = pos;
		tree->scale = scale;

		trees.push_back(tree);
	}*/
	VertexTest* vertices = new VertexTest[count];

	for (UINT i = 0; i < count; i++)
	{		
		Vector3 pos;
		pos.x = Random(0.0f, 256.0f);
		pos.z = Random(0.0f, 256.0f);

		Vector2 size;
		size.x = Random(3.0f, 10.0f);
		size.y = Random(3.0f, 10.0f);		

		pos.y = size.y * 0.5f + terrain->GetPosY(pos);

		vertices[i].position = pos;
		vertices[i].size = size;
		vertices[i].dir.x = Random(-1.0f, 1.0f);
		vertices[i].dir.y = Random(-1.0f, 1.0f);
		vertices[i].dir.z = Random(-1.0f, 1.0f);
	}

	vertexBuffer = new VertexBuffer(vertices, sizeof(VertexTest), count);
	delete[] vertices;

	shader = Shader::Add(L"GeometryBillboard");
	shader->CreateGeometryShader();

	texture = Texture::Add(L"Textures/Landscape/Tree.png");

	blendState[0] = new BlendState();
	blendState[1] = new BlendState();
	blendState[1]->AlphaToCoverage(true);

	floatBuffer = new FloatBuffer();
}

BillboardScene::~BillboardScene()
{
	delete terrain;

	/*
	for (Billboard* tree : trees)
		delete tree;
		*/
	delete vertexBuffer;

	delete blendState[0];
	delete blendState[1];

	delete floatBuffer;
}

void BillboardScene::Update()
{
	//for (Billboard* tree : trees)
		//tree->Update();

	floatBuffer->data.index += DELTA;

	terrain->Update();
}

void BillboardScene::PreRender()
{
}

void BillboardScene::Render()
{
	//sort(trees.begin(), trees.end(), BillboardScene::Compare);
	terrain->Render();

	//for (Billboard* tree : trees)
		//tree->Render();	
	vertexBuffer->Set();
	IASetPT(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);

	VP->SetGSBuffer(0);
	floatBuffer->SetGSBuffer(10);

	texture->Set(10);
	shader->Set();

	blendState[1]->SetState();

	DC->Draw(count, 0);

	blendState[0]->SetState();

	DC->GSSetShader(nullptr, nullptr, 0);
}

void BillboardScene::PostRender()
{
}

bool BillboardScene::Compare(Billboard* a, Billboard* b)
{
	return false;
}
