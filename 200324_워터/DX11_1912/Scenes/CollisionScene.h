#pragma once

class CollisionScene : public Scene
{
private:
	Collision* box1;
	Collision* box2;

	float distance;
	Vector3 contact;
public:
	CollisionScene();
	~CollisionScene();

	// Scene을(를) 통해 상속됨
	virtual void Update() override;
	virtual void PreRender() override;
	virtual void Render() override;
	virtual void PostRender() override;
};