#include "Framework.h"

CollisionScene::CollisionScene()
{
	//box1 = new BoxCollision();
	//box2 = new BoxCollision();
	box1 = new BoxCollision();
	box2 = new SphereCollision();
	box2->position = Vector3(3, 0, 0);
}

CollisionScene::~CollisionScene()
{
	delete box1;	
	delete box2;
}

void CollisionScene::Update()
{
	if (KEYPRESS('I'))
		box1->position.z += DELTA;
	if (KEYPRESS('K'))
		box1->position.z -= DELTA;

	if (KEYPRESS('J'))
		box1->position.x -= DELTA;
	if (KEYPRESS('L'))
		box1->position.x += DELTA;

	if (KEYPRESS('U'))
		box1->position.y -= DELTA;
	if (KEYPRESS('O'))
		box1->position.y += DELTA;

	if (KEYPRESS(VK_NUMPAD4))
		box1->rotation.y -= DELTA;
	if (KEYPRESS(VK_NUMPAD6))
		box1->rotation.y += DELTA;

	if (KEYPRESS(VK_NUMPAD8))
		box1->rotation.x -= DELTA;
	if (KEYPRESS(VK_NUMPAD5))
		box1->rotation.x += DELTA;

	if (KEYPRESS(VK_NUMPAD7))
		box1->scale.x += DELTA;
	if (KEYPRESS(VK_NUMPAD9))
		box1->scale.x -= DELTA;

	if (KEYPRESS(VK_NUMPAD1))
		box1->scale.y += DELTA;
	if (KEYPRESS(VK_NUMPAD3))
		box1->scale.y -= DELTA;
	/*
	if (box2->IsCollision(box1))
	{
		box2->SetColor(Color(1, 0, 0, 1));
	}
	else
	{
		box2->SetColor(Color(0, 1, 0, 1));
	}*/

	if (KEYDOWN(VK_LBUTTON))
	{
		Ray ray = CAMERA->GetPickingRay();
		if (box1->IsCollision(ray, &distance, &contact))
			box1->SetColor(Color(0, 0, 1, 1));
		
		if (box2->IsCollision(ray))
			box2->SetColor(Color(0, 0, 1, 1));
	}
}

void CollisionScene::PreRender()
{
}

void CollisionScene::Render()
{
	box1->Render();
	box2->Render();
}

void CollisionScene::PostRender()
{
	ImGui::SliderFloat("Distance", &distance, 0, 1000);
	ImGui::SliderFloat3("Contact", contact, -100, 100);
}
