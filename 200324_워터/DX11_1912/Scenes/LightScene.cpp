#include "Framework.h"
#include "LightScene.h"

LightScene::LightScene()
{
	//Export();
	plane = new GameModel("plane");
	plane->GetModel()->SetDiffuseMap(L"Textures/Landscape/Dirt2.png");
	plane->SetShader(L"PointLight");
	plane->scale = { 5, 5, 5 };

	bunny = new GameModel("bunny");
	bunny->scale = Vector3(0.01f, 0.01f, 0.01f);	
	bunny->position.y = 2.5f;

	bunny->GetModel()->SetShader(L"PointLight");

	ambientBuffer = new AmbientBuffer();
	pointBuffer = new PointBuffer();
	spotBuffer = new SpotBuffer();
	capsuleBuffer = new CapsuleBuffer();

	PointLightData lightData;
	lightData.color = Color(1, 0, 0, 1);
	lightData.position = Vector3(-3, 3, 0);
	
	pointBuffer->Add(lightData);

	lightData.color = Color(0, 1, 0, 1);
	lightData.position = Vector3(0, 3, 0);

	pointBuffer->Add(lightData);

	lightData.color = Color(0, 0, 1, 1);
	lightData.position = Vector3(3, 3, 0);

	pointBuffer->Add(lightData);	
}

LightScene::~LightScene()
{
	delete plane;
	delete bunny;

	delete ambientBuffer;
	delete pointBuffer;
	delete spotBuffer;
	delete capsuleBuffer;	
}

void LightScene::Update()
{
	plane->Update();
	bunny->Update();	
}

void LightScene::PreRender()
{
}

void LightScene::Render()
{
	plane->Render();

	ambientBuffer->SetPSBuffer(2);
	pointBuffer->SetPSBuffer(3);
	//spotBuffer->SetPSBuffer(12);
	//capsuleBuffer->SetPSBuffer(9);
	bunny->Render();	
}

void LightScene::PostRender()
{
	ImGui::Text("Ambient");
	ImGui::ColorEdit4("AmbiendFloor", ambientBuffer->data.floorColor);
	ImGui::ColorEdit4("AmbiendCeil", ambientBuffer->data.ceilColor);
	/*
	ImGui::Text("Point");
	ImGui::SliderFloat3("PointPosition", pointBuffer->data.position, -100, 100);
	ImGui::SliderFloat("PointRange", &pointBuffer->data.range, 0, 50);
	ImGui::ColorEdit4("PointColor", pointBuffer->data.color);
	
	ImGui::Text("Spot");
	ImGui::ColorEdit4("SpotColor", spotBuffer->data.color);
	ImGui::SliderFloat3("SpotPosition", spotBuffer->data.position, -100, 100);
	ImGui::SliderFloat("SpotRange", &spotBuffer->data.range, 0, 100);
	ImGui::SliderFloat3("SpotDirection", spotBuffer->data.direction, -1, 1);
	ImGui::SliderFloat("SpotOuter", &spotBuffer->data.outer, 0, 180);
	ImGui::SliderFloat("SpotInner", &spotBuffer->data.inner, 0, 180);
	*/

	/*
	ImGui::Text("Capsule");
	ImGui::ColorEdit4("CapsuleColor", capsuleBuffer->data.color);
	ImGui::SliderFloat3("CapsulePosition", capsuleBuffer->data.position, -100, 100);
	ImGui::SliderFloat("CapsuleRange", &capsuleBuffer->data.range, 0, 100);
	ImGui::SliderFloat3("CapsuleDirection", capsuleBuffer->data.direction, -1, 1);
	ImGui::SliderFloat("CapsuleLength", &capsuleBuffer->data.length, 0, 50);
	*/
	ImGui::Text("Point");
	for (int i = 0; i < pointBuffer->data.lightCount; i++)
	{		
		ImGui::SliderFloat3("PointPosition" + i, pointBuffer->data.lights[i].position, -100, 100);
		ImGui::SliderFloat("PointRange" + i, &pointBuffer->data.lights[i].range, 0, 50);
		ImGui::ColorEdit4("PointColor" + i, pointBuffer->data.lights[i].color);
	}
}

void LightScene::Export()
{
	string name = "Bunny";

	Exporter* reader = new Exporter("FbxData/Models/" + name + ".fbx");
	reader->ExporterMaterial(name, name + "/");
	reader->ExporterMesh(name, name + "/");
	delete reader;
}
