#pragma once

class LandscapeScene : public Scene
{
private:
	Sphere* sphere;
	TerrainEditor* terrain;	
public:
	LandscapeScene();
	~LandscapeScene();

	virtual void Update() override;
	virtual void PreRender() override;
	virtual void Render() override;
	virtual void PostRender() override;
};
