#pragma once

class ComputeScene : public Scene
{
private:
	Render2D* render2D;

public:
	ComputeScene();
	~ComputeScene();

	// Scene을(를) 통해 상속됨
	virtual void Update() override;
	virtual void PreRender() override;
	virtual void Render() override;
	virtual void PostRender() override;

	void Raw();
	void Intersection();
	void Texture2D();
};