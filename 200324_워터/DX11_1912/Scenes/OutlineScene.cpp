#include "Framework.h"

OutlineScene::OutlineScene()
{
	player = new Pikachu();
	sky = new Scattering();

	renderTarget = new RenderTarget();
	playerTarget = new RenderTarget();

	screen2D = new Render2D();
	screen2D->scale = { WIN_WIDTH, WIN_HEIGHT };
	screen2D->position = { WIN_WIDTH * 0.5f, WIN_HEIGHT * 0.5f };
	screen2D->SetSRV(renderTarget->GetSRV());

	player2D = new BlurEffect();
	player2D->scale = { WIN_WIDTH, WIN_HEIGHT };
	player2D->position = { WIN_WIDTH * 0.5f, WIN_HEIGHT * 0.5f };
	player2D->SetSRV(playerTarget->GetSRV());

	blendState[0] = new BlendState();
	blendState[1] = new BlendState();
	blendState[1]->Alpha(true);
}

OutlineScene::~OutlineScene()
{
	delete player;
	delete sky;

	delete renderTarget;
	delete playerTarget;

	delete screen2D;
	delete player2D;

	delete blendState[0];
	delete blendState[1];
}

void OutlineScene::Update()
{
	player->Update();
	sky->Update();

	screen2D->Update();
	player2D->Update();
}

void OutlineScene::PreRender()
{
	playerTarget->Set(Color(0, 0, 0, 0));

	
	player->Render();		

	sky->PreRender();

	renderTarget->Set();
	sky->Render();
	blendState[1]->SetState();
	player2D->Render();
	blendState[0]->SetState();
}

void OutlineScene::Render()
{	
	//sky->Render();
	//player->Render();
}

void OutlineScene::PostRender()
{
	screen2D->Render();
	sky->PostRender();
}
