#include "Header.hlsli"

cbuffer LightVP : register(b10)
{
    matrix LightV;
    matrix LightP;
}

struct PixelInput
{
    float4 position : SV_POSITION;
    float3 normal : NORMAL;
    float2 uv : UV;
    float4 vPosition : LIGHTVIEW;
};

PixelInput VS(VertexUVNormalTangentBlend input)
{
    PixelInput output;
    matrix boneWorld;
    if(isUseBlend)
       boneWorld = SkinWorld(input.blendIndices, input.blendWeights);
    else
       boneWorld = bones[boneIndex];
    
    output.position = mul(input.position, boneWorld);
    output.position = mul(output.position, view);
    output.position = mul(output.position, projection);
    
    output.vPosition = mul(input.position, boneWorld);
    output.vPosition = mul(output.vPosition, LightV);
    output.vPosition = mul(output.vPosition, LightP);
        
    output.normal = mul(input.normal, (float3x3) boneWorld);
    output.uv = input.uv;
    
    return output;
}

cbuffer Shadow : register(b10)
{
    float2 mapSize;
    
    float bias;
    int selected;
}

Texture2D depthMap : register(t10);
SamplerComparisonState depthSampler : register(s11);

float4 PS(PixelInput input) : SV_Target
{
    input.vPosition.xyz /= input.vPosition.w;
    
    float4 color = 0;
    float4 albedo = diffuseMap.Sample(diffuseSamp, input.uv);
    
    float3 normal = normalize(input.normal);
    float3 light = normalize(lightDirection);
    
    float diffuseIntensity = saturate(dot(normal, -light));
    
    color = albedo * diffuseIntensity;
 
    if(input.vPosition.x < -1.0f || input.vPosition.x > 1.0f || 
        input.vPosition.y < -1.0f || input.vPosition.y > 1.0f || 
        input.vPosition.z < 0.0f || input.vPosition.z > 1.0f)
        return color;
    
    input.vPosition.x = input.vPosition.x * 0.5f + 0.5f;
    input.vPosition.y = -input.vPosition.y * 0.5f + 0.5f;
    input.vPosition.z -= bias;
    
    float depth = 0;
    float factor = 0;
    
    [flatten]
    if(selected == 0)
    {
        depth = depthMap.Sample(diffuseSamp, input.vPosition.xy).r;
        factor = (float) input.vPosition.z <= depth;
    }else if(selected == 1)
    {
        depth = depthMap.SampleCmpLevelZero(depthSampler, input.vPosition.xy,
        input.vPosition.z).r;
        factor = input.vPosition.z <= depth;
    }else if(selected == 2)
    {
        float avg = 0;
        float sum = 0;
        
        for (float y = -0.5f; y <= 0.5f; y += 1.0f)
        {
            for (float x = -0.5f; x <= 0.5f; x += 1.0f)
            {
                float2 offset = float2(x / mapSize.x, y / mapSize.y);
                sum += depthMap.SampleCmpLevelZero(depthSampler, input.vPosition.xy + offset,
                input.vPosition.z).r;
                avg++;
            }
        }
        factor = sum / avg;
    }
    
    factor = saturate(factor + depth);
    
    color.rgb *= factor;
    
    return float4(color.rgb, 1);
}