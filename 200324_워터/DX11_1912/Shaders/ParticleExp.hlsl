#include "Header.hlsli"

cbuffer VS : register(b10)
{
    float duration;
    float age;
    float gravity;
}

cbuffer PS : register(b10)
{
    float4 startColor;
    float4 endColor;
}

Texture2D map : register(t10);
SamplerState samp : register(s10);

struct VertexInput
{
    float4 position : POSITION;
    float2 size : SIZE;
    float3 velocity : VELOCITY;
};

struct VertexOutput
{
    float3 position : POSITION;
    float2 size : SIZE;
    float time : TIME;
};

struct PixelInput
{
    float4 position : SV_Position;
    float2 uv : UV;
    float time : TIME;
};

VertexOutput VS(VertexInput input)
{
    VertexOutput output;
    
    output.time = age / duration;
    
    input.velocity.y -= gravity * age;
    
    output.position = input.position.xyz + input.velocity * age;
    output.position = mul(output.position, (float3x3) world);   
    
    output.size = input.size;
    
    return output;
}

static const float2 TEXCOORD[4] =
{
    float2(0.0f, 1.0f),
    float2(0.0f, 0.0f),
    float2(1.0f, 1.0f),
    float2(1.0f, 0.0f)
};

[maxvertexcount(4)]
void GS(point VertexOutput input[1], inout TriangleStream<PixelInput> stream)
{
    float3 up = float3(0, 1, 0);
    float3 forward = CamPos() - input[0].position;
    
    forward.y = 0.0f;
    forward = normalize(forward);

    float3 right = normalize(cross(up, forward));
    
    float halfWidth = input[0].size.x * 0.5f;
    float halfHeight = input[0].size.y * 0.5f;
    
    float4 vertices[4];
    vertices[0] = float4(input[0].position + halfWidth * right - halfHeight * up, 1.0f);
    vertices[1] = float4(input[0].position + halfWidth * right + halfHeight * up, 1.0f);
    vertices[2] = float4(input[0].position - halfWidth * right - halfHeight * up, 1.0f);
    vertices[3] = float4(input[0].position - halfWidth * right + halfHeight * up, 1.0f);       
    
    PixelInput output;
    [unroll]
    for (int i = 0; i < 4; i++)
    {
        output.position = mul(vertices[i], view);
        output.position = mul(output.position, projection);
        
        output.uv = TEXCOORD[i];
        output.time = input[0].time;
        
        stream.Append(output);
    }
}

float4 PS(PixelInput input) : SV_TARGET
{
    float4 albedo = map.Sample(samp, input.uv);
    
    float4 color = lerp(startColor, endColor, input.time);
    
    return albedo * color;
}