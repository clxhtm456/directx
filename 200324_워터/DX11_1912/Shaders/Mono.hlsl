#include "Header.hlsli"

cbuffer Color : register(b10)
{
    float4 color;
}

Texture2D map : register(t10);
SamplerState samp : register(s10);

struct PixelInput
{
    float4 position : SV_Position;
    float2 uv : UV;
};

PixelInput VS(VertexUV input)
{
    PixelInput output;
    output.position = mul(input.position, world);
    output.position = mul(output.position, view);
    output.position = mul(output.position, projection);
    
    output.uv = input.uv;
    
    return output;
}

float4 PS(PixelInput input) : SV_TARGET
{
    float4 albedo = map.Sample(samp, input.uv);
    
    //float average = (albedo.r + albedo.g + albedo.b) / 3;
    /*
    float3 gray;
    gray.r = albedo.r * 0.3f;
    gray.g = albedo.g * 0.59f;
    gray.b = albedo.b * 0.11f;    
    float scale = gray.r + gray.g + gray.b;
    */
    //float scale = dot(albedo.rgb, float3(0.3f, 0.59f, 0.11f));    
    //return float4(scale.xxx, 1) * color;
    
    //Sepia
    float4 sepia;
    sepia.a = albedo.a;
    
    sepia.r = dot(albedo.rgb, float3(0.393f, 0.769f, 0.189f));
    sepia.g = dot(albedo.rgb, float3(0.349f, 0.686f, 0.168f));
    sepia.b = dot(albedo.rgb, float3(0.272f, 0.534f, 0.131f));
    
    return sepia * color;
}