#include "Header.hlsli"

struct PixelInput
{
    float4 position : SV_POSITION;    
};

PixelInput VS(VertexUVNormalTangentBlend input)
{
    PixelInput output;
    matrix boneWorld = SkinWorld(input.blendIndices, input.blendWeights);
    
    output.position = mul(input.position, boneWorld);
    output.position = mul(output.position, view);
    output.position = mul(output.position, projection);    
    
    return output;
}

float4 PS(PixelInput input) : SV_Target
{
    float depth = input.position.z / input.position.w;    
    
    return float4(depth.xxx, 1);
}