#include "Header.hlsli"

cbuffer PostEffect : register(b10)
{
    int count;
    float width;
    float height;
    int select;
}    

Texture2D map : register(t10);
SamplerState samp : register(s10);

struct PixelInput
{
    float4 position : SV_Position;
    float2 uv : UV;
};

PixelInput VS(VertexUV input)
{
    PixelInput output;
    output.position = mul(input.position, world);
    output.position = mul(output.position, view);
    output.position = mul(output.position, projection);
    
    output.uv = input.uv;
    
    return output;
}

float4 CrossBlur(float2 uv)
{
    float4 color = 0;
    
    for (int i = 0; i < count; i++)
    {
        float divX = (1 + i) / width;
        float divY = (1 + i) / height;
        
        color += map.Sample(samp, float2(uv.x + divX, uv.y));
        color += map.Sample(samp, float2(uv.x - divX, uv.y));
        color += map.Sample(samp, float2(uv.x, uv.y + divY));
        color += map.Sample(samp, float2(uv.x, uv.y - divY));
    }

    color /= count * 4;
    
    return color;
}

static const float2 arr[8] =
{
    float2(-1, -1), float2(0, -1), float2(1, -1),
    float2(-1, 0), float2(1, 0),
    float2(-1, 1), float2(0, 1), float2(1, 1)
};

float4 OctaBlur(float2 uv)
{
    float4 color = 0;
    
    for (int i = 0; i < count; i++)
    {
        float divX = (1 + i) / width;
        float divY = (1 + i) / height;
        
        for (int j = 0; j < 8; j++)
        {
            float x = arr[j].x * divX + uv.x;
            float y = arr[j].y * divY + uv.y;
            
            color += map.Sample(samp, float2(x, y));
        }        
    }

    color /= count * 8;
    
    return color;
}

static const float weights[13] =
{
    0.0561f, 0.1353f, 0.2780f, 0.4868f, 0.7261f, 0.9231f,
    1.0f,
    0.9231f, 0.7261f, 0.4868f, 0.2780f, 0.1353f, 0.0561f
};

float4 GaussianBlur(float2 uv)
{
    float divX = 1.0f / width;
    float divY = 1.0f / height;
    
    float sum = 0;
    float4 color = 0;
    
    for (int i = -6; i <= 6; i++)
    {
        float2 temp = uv + float2(divX * i * count, 0);
        color += weights[6 + i] * map.Sample(samp, temp);
        
        temp = uv + float2(0, divY * i * count);
        color += weights[6 + i] * map.Sample(samp, temp);
        
        sum += weights[6 + i] * 2;
    }

    color /= sum;
    
    return color;
}

float4 Mosaic(float2 uv)
{
    int x = (int) (uv.x * count);
    int y = (int) (uv.y * count);
    
    float2 temp;
    temp.x = (float) x / count;
    temp.y = (float) y / count;
    
    return map.Sample(samp, temp);
}

//width = radius;
//height = amount;
float4 RadialBlur(float2 uv)
{
    float2 radiusUV = uv - float2(0.5f, 0.5f);
    float r = length(radiusUV);
    radiusUV /= r;
    
    r = saturate(2 * r / width);
    
    float2 delta = -radiusUV * r * r * height / count;    
    
    float4 color = 0;
    
    for (int i = 0; i < count; i++)
    {
        color += map.Sample(samp, uv);
        uv += delta;
    }
    
    color /= count;

    return color;
}

float4 Outline(float2 uv)
{
    float sum = 0;
    
    for (int i = 1; i <= count; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            float2 temp = float2(1.0f / width * i, 1.0f / height * i);
            temp *= arr[j];
            
            float4 albedo = map.Sample(samp, uv + temp);
            
            sum += albedo.a;
        }
    }

    sum /= count * 8;
    
    if(sum > 0.0f && sum < 1.0f)
        return float4(1.0f, 0.0f, 0.0f, 1.0f);
    
    return map.Sample(samp, uv);
}

float4 PS(PixelInput input) : SV_TARGET
{
    if(select == 1)
        return CrossBlur(input.uv);
    else if (select == 2)
        return OctaBlur(input.uv);
    else if (select == 3)
        return GaussianBlur(input.uv);
    else if (select == 4)
        return Mosaic(input.uv);
    else if (select == 5)
        return RadialBlur(input.uv);
    else if (select == 6)
        return Outline(input.uv);
    
    return map.Sample(samp, input.uv);
}