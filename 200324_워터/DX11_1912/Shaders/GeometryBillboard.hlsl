//GeometryBillboard.hlsl
#include "Header.hlsli"

cbuffer Data : register(b10)
{
    float time;
}

Texture2D map : register(t10);
SamplerState samp : register(s10);

struct VertexInput
{
    float4 position : POSITION;
    float2 size : SIZE;
    float3 dir : DIR;
};

struct VertexOutput
{
    float3 position : POSITION;
    float2 size : SIZE;
    float3 dir : DIR;
};

struct PixelInput
{
    float4 position : SV_Position;
    float2 uv : UV;
    float4 color : COLOR;
};

VertexOutput VS(VertexInput input)
{
    VertexOutput output;
    
    output.position = input.position.xyz;
    
    //output.position.x += sin(time);
    //output.position += input.dir * sin(time);
    
    output.size = input.size;
    output.dir = input.dir;
    
    return output;
}

static const float2 TEXCOORD[4] =
{
    float2(0.0f, 1.0f),
    float2(0.0f, 0.0f),
    float2(1.0f, 1.0f),
    float2(1.0f, 0.0f)
};

[maxvertexcount(4)]
void GS(point VertexOutput input[1], inout TriangleStream<PixelInput> stream)
{
    float3 up = float3(0, 1, 0);
    float3 forward = CamPos() - input[0].position;
    
    forward.y = 0.0f;
    forward = normalize(forward);

    float3 right = normalize(cross(up, forward));
    
    float halfWidth = input[0].size.x * 0.5f;
    float halfHeight = input[0].size.y * 0.5f;
    
    float4 vertices[4];
    vertices[0] = float4(input[0].position + halfWidth * right - halfHeight * up, 1.0f);
    vertices[1] = float4(input[0].position + halfWidth * right + halfHeight * up, 1.0f);    
    vertices[2] = float4(input[0].position - halfWidth * right - halfHeight * up, 1.0f);
    vertices[3] = float4(input[0].position - halfWidth * right + halfHeight * up, 1.0f);
    
    vertices[1].x += sin(time);
    vertices[3].x += sin(time);
    
    PixelInput output;
    
    float4 color;
    color.r = Random(input[0].dir.x, 0.0f, 1.0f);
    color.g = Random(input[0].dir.y, 0.0f, 1.0f);
    color.b = Random(input[0].dir.z, 0.0f, 1.0f);
    color.a = 1.0f;
    
    [unroll]
    for (int i = 0; i < 4; i++)
    {
        output.position = mul(vertices[i], view);
        output.position = mul(output.position, projection);
        
        output.uv = TEXCOORD[i];
        output.color = color;
        
        stream.Append(output);
    }
}

float4 PS(PixelInput input) : SV_TARGET
{   
    return map.Sample(samp, input.uv) * input.color;
}