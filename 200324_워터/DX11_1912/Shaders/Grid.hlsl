cbuffer VP : register(b0)
{
    matrix view;
    matrix projection;
    matrix invView;
}

cbuffer W : register(b1)
{
    matrix world;
}

cbuffer Light : register(b0)
{
    float3 lightDirection;
    float specExp;
}

Texture2D map : register(t0);
SamplerState samp : register(s0);

struct VertexInput
{
    float4 position : POSITION; 
    float2 uv : UV;
    float3 normal : NORMAL;
};

struct PixelInput
{
    float4 position : SV_POSITION;    
    float2 uv : UV;
    float3 normal : NORMAL;
    float3 viewDir : VIEWDIR;
};

PixelInput VS(VertexInput input)
{
    PixelInput output;
    output.position = mul(input.position, world);
    
    float3 camPos = invView._41_42_43;
    output.viewDir = normalize(output.position.xyz - camPos);
    
    output.position = mul(output.position, view);
    output.position = mul(output.position, projection);    
    output.uv = input.uv;
    
    output.normal = mul(input.normal, (float3x3) world);
    
    return output;
}

float4 PS(PixelInput input) : SV_TARGET
{
    float4 albedo = map.Sample(samp, input.uv);
    
    float3 light = normalize(lightDirection);
    float3 normal = normalize(input.normal);
    
    float diffuse = saturate(dot(normal, -light));
    
    float specular = 0;
    if(diffuse > 0)
    {
        //�� ���̵�
        //float3 reflection = normalize(reflect(light, normal));
        //specular = saturate(dot(reflection, -input.viewDir));
        
        //���� �� ���̵�
        float3 halfWay = normalize(input.viewDir + light);
        specular = saturate(dot(-halfWay, normal));
        
        specular = pow(specular, specExp);
    }
    
    return float4(albedo.rgb * diffuse + specular.xxx, 1);
}