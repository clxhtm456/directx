//Texture2DArray<float4> input;
Texture2D input;
RWTexture2DArray<float4> output;

[numthreads(32, 32, 1)]
void CS( uint3 DTid : SV_DispatchThreadID )
{
    //float4 color = input.Load(int4(DTid, 0));
    float4 color = input.Load(DTid);
    
    //output[DTid] = float4(1.0f - color.rgb, 1);
    output[DTid] = float4(((color.r + color.g + color.b) / 3.0f).xxx, 1);
    //output[DTid] = color;
}