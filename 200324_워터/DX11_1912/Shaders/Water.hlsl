#include "Header.hlsli"

cbuffer VS_Ocean : register(b10)
{
    float waveFrequency;
    float waveAmplitude;
    
    float2 textureScale;
    
    float2 bumpSpeed;
    float bumpHeight;
    float runningTime;
}

cbuffer PS_Ocean : register(b10)
{
    float4 shallowColor;
    float4 deepColor;
    
    float fresnelBias;//편향
    float fresnelPower;//강도
    float fresnelAmount;//보정값
    float shoreBlend;//알파값 변화정도
    
    float2 oceanSize;
    float heightRatio;
}

struct PixelInput
{
    float4 position : SV_POSITION;
    float4 oPosition : POSITION0;
    float4 wPosition : POSITION1;
    float2 texScale : TEXSCALE;
    float3 normal : NORMAL;
    float3 view : VIEW;
    
    float2 bump[3] : BUMP;
    float3 tangent[3] : TANGENT;
};

struct Wave
{
    float frequency;//파동
    float amplitude;//진폭
    float phase;//위상
    float2 direction;
};

float EvaluateWave(Wave wave, float2 position, float time)
{
    float s = sin(dot(wave.direction, position) * wave.frequency + time) + wave.phase;
    
    return wave.amplitude * s;
}

float EvaluateDifferent(Wave wave, float2 position, float time)
{
    float s = cos(dot(wave.direction, position) * wave.frequency + time) + wave.phase;
    
    return wave.amplitude * wave.frequency * s;
}

PixelInput VS(VertexUV input)
{
    PixelInput output;
    
    Wave wave[3] =
    {
        waveFrequency * 1.0f, waveAmplitude * 1.0f, 1.50f, float2(-1.0f, 0.0f),
        waveFrequency * 2.0f, waveAmplitude * 0.5f, 2.30f, float2(-0.7f, -0.7f),
        waveFrequency * 3.0f, waveAmplitude * 1.0f, 1.25f, float2(0.2f, 0.1f)
    };
    
    float ddx = 0, ddy = 0;
    
    for (int i = 0; i < 3; i++)
    {
        input.position.y += EvaluateWave(wave[i], input.position.xz, runningTime);
        
        float diff = EvaluateDifferent(wave[i], input.position.xz, runningTime);
        ddx += diff * wave[i].direction.x;
        ddy += diff * wave[i].direction.y;
    }
    
    float3 T = float3(1, ddx, 0);
    float3 B = float3(-ddx, 1, -ddy);
    float3 N = float3(0, ddy, 1);
    
    float3x3 matTangent = float3x3(
            normalize(T) * bumpHeight,
            normalize(B) * bumpHeight,
            normalize(T)
    );
    
    output.tangent[0] = mul(world[0].xyz, matTangent);
    output.tangent[1] = mul(world[1].xyz, matTangent);
    output.tangent[2] = mul(world[2].xyz, matTangent);
    
    output.texScale = input.uv * textureScale;
    
    float tempTime = fmod(runningTime, 100);
    output.bump[0] = output.texScale + tempTime * bumpSpeed;
    output.bump[1] = output.texScale * 2.0f + tempTime * bumpSpeed * 4.0f;
    output.bump[2] = output.texScale * 4.0f + tempTime * bumpSpeed * 8.0f;
    
    output.oPosition = input.position;
    output.position = mul(input.position, world);
    output.wPosition = output.position;
    output.position = mul(output.position, view);
    output.position = mul(output.position, projection);
    
    output.view = output.wPosition.xyz - CamPos();
    
    return output;
}

Texture2D heightMap : register(t10);
SamplerState heightSampler : register(s10);

bool WithInBound(float3 position)
{
    return (position.x > 0.0f && position.z > 0.0f && position.x < oceanSize.x 
    && position.z < oceanSize.y);
}

float EvaluateShoreBlend(float3 position)
{
    float2 temp = float2(position.x / oceanSize.x, position.z / oceanSize.y);
    float color = heightMap.Sample(heightSampler, temp).r / heightRatio;
    
    return 1.0f - color * shoreBlend;
}

float4 PS(PixelInput input) : SV_TARGET
{
    float4 t0 = normalMap.Sample(normalSamp, input.bump[0]) * 2.0f - 1.0f;
    float4 t1 = normalMap.Sample(normalSamp, input.bump[1]) * 2.0f - 1.0f;
    float4 t2 = normalMap.Sample(normalSamp, input.bump[2]) * 2.0f - 1.0f;
    
    float3 normal = t0.xyz + t1.xyz + t2.xyz;
    
    float3x3 matTangent;
    matTangent[0] = input.tangent[0];
    matTangent[1] = input.tangent[1];
    matTangent[2] = input.tangent[2];
    
    normal = normalize(mul(normal, matTangent));
    
    float facing = 1.0f - saturate(dot(input.view, normal));
    float fresnel = fresnelBias + (1.0f - fresnelBias) * pow(facing, fresnelPower);
    
    float alpha = 0;
    float4 color = 0;
    
    color = lerp(deepColor, shallowColor, facing);
    
    if (shoreBlend > 0 && WithInBound(input.oPosition.xyz))
    {
        alpha = EvaluateShoreBlend(input.oPosition.xyz);
        color.rgb = lerp(2, color.rgb, color.a);
    }

    color.rgb = color.rgb * fresnel * mAmbient.rgb;
    
    float3 light = normalize(lightDirection);
    
    float diffuseIntensity = saturate(dot(normal, -light));
    
    float specularIntensity = 0;
    if (diffuseIntensity > 0)
    {
        float3 halfWay = normalize(input.view + light);
        specularIntensity = saturate(dot(-halfWay, normal));
        
        specularIntensity = pow(specularIntensity, specExp);
    }
    
    float4 result = color * diffuseIntensity * mDiffuse + color * specularIntensity * mSpecular;
    result.a = alpha;
    
    return result;
}