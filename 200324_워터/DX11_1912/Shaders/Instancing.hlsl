#include "Header.hlsli"

struct VertexInput
{
    float4 position : POSITION;
    float2 uv : UV;
    float3 normal : NORMAL;
    
    matrix transform : INSTANCE;
    uint instanceID : SV_InstanceID;
};

struct PixelInput
{
    float4 position : SV_Position;
    float2 uv : UV;
    float3 normal : NORMAL;
    float4 color : COLOR;
};

PixelInput VS(VertexInput input)
{
    PixelInput output;
    
    output.position = mul(input.position, input.transform);
    //output.position = mul(input.position, world);
    output.position = mul(output.position, view);
    output.position = mul(output.position, projection);
    output.uv = input.uv;
    
    output.normal = mul(input.normal, (float3x3) input.transform);
    //output.normal = mul(input.normal, (float3x3) world);
    
    if(input.instanceID % 3 == 0)
    {
        output.color = float4(1, 0, 0, 1);
    }
    else if (input.instanceID % 3 == 1)
    {
        output.color = float4(0, 1, 0, 1);
    }
    else if (input.instanceID % 3 == 2)
    {
        output.color = float4(0, 0, 1, 1);
    }
    
    return output;
}

float4 PS(PixelInput input) : SV_TARGET
{
    float4 albedo = diffuseMap.Sample(diffuseSamp, input.uv);
    
    float diffuse = dot(normalize(input.normal), normalize(-lightDirection));
    
    return albedo * diffuse * input.color;
}