#include "Header.hlsli"

cbuffer Sky : register(b10)
{
    float4 center;
    float4 apex;
    
    float height;
}

struct PixelInput
{
    float4 position : SV_Position;
    float4 oPosition : POSITION;
};

PixelInput VS(Vertex input)
{
    PixelInput output;
    output.position = mul(input.position, world);
    output.position = mul(output.position, view);
    output.position = mul(output.position, projection);
    
    output.oPosition = input.position;
    
    return output;
}

float4 PS(PixelInput input) : SV_TARGET
{
    float y = saturate(input.oPosition.y);
    
    return lerp(center, apex, y * height);
}