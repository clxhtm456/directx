#include "Header.hlsli"

cbuffer Point : register(b11)
{
    float3 pointLightPosition;
    float pointLightRange;
    float4 pointLightColor;
}

cbuffer Spot : register(b12)
{
    float4 spotLightColor;
    float3 spotLightPosition;
    float spotLightRange;
    float3 spotLightDirection;
    float spotLightOuter;
    float spotLightInner;
}

cbuffer Capsule : register(b9)
{
    float4 capsuleLightColor;
    float3 capsuleLightPosition;
    float capsuleLightRange;
    float3 capsuleLightDirection;
    float capsuleLightLength;
}

struct PixelInput
{
    float4 position : SV_Position;    
    float2 uv : UV;
    float3 normal : NORMAL;
    float4 wPosition : POSITION0;
    float3 cPosition : POSITION1;
};

PixelInput VS(VertexUVNormal input)
{
    PixelInput output;
    matrix boneWorld = bones[boneIndex];
    
    output.position = mul(input.position, boneWorld);
    output.wPosition = output.position;
    output.position = mul(output.position, view);
    output.position = mul(output.position, projection);
    output.uv = input.uv;
    
    output.normal = mul(input.normal, (float3x3) boneWorld);
    output.cPosition = CamPos();
    
    return output;
}

float3 CalcAmbient(float3 normal, float3 color)
{
    float up = normal.y * 0.5f + 0.5f;
    
    float3 ambient = ambientFloor + up * ambientCeil;
    
    return ambient * color;
}

float3 CalcPoint(float3 wPosition, float3 cPosition, float3 normal)
{
    float3 toLight = pointLightPosition.xyz - wPosition;
    float distanceToLight = length(toLight);
    toLight /= distanceToLight;
    
    //Diffuse
    float diffuseIntensity = dot(toLight, normal);
    float3 color = pointLightColor.rgb * saturate(diffuseIntensity);
    
    //Specualr
    float3 toEye = normalize(cPosition - wPosition);
    float3 halfWay = normalize(toEye + toLight);
    float specularIntensity = saturate(dot(halfWay, normal));
    color += pointLightColor.rgb * pow(specularIntensity, specExp) * mSpecular.rgb;
    
    //Attenunation
    float distanceToLightNormal = 1.0f - saturate(distanceToLight / pointLightRange);
    float attention = distanceToLightNormal * distanceToLightNormal;
    color *= attention * mDiffuse.rgb;
    
    return color;
}

float3 CalcSpot(float3 wPosition, float3 cPosition, float3 normal)
{
    //SpotLight
    float3 toLight = spotLightPosition.xyz - wPosition;
    float distanceToLight = length(toLight);
    toLight /= distanceToLight;
    
    //Diffuse
    float diffuseIntensity = dot(toLight, normal);
    float3 color = spotLightColor.rgb * saturate(diffuseIntensity);
    
    //Specular
    float3 toEye = normalize(cPosition - wPosition);
    float3 halfWay = normalize(toEye + toLight);
    float specularIntensity = saturate(dot(halfWay, normal));
    color += spotLightColor.rgb * pow(specularIntensity, specExp) * mSpecular.rgb;
    
    //Attenuation
    float3 dir = -normalize(spotLightDirection);
    float cosAngle = dot(dir, toLight);
    
    float outer = cos(radians(spotLightOuter));
    float inner = 1.0f / cos(radians(spotLightInner));
    
    float conAttention = saturate((cosAngle - outer) * inner);
    conAttention *= conAttention;
    
    float distanceToLightNormal = 1.0f - saturate(distanceToLight / spotLightRange);
    float attention = distanceToLightNormal * distanceToLightNormal;
    color *= attention * conAttention * mDiffuse.rgb;
    
    return color;
}

float3 CalcCapsule(float3 wPosition, float3 cPosition, float3 normal)
{
    //CapsuleLight
    float3 direction = normalize(capsuleLightDirection);
    float3 start = wPosition - capsuleLightPosition;
    float distanceOnLine = dot(start, direction) / capsuleLightLength;
    distanceOnLine = saturate(distanceOnLine) * capsuleLightLength;
    
    float3 pointOnLine = capsuleLightPosition + direction * distanceOnLine;
    float3 toLight = pointOnLine - wPosition;
    float distanceToLight = length(toLight);
    toLight /= distanceToLight;
    
    //Diffuse
    float diffuseIntensity = dot(toLight, normal);
    float3 color = capsuleLightColor.rgb * saturate(diffuseIntensity);
    
    //Specualr
    float3 toEye = normalize(cPosition - wPosition);
    float3 halfWay = normalize(toEye + toLight);
    float specularIntensity = saturate(dot(halfWay, normal));
    color += capsuleLightColor.rgb * pow(specularIntensity, specExp) * mSpecular.rgb;
    
    //Attenunation
    float distanceToLightNormal = 1.0f - saturate(distanceToLight / capsuleLightRange);
    float attention = distanceToLightNormal * distanceToLightNormal;
    color *= attention * mDiffuse.rgb;
    
    return color;
}

float4 PS(PixelInput input) : SV_TARGET
{
    float4 color = diffuseMap.Sample(diffuseSamp, input.uv);
    
    float3 normal = normalize(input.normal);
    
    float3 ambient = CalcAmbient(normal, color.rgb) * mAmbient.rgb;
    //float3 pointColor = CalcPoint(input.wPosition.xyz, input.cPosition, normal);        
    //float3 spotColor = CalcSpot(input.wPosition.xyz, input.cPosition, normal);
    float3 capsuleColor = CalcCapsule(input.wPosition.xyz, input.cPosition, normal);
    
    return float4(capsuleColor + ambient, 1);
}