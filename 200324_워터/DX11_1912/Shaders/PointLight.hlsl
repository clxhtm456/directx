#include "Header.hlsli"

struct PixelInput
{
    float4 position : SV_Position;
    float2 uv : UV;
    float3 normal : NORMAL;
    float4 wPosition : POSITION0;
    float3 cPosition : POSITION1;
};

PixelInput VS(VertexUVNormal input)
{
    PixelInput output;
    matrix boneWorld = bones[boneIndex];
    
    output.position = mul(input.position, boneWorld);
    output.wPosition = output.position;
    output.position = mul(output.position, view);
    output.position = mul(output.position, projection);
    output.uv = input.uv;
    
    output.normal = mul(input.normal, (float3x3) boneWorld);
    output.cPosition = CamPos();
    
    return output;
}

float3 CalcAmbient(float3 normal, float3 color)
{
    float up = normal.y * 0.5f + 0.5f;
    
    float3 ambient = ambientFloor + up * ambientCeil;
    
    return ambient * color;
}

void CalcPoint(inout float3 result, PointLight light, float3 wPosition, float3 cPosition, float3 normal)
{
    float3 toLight = light.position.xyz - wPosition;
    float distanceToLight = length(toLight);
    toLight /= distanceToLight;
    
    //Diffuse
    float diffuseIntensity = dot(toLight, normal);
    float3 color = light.color.rgb * saturate(diffuseIntensity);
    
    //Specualr
    float3 toEye = normalize(cPosition - wPosition);
    float3 halfWay = normalize(toEye + toLight);
    float specularIntensity = saturate(dot(halfWay, normal));
    color += light.color.rgb * pow(specularIntensity, specExp) * mSpecular.rgb;
    
    //Attenunation
    float distanceToLightNormal = 1.0f - saturate(distanceToLight / light.range);
    float attention = distanceToLightNormal * distanceToLightNormal;
    color *= attention * mDiffuse.rgb;
    
    result += color;
}

float4 PS(PixelInput input) : SV_TARGET
{
    float4 color = diffuseMap.Sample(diffuseSamp, input.uv);
    
    float3 normal = normalize(input.normal);
    
    float3 pointColor = 0;
    
    for (int i = 0; i < pointLightCount; i++)
    {
        CalcPoint(pointColor, pointLights[i], input.wPosition.xyz, input.cPosition, normal);
    }
    
    color.rgb *= pointColor;
    
    return color;
}