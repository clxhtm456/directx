cbuffer VP : register(b0)
{    
    matrix view;
    matrix projection;
}

cbuffer W : register(b1)
{
    matrix world;    
}

cbuffer Color : register(b0)
{
    float4 color;
}

Texture2D map : register(t0);
SamplerState samp : register(s0);

struct VertexInput
{
    float4 position : POSITION;    
    float2 uv : UV;
};

struct PixelInput
{
    float4 position : SV_POSITION;    
    float2 uv : UV;
};

PixelInput VS(VertexInput input)
{
    PixelInput output;
    output.position = mul(input.position, world);     
    output.position = mul(output.position, view);
    output.position = mul(output.position, projection);
    output.uv = input.uv;
    
    return output;
}

float4 PS(PixelInput input) : SV_TARGET
{
    return map.Sample(samp, input.uv) * color;    
}