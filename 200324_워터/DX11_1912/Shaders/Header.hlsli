#define MAX_BONE 256
#define MAX_KEY 500

#define MAX_POINTLIGHT 32

cbuffer VP : register(b0)
{
    matrix view;
    matrix projection;
    matrix invView;
}

cbuffer W : register(b1)
{
    matrix world;
}

cbuffer Bones : register(b2)
{
    matrix bones[MAX_BONE];
    
    int index;
    int isUseBlend;
}

cbuffer BoneIndex : register(b3)
{
    int boneIndex;
}

cbuffer Light : register(b0)
{
    float3 lightDirection;
    float specExp;
    
    float4 ambientLight;
    
    int isSpecularMap;
    int isNormalMap;
}

cbuffer Material : register(b1)
{
    float4 mDiffuse;
    float4 mSpecular;
    float4 mAmbient;
}

cbuffer Ambient : register(b2)
{
    float4 ambientFloor;
    float4 ambientCeil;
}

struct PointLight
{
    float3 position;
    float range;
    float4 color;
};

cbuffer Point : register(b3)
{
    PointLight pointLights[MAX_POINTLIGHT];
    int pointLightCount;
}

Texture2D diffuseMap : register(t0);
SamplerState diffuseSamp : register(s0);

Texture2D specularMap : register(t1);
SamplerState specularSamp : register(s1);

Texture2D normalMap : register(t2);
SamplerState normalSamp : register(s2);

struct Vertex
{
    float4 position : POSITION;    
};

struct VertexUV
{
    float4 position : POSITION;
    float2 uv : UV;    
};

struct VertexSize
{
    float4 position : POSITION;
    float2 size : SIZE;
};

struct VertexUVNormal
{
    float4 position : POSITION;
    float2 uv : UV;
    float3 normal : NORMAL;
};

struct VertexUVNormalTangent
{
    float4 position : POSITION;
    float2 uv : UV;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
};

struct VertexUVNormalTangentBlend
{
    float4 position : POSITION;
    float2 uv : UV;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float4 blendIndices : BLENDINDICES;
    float4 blendWeights : BLENDWEIGHTS;
};

float3 CamPos()
{
    return invView._41_42_43;
}

float Random(float seed, float min, float max)
{
    float value = frac(sin(seed) * 10000.0f);
    float random = value * (max - min);
    
    return min + random;
}

matrix SkinWorld(float4 indices, float4 weights)
{
    matrix transform = 0;
    transform += mul(weights.x, bones[(uint) indices.x]);
    transform += mul(weights.y, bones[(uint) indices.y]);
    transform += mul(weights.z, bones[(uint) indices.z]);
    transform += mul(weights.w, bones[(uint) indices.w]);
    
    return transform;
}