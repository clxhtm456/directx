#include "Header.hlsli"

TextureCube cubeMap : register(t10);
SamplerState samp : register(s10);

struct PixelInput
{
    float4 position : SV_Position;
    float4 oPosition : POSITION;    
};

PixelInput VS(Vertex input)
{
    PixelInput output;
    output.position = mul(input.position, world);
    output.position = mul(output.position, view);
    output.position = mul(output.position, projection);
    
    output.oPosition = input.position;
    
    return output;
}

float4 PS(PixelInput input) : SV_TARGET
{   
    return cubeMap.Sample(samp, input.oPosition.xyz);
}