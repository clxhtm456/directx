#pragma once

struct Vertex
{
	Vector3 position;

	Vertex()
	{
		position = Vector3(0, 0, 0);
	}
	Vertex(float x, float y, float z)
	{
		position = Vector3(x, y, z);
	}
};

struct VertexUV
{
	Vector3 position;
	Vector2 uv;

	VertexUV()
	{
		position = Vector3(0, 0, 0);
		uv = Vector2(0, 0);
	}
};

struct VertexSize
{
	Vector3 position;
	Vector2 size;

	VertexSize()
	{
		position = Vector3(0, 0, 0);
		size = Vector2(0, 0);
	}
};

struct VertexUVNormal
{
	Vector3 position;
	Vector2 uv;
	Vector3 normal;

	VertexUVNormal()
	{
		position = Vector3(0, 0, 0);
		uv = Vector2(0, 0);
		normal = Vector3(0, 0, 0);
	}

	VertexUVNormal(float x, float y, float z, float u, float v, float nx, float ny, float nz)
	{
		position = Vector3(x, y, z);
		uv = Vector2(u, v);
		normal = Vector3(nx, ny, nz);
	}
};

struct VertexUVNormalTangent
{
	Vector3 position;
	Vector2 uv;
	Vector3 normal;
	Vector3 tangent;

	VertexUVNormalTangent()
	{
		position = Vector3(0, 0, 0);
		uv = Vector2(0, 0);
		normal = Vector3(0, 0, 0);
		tangent = Vector3(0, 0, 0);
	}
};

struct VertexUVNormalTangentAlpha
{
	Vector3 position;
	Vector2 uv;
	Vector3 normal;
	Vector3 tangent;
	Color alpha;

	VertexUVNormalTangentAlpha()
	{
		position = Vector3(0, 0, 0);
		uv = Vector2(0, 0);
		normal = Vector3(0, 0, 0);
		tangent = Vector3(0, 0, 0);
		alpha = Color(0, 0, 0, 0);
	}
};

struct VertexUVNormalTangentBlend
{
	Vector3 position;
	Vector2 uv;
	Vector3 normal;
	Vector3 tangent;
	Vector4 blendIndices;
	Vector4 blendWeights;

	VertexUVNormalTangentBlend()
	{
		position = Vector3(0, 0, 0);
		uv = Vector2(0, 0);
		normal = Vector3(0, 0, 0);
		tangent = Vector3(0, 0, 0);
		blendIndices = Vector4(0, 0, 0, 0);
		blendWeights = Vector4(0, 0, 0, 0);
	}
};
