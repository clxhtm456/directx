#include "Framework.h"

ComputeShader::ComputeShader(wstring file)
{
	file = L"Shaders/" + file + L".hlsl";

	UINT flags = D3DCOMPILE_ENABLE_STRICTNESS | D3DCOMPILE_DEBUG;

	ID3DBlob* blob;

	D3DX11CompileFromFile(file.c_str(), nullptr, nullptr,
		"CS", "cs_5_0", flags, 0, nullptr, &blob, nullptr, nullptr);

	HRESULT hr = DEVICE->CreateComputeShader(blob->GetBufferPointer(),
		blob->GetBufferSize(), nullptr, &shader);
	DEBUG(hr);

	blob->Release();
}

ComputeShader::~ComputeShader()
{
	shader->Release();
}

void ComputeShader::Set()
{
	DC->CSSetShader(shader, nullptr, 0);
}
