#pragma once

class ComputeShader
{
private:
	ID3D11ComputeShader* shader;

public:
	ComputeShader(wstring file);
	~ComputeShader();

	void Set();
};