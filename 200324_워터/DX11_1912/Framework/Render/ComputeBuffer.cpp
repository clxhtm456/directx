#include "Framework.h"

RawBuffer::RawBuffer(void* inputData, UINT byteWidth)
	: inputData(inputData), byteWidth(byteWidth)
{
	CreateBuffer();
}

RawBuffer::~RawBuffer()
{
}

void RawBuffer::Copy(void* data, UINT size)
{
	DC->CopyResource(result, output);

	D3D11_MAPPED_SUBRESOURCE subResource;
	DC->Map(result, 0, D3D11_MAP_READ, 0, &subResource);

	memcpy(data, subResource.pData, size);

	DC->Unmap(result, 0);
}

void RawBuffer::CreateInput()
{
	ID3D11Buffer* buffer;

	D3D11_BUFFER_DESC desc = {};
	desc.ByteWidth = byteWidth;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	desc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS;

	D3D11_SUBRESOURCE_DATA subResource = {};
	subResource.pSysMem = inputData;

	HRESULT hr = DEVICE->CreateBuffer(&desc, 
		inputData != nullptr ? &subResource : nullptr, &buffer);
	DEBUG(hr);

	input = (ID3D11Resource*)buffer;
}

void RawBuffer::CreateSRV()
{
	ID3D11Buffer* buffer = (ID3D11Buffer*)input;

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Format = DXGI_FORMAT_R32_TYPELESS;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
	srvDesc.BufferEx.Flags = D3D11_BUFFEREX_SRV_FLAG_RAW;
	srvDesc.BufferEx.NumElements = byteWidth / 4;

	HRESULT hr = DEVICE->CreateShaderResourceView(buffer, &srvDesc, &srv);
	DEBUG(hr);
}

void RawBuffer::CreateOutput()
{
	ID3D11Buffer* buffer;

	D3D11_BUFFER_DESC desc = {};
	desc.ByteWidth = byteWidth;
	desc.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
	desc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS;	

	HRESULT hr = DEVICE->CreateBuffer(&desc, nullptr, &buffer);
	DEBUG(hr);

	output = (ID3D11Resource*)buffer;
}

void RawBuffer::CreateUAV()
{
	ID3D11Buffer* buffer = (ID3D11Buffer*)output;	

	D3D11_BUFFER_DESC desc;
	buffer->GetDesc(&desc);

	D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc = {};
	uavDesc.Format = DXGI_FORMAT_R32_TYPELESS;
	uavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	uavDesc.Buffer.Flags = D3D11_BUFFER_UAV_FLAG_RAW;
	uavDesc.Buffer.NumElements = desc.ByteWidth / 4;

	HRESULT hr = DEVICE->CreateUnorderedAccessView(buffer, &uavDesc, &uav);
	DEBUG(hr);
}

void RawBuffer::CreateResult()
{
	ID3D11Buffer* buffer;

	D3D11_BUFFER_DESC desc;
	((ID3D11Buffer*)output)->GetDesc(&desc);
	desc.Usage = D3D11_USAGE_STAGING;
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	desc.BindFlags = 0;
	desc.MiscFlags = 0;

	HRESULT hr = DEVICE->CreateBuffer(&desc, nullptr, &buffer);
	DEBUG(hr);

	result = (ID3D11Resource*)buffer;
}

/////////////////////////////////////////////////////////////////////////////////////

StructBuffer::StructBuffer(void* inputData, UINT inputStride, UINT inputCount, UINT outputStride, UINT outputCount)
	: inputData(inputData), inputStride(inputStride), inputCount(inputCount),
	outputStride(outputStride), outputCount(outputCount)
{
	if (outputStride == 0 || outputCount == 0)
	{
		this->outputStride = inputStride;
		this->outputCount = inputCount;
	}

	CreateBuffer();
}

StructBuffer::~StructBuffer()
{
}

void StructBuffer::Copy(void* data, UINT size)
{
	DC->CopyResource(result, output);

	D3D11_MAPPED_SUBRESOURCE subResource;
	DC->Map(result, 0, D3D11_MAP_READ, 0, &subResource);

	memcpy(data, subResource.pData, size);

	DC->Unmap(result, 0);
}

void StructBuffer::CreateInput()
{
	ID3D11Buffer* buffer;

	D3D11_BUFFER_DESC desc = {};
	desc.ByteWidth = InputByteWidth();
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	desc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	desc.StructureByteStride = inputStride;

	D3D11_SUBRESOURCE_DATA subResource = {};
	subResource.pSysMem = inputData;

	HRESULT hr = DEVICE->CreateBuffer(&desc,
		inputData != nullptr ? &subResource : nullptr, &buffer);
	DEBUG(hr);

	input = (ID3D11Resource*)buffer;
}

void StructBuffer::CreateSRV()
{
	ID3D11Buffer* buffer = (ID3D11Buffer*)input;

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Format = DXGI_FORMAT_UNKNOWN;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;	
	srvDesc.BufferEx.NumElements = inputCount;

	HRESULT hr = DEVICE->CreateShaderResourceView(buffer, &srvDesc, &srv);
	DEBUG(hr);
}

void StructBuffer::CreateOutput()
{
	ID3D11Buffer* buffer;

	D3D11_BUFFER_DESC desc = {};
	desc.ByteWidth = OutputByteWidth();
	desc.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
	desc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	desc.StructureByteStride = outputStride;

	HRESULT hr = DEVICE->CreateBuffer(&desc, nullptr, &buffer);
	DEBUG(hr);

	output = (ID3D11Resource*)buffer;
}

void StructBuffer::CreateUAV()
{
	ID3D11Buffer* buffer = (ID3D11Buffer*)output;

	D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc = {};
	uavDesc.Format = DXGI_FORMAT_UNKNOWN;
	uavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;	
	uavDesc.Buffer.NumElements = outputCount;

	HRESULT hr = DEVICE->CreateUnorderedAccessView(buffer, &uavDesc, &uav);
	DEBUG(hr);
}

void StructBuffer::CreateResult()
{
	ID3D11Buffer* buffer;

	D3D11_BUFFER_DESC desc;
	((ID3D11Buffer*)output)->GetDesc(&desc);
	desc.Usage = D3D11_USAGE_STAGING;
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	desc.BindFlags = 0;
	desc.MiscFlags = 0;

	HRESULT hr = DEVICE->CreateBuffer(&desc, nullptr, &buffer);
	DEBUG(hr);

	result = (ID3D11Resource*)buffer;
}

///////////////////////////////////////////////////////////////////////////////

TextureBuffer::TextureBuffer(ID3D11Texture2D* src)
	: inputTexture(src)
{
	CreateBuffer();
}

TextureBuffer::~TextureBuffer()
{
}

void TextureBuffer::CreateInput()
{
	D3D11_TEXTURE2D_DESC srcDesc;
	inputTexture->GetDesc(&srcDesc);

	width = srcDesc.Width;
	height = srcDesc.Height;
	page = srcDesc.ArraySize;
	format = srcDesc.Format;

	D3D11_TEXTURE2D_DESC desc = {};
	desc.Width = width;
	desc.Height = height;
	desc.MipLevels = 1;
	desc.ArraySize = page;
	desc.Format = format;
	desc.SampleDesc.Count = 1;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

	ID3D11Texture2D* texture;
	DEVICE->CreateTexture2D(&desc, nullptr, &texture);
	DC->CopyResource(texture, inputTexture);

	input = (ID3D11Resource*)texture;
}

void TextureBuffer::CreateSRV()
{
	ID3D11Texture2D* buffer = (ID3D11Texture2D*)input;

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Format = format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
	srvDesc.Texture2DArray.MipLevels = 1;
	srvDesc.Texture2DArray.ArraySize = page;	

	HRESULT hr = DEVICE->CreateShaderResourceView(buffer, &srvDesc, &srv);
	DEBUG(hr);
}

void TextureBuffer::CreateOutput()
{
	D3D11_TEXTURE2D_DESC desc = {};
	desc.Width = width;
	desc.Height = height;
	desc.MipLevels = 1;
	desc.ArraySize = page;
	desc.Format = format;
	desc.SampleDesc.Count = 1;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;

	ID3D11Texture2D* texture;
	DEVICE->CreateTexture2D(&desc, nullptr, &texture);	

	output = (ID3D11Resource*)texture;
}

void TextureBuffer::CreateUAV()
{
	ID3D11Texture2D* buffer = (ID3D11Texture2D*)output;

	D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc = {};
	uavDesc.Format = DXGI_FORMAT_UNKNOWN;
	uavDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2DARRAY;
	uavDesc.Texture2DArray.ArraySize = page;	

	HRESULT hr = DEVICE->CreateUnorderedAccessView(buffer, &uavDesc, &uav);
	DEBUG(hr);
}

void TextureBuffer::CreateResult()
{
	ID3D11Texture2D* buffer = (ID3D11Texture2D*)output;

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Format = format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
	srvDesc.Texture2DArray.MipLevels = 1;
	srvDesc.Texture2DArray.ArraySize = page;

	HRESULT hr = DEVICE->CreateShaderResourceView(buffer, &srvDesc, &outputSrv);
	DEBUG(hr);
}
