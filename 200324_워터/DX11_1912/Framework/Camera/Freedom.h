#pragma once

class Freedom : public Camera
{
private:
	float moveSpeed;
	float rotationSpeed;
	float wheelSpeed;	
public:
	Freedom();
	~Freedom();

	void Update() override;
};