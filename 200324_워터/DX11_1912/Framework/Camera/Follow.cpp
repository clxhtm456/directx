#include "Framework.h"

Follow::Follow()
	: distance(60), height(60), offset(0, 5, 0), rotDamping(0),
	moveDamping(5), rotY(0), zoomSpeed(10), destPos(0, 0, 0), destRot(0),
	target(nullptr)
{
}

Follow::~Follow()
{
}

void Follow::Update()
{
	if (target == nullptr)
		return;

	Vector3 tempPos = Vector3(0, 0, -distance);

	if (rotDamping > 0.0f)
	{
		if (target->rotation.y != destRot)
		{
			destRot = LERP(destRot, target->rotation.y, rotDamping * DELTA);
		}

		D3DXMatrixRotationY(&matRotation, destRot + D3DX_PI);
	}
	else
	{
		MouseControl();
		D3DXMatrixRotationY(&matRotation, rotY);
	}

	D3DXVec3TransformCoord(&destPos, &tempPos, &matRotation);

	destPos += target->position;
	destPos.y += height;

	D3DXVec3Lerp(&position, &position, &destPos, moveDamping * DELTA);

	D3DXMatrixLookAtLH(&matView, &position, &(target->position + offset),
		&up);
}

void Follow::PostRender()
{
	ImGui::Text("Camera Setting");
	ImGui::SliderFloat("Distance", &distance, 1.0f, 100.0f);
	ImGui::SliderFloat("Height", &height, 0.0f, 100.0f);
	ImGui::SliderFloat("MoveDamping", &moveDamping, 0.0f, 30.0f);
	ImGui::SliderFloat("RotDamping", &rotDamping, 0.0f, 30.0f);
	ImGui::SliderFloat3("Offset", offset, 0.0f, 20.0f);	
}

void Follow::MouseControl()
{
	if (KEYPRESS(VK_RBUTTON))
	{
		{//Rotation
			Vector3 val = mousePos - oldPos;

			if (KEYPRESS(VK_RBUTTON))
			{
				rotY += val.x * 0.001f;
			}

			oldPos = mousePos;
		}
	}

	{
		distance -= wheelValue * 0.1f;
		height -= wheelValue * 0.1f;

		if (distance < 1.0f)
			distance = 1.0f;
		if (height < 1.0f)
			height = 1.0f;
	}
}
