#include "Framework.h"

Freedom::Freedom()
	: moveSpeed(30), rotationSpeed(0.005f), wheelSpeed(0.1f)
{	
}

Freedom::~Freedom()
{
}

void Freedom::Update()
{
	{//Move
		if (KEYPRESS(VK_RBUTTON))
		{
			if (KEYPRESS('W'))
				position += forward * moveSpeed * DELTA;
			if (KEYPRESS('S'))
				position -= forward * moveSpeed * DELTA;
			if (KEYPRESS('A'))
				position -= right * moveSpeed * DELTA;
			if (KEYPRESS('D'))
				position += right * moveSpeed * DELTA;
			if (KEYPRESS('Q'))
				position -= up * moveSpeed * DELTA;
			if (KEYPRESS('E'))
				position += up * moveSpeed * DELTA;
		}

		position += forward * wheelValue * wheelSpeed;		
	}

	{//Rotation
		Vector3 val = mousePos - oldPos;

		if (KEYPRESS(VK_RBUTTON))
		{
			rotation.x += val.y * rotationSpeed;
			rotation.y += val.x * rotationSpeed;

			Rotation();
		}

		oldPos = mousePos;
	}

	View();
}
