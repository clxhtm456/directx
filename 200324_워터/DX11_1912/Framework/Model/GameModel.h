#pragma once

class GameModel : public Transform
{
protected:
	Model* model;
	IntBuffer* intBuffer;
	vector<Matrix> boneTransforms;

	bool isReceiveShadow;
public:
	GameModel(string file);
	~GameModel();

	virtual void Update();
	virtual void Render();

	void SetShader(wstring file) { model->SetShader(file); }

	Model* GetModel() { return model; }

	void SetShadow(bool value) { isReceiveShadow = value; }
	bool IsShadowRender() { return isReceiveShadow; }
};