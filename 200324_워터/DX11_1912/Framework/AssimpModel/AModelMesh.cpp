#include "Framework.h"

AModelMesh::AModelMesh(): transformsSRV(nullptr)
{
	boneBuffer = new BoneBuffer();
}

AModelMesh::~AModelMesh()
{
	delete boneBuffer;

	delete vertexBuffer;
	delete indexBuffer;

	delete[] vertices;
	delete[] indices;
}

void AModelMesh::Binding(AModel* model)
{
	vertexBuffer = new VertexBuffer(vertices, sizeof(ModelVertexType), vertexCount);
	indexBuffer = new IndexBuffer(indices, indexCount);

	Material* srcMateiral = model->MaterialByName(materialName);

	material = new Material();
	material->GetBuffer()->data = srcMateiral->GetBuffer()->data;

	if (srcMateiral->GetDiffuseMap() != nullptr)
		material->SetDiffuseMap(srcMateiral->GetDiffuseMap()->GetFile());
	if (srcMateiral->GetSpecularMap() != nullptr)
		material->SetSpecularMap(srcMateiral->GetSpecularMap()->GetFile());
	if (srcMateiral->GetNormalMap() != nullptr)
		material->SetNormalMap(srcMateiral->GetNormalMap()->GetFile());
}

void AModelMesh::Update()
{
	boneBuffer->SetIndex(boneIndex);
}

void AModelMesh::Render(UINT drawCount)
{
	vertexBuffer->Set();
	indexBuffer->Set();

	IASetPT();

	if(transformsSRV != nullptr)
		DC->VSSetShaderResources(10, 1, &transformsSRV);

	boneBuffer->SetVSBuffer(2);

	material->Set();

	DC->DrawIndexedInstanced(indexCount, drawCount,0, 0, 0);
}

void AModelMesh::SetShader(Shader* shader)
{
	material->SetShader(shader);
}

void AModelMesh::SetTransforms(Matrix* transforms)
{
	boneBuffer->Bones(transforms, MAX_MODEL_BONE);
}
