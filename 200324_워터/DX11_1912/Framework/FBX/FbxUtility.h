#pragma once

class FbxUtility
{
public:
	static Matrix Negative();

	static Color ToColor(FbxPropertyT<FbxDouble3>& vec, FbxPropertyT<FbxDouble>& factor);
	static Matrix ToMatrix(FbxAMatrix& value);
	static Matrix ToMatrix(aiMatrix4x4& value);
	static Vector3 ToVector3(FbxVector4& value);

	static string GetTextureFile(FbxProperty& prop);
	static string GetMaterialName(FbxMesh* mesh, int polygonIndex, int cpIndex);
	static Vector2 GetUV(FbxMesh* mesh, int cpIndex, int uvIndex);
};