#include "Framework.h"

Environment* Environment::instance = nullptr;

Environment::Environment()
{
	perspective = new Perspective();
	viewport = new Viewport();

	viewProjection = new ViewProjectionBuffer();
	viewProjection->SetProjection(perspective->GetMatrix());

	light = new LightBuffer();
	mainCamera = new Freedom();
	mainCamera->SetPos(8, 13, -30);	
	//mainCamera = new Follow();
}

Environment::~Environment()
{
	delete perspective;
	delete viewport;

	delete viewProjection;
	delete light;

	delete mainCamera;
}
