#pragma once

class Frustum
{
private:
	Perspective* perspective;

	D3DXPLANE planes[6];

public:
	Frustum(Perspective* perspective = nullptr);
	~Frustum();

	void Update();

	bool ContainPoint(Vector3 position);
	bool ContainCube(Vector3 center, float radius);
};