#include "Framework.h"

Frustum::Frustum(Perspective* perspective)
	: perspective(perspective)
{
	if (perspective == nullptr)
		this->perspective = Environment::Get()->GetPerspective();
}

Frustum::~Frustum()
{
}

void Frustum::Update()
{
	Matrix V, P;
	V = CAMERA->GetView();
	P = perspective->GetMatrix();

	Matrix matrix = V * P;

	//Near
	planes[0].a = matrix._14 + matrix._13;
	planes[0].b = matrix._24 + matrix._23;
	planes[0].c = matrix._34 + matrix._33;
	planes[0].d = matrix._44 + matrix._43;

	//Far
	planes[1].a = matrix._14 - matrix._13;
	planes[1].b = matrix._24 - matrix._23;
	planes[1].c = matrix._34 - matrix._33;
	planes[1].d = matrix._44 - matrix._43;

	//Left
	planes[2].a = matrix._14 + matrix._11;
	planes[2].b = matrix._24 + matrix._21;
	planes[2].c = matrix._34 + matrix._31;
	planes[2].d = matrix._44 + matrix._41;

	//Right
	planes[3].a = matrix._14 - matrix._11;
	planes[3].b = matrix._24 - matrix._21;
	planes[3].c = matrix._34 - matrix._31;
	planes[3].d = matrix._44 - matrix._41;

	//Bottom
	planes[4].a = matrix._14 + matrix._12;
	planes[4].b = matrix._24 + matrix._22;
	planes[4].c = matrix._34 + matrix._32;
	planes[4].d = matrix._44 + matrix._42;

	//Top
	planes[5].a = matrix._14 - matrix._12;
	planes[5].b = matrix._24 - matrix._22;
	planes[5].c = matrix._34 - matrix._32;
	planes[5].d = matrix._44 - matrix._42;

	for (UINT i = 0; i < 6; i++)
		D3DXPlaneNormalize(&planes[i], &planes[i]);
}

bool Frustum::ContainPoint(Vector3 position)
{
	for (UINT i = 0; i < 6; i++)
	{
		if (D3DXPlaneDotCoord(&planes[i], &position) < 0.0f)
		return false;
	}

	return true;
}

bool Frustum::ContainCube(Vector3 center, float radius)
{
	Vector3 edge;

	for (UINT i = 0; i < 6; i++)
	{
		//1
		edge.x = center.x - radius;
		edge.y = center.y - radius;
		edge.z = center.z - radius;
		if (D3DXPlaneDotCoord(&planes[i], &edge) > 0.0f)
			continue;

		//2
		edge.x = center.x + radius;
		edge.y = center.y - radius;
		edge.z = center.z - radius;
		if (D3DXPlaneDotCoord(&planes[i], &edge) > 0.0f)
			continue;

		//3
		edge.x = center.x + radius;
		edge.y = center.y + radius;
		edge.z = center.z - radius;
		if (D3DXPlaneDotCoord(&planes[i], &edge) > 0.0f)
			continue;

		//4
		edge.x = center.x - radius;
		edge.y = center.y - radius;
		edge.z = center.z + radius;
		if (D3DXPlaneDotCoord(&planes[i], &edge) > 0.0f)
			continue;

		//5
		edge.x = center.x + radius;
		edge.y = center.y - radius;
		edge.z = center.z + radius;
		if (D3DXPlaneDotCoord(&planes[i], &edge) > 0.0f)
			continue;

		//6
		edge.x = center.x - radius;
		edge.y = center.y + radius;
		edge.z = center.z + radius;
		if (D3DXPlaneDotCoord(&planes[i], &edge) > 0.0f)
			continue;

		//7
		edge.x = center.x - radius;
		edge.y = center.y + radius;
		edge.z = center.z - radius;
		if (D3DXPlaneDotCoord(&planes[i], &edge) > 0.0f)
			continue;

		//8
		edge.x = center.x + radius;
		edge.y = center.y + radius;
		edge.z = center.z + radius;
		if (D3DXPlaneDotCoord(&planes[i], &edge) > 0.0f)
			continue;

		return false;
	}

	return true;
}
