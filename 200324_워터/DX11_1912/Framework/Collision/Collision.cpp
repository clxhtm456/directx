#include "Framework.h"

Collision::Collision()
{
	shader = Shader::Add(L"Collision");
	colorBuffer = new ColorBuffer();
	colorBuffer->data.color = Color(0, 1, 0, 1);
}

Collision::~Collision()
{
	delete colorBuffer;
	delete vertexBuffer;
	delete indexBuffer;
}

void Collision::Render()
{
	UpdateWorld();

	vertexBuffer->Set();
	indexBuffer->Set();
	IASetPT(D3D_PRIMITIVE_TOPOLOGY_LINELIST);

	colorBuffer->SetPSBuffer(10);
	SetVS();

	shader->Set();

	DC->DrawIndexed(indexCount, 0, 0);
}
