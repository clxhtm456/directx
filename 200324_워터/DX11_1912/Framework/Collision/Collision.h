#pragma once

class Collision : public Transform
{
protected:
	Shader* shader;

	ColorBuffer* colorBuffer;

	VertexBuffer* vertexBuffer;
	IndexBuffer* indexBuffer;

	UINT indexCount;
public:
	Collision();
	virtual ~Collision();

	virtual bool IsCollision(Collision* collision) = 0;
	virtual bool IsCollision(IN Ray ray, OUT float* distance = nullptr,
		OUT Vector3* contact = nullptr) = 0;

	void Render();

	void SetColor(Color color) { colorBuffer->data.color = color; }
};