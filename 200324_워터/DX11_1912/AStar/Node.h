#pragma once

class Node
{
public:
	friend class AStar;
	friend class Heap;

	struct EdgeInfo
	{
		int index;
		float edgeCost;
	};

	enum State
	{
		NONE,
		OPEN,
		CLOSED,
		USING,
		OBSTACLE
	};

private:
	SphereCollision* sphere;

	Vector3 pos;
	int index;
	int via;

	float f, g, h;

	State state;

	Vector2 interval;

	vector<EdgeInfo*> edges;

public:
	Node(Vector3 pos, int index, Vector2 interval);
	~Node();

	void Update();
	void Render();

	void AddEdge(Node* node);

	BoxCollision* SetObstalce();
};