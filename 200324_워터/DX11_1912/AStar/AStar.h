#pragma once

class Terrain;

class AStar
{
private:
	UINT width, height;

	vector<Node*> nodes;
	//vector<int> openNodes;
	Heap* heap;

	vector<BoxCollision*> obstacles;
public:
	AStar(UINT width = 20, UINT height = 20);
	~AStar();

	void SetUp(Terrain* terrain);
	void Update();
	void Render();

	vector<Vector3> FindPath(int start, int end);	
	int FindCloseNode(Vector3 pos);
	void MakeDirectPath(IN Vector3 start, IN Vector3 end, OUT vector<Vector3>& path);

	bool IsCollisionObstalce(Ray ray, float destDistance);

	void Reset();
private:
	float GetManhattanDistance(int curIndex, int end);
	float GetDistance(int curIndex, int end);

	void Extend(int center, int end);
	int GetMinNode();
};