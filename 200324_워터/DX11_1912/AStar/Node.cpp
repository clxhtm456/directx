#include "Framework.h"

Node::Node(Vector3 pos, int index, Vector2 interval)
	: pos(pos), index(index), via(-1), f(0), g(0), h(0), state(NONE), interval(interval)
{
	sphere = new SphereCollision();
	sphere->position = pos;	
}

Node::~Node()
{
	delete sphere;	
}

void Node::Update()
{
	switch (state)
	{
	case Node::NONE:
		sphere->SetColor(Color(1, 1, 1, 1));
		break;
	case Node::OPEN:
		sphere->SetColor(Color(0, 0, 1, 1));
		break;
	case Node::CLOSED:
		sphere->SetColor(Color(1, 0, 0, 1));
		break;
	case Node::USING:
		sphere->SetColor(Color(0, 1, 0, 1));
		break;
	case Node::OBSTACLE:
		sphere->SetColor(Color(0, 0, 0, 1));
		break;
	default:
		break;
	}
}

void Node::Render()
{	
	sphere->Render();
}

void Node::AddEdge(Node* node)
{
	EdgeInfo* edge = new EdgeInfo();
	edge->index = node->index;
	edge->edgeCost = Distance(pos, node->pos);

	edges.push_back(edge);
}

BoxCollision* Node::SetObstalce()
{
	state = OBSTACLE;

	Vector3 min = { -interval.x * 0.5f, -10.0f, -interval.y * 0.5f };
	Vector3 max = min * -1.0f;

	BoxCollision* box = new BoxCollision(min, max);
	box->position = sphere->position;

	return box;
}