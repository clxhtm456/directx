﻿// header.h: 표준 시스템 포함 파일
// 또는 프로젝트 특정 포함 파일이 들어 있는 포함 파일입니다.
//

#pragma once

#include "targetver.h"
#define WIN32_LEAN_AND_MEAN

#define WIN_WIDTH 1280
#define WIN_HEIGHT 720
#define WIN_START_X 0
#define WIN_START_Y 0

#define MAX_MODEL_BONE 256
#define MAX_MODEL_KEY 500
#define MAX_MODEL_INSTANCE 500

#define MAX_POINTLIGHT 32

#define DEBUG(hr) assert(SUCCEEDED(hr))

#define DEVICE Device::Get()->GetDevice()
#define DC Device::Get()->GetContext()

#define VP Environment::Get()->GetVP()
#define LIGHT Environment::Get()->GetLight()
#define CAMERA Environment::Get()->GetCamera()

#define KEYDOWN(k) Keyboard::Get()->Down(k)
#define KEYUP(k) Keyboard::Get()->Up(k)
#define KEYPRESS(k) Keyboard::Get()->Press(k)

#define DELTA Timer::Get()->Delta()

#define LERP(s, e, t) (s + (e - s)*t)

#include <windows.h>

#include <cassert>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <cmath>
#include <functional>
#include <algorithm>

using namespace std;

#include <d3d11.h>
#include <D3DX11.h>
#include <d3dcompiler.h>
#include <D3DX10math.h>

#pragma comment(lib, "d3dx10.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dx11.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "dxguid.lib")

//ImGui
#include <imgui.h>
#include <imguiDx11.h>
#pragma comment(lib, "imgui.lib")

//FBX SDK
#define FBXSDK_SHARED
#include <fbxsdk.h>
#pragma comment(lib, "libfbxsdk.lib")

using namespace fbxsdk;

//Assimp
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#pragma comment(lib, "assimp/assimp-vc142-mtd.lib")

//DirectWrite
#include <d2d1_2.h>
#include <dwrite.h>

#pragma comment(lib, "d2d1.lib")
#pragma comment(lib, "dwrite.lib")

typedef D3DXVECTOR4 Vector4;
typedef D3DXVECTOR3 Vector3;
typedef D3DXVECTOR2 Vector2;
typedef D3DXMATRIX Matrix;
typedef D3DXCOLOR Color;
typedef D3DXQUATERNION Quaternion;

//Framework Header
#include "Framework/Core/Device.h"

#include "Framework/Utility/Keyboard.h"
#include "Framework/Utility/Timer.h"
#include "Framework/Utility/Binary.h"
#include "Framework/Utility/Xml.h"
#include "Framework/Utility/Utility.h"
#include "Framework/Utility/Math.h"
#include "Framework/Utility/DirectWrite.h"

using namespace GameMath;

#include "Framework/Render/Shader.h"
#include "Framework/Render/ComputShader.h"
#include "Framework/Render/Buffer.h"
#include "Framework/Render/ConstBuffer.h"
#include "Framework/Render/GlobalBuffer.h"
#include "Framework/Render/VertexLayouts.h"
#include "Framework/Render/Texture.h"
#include "Framework/Render/Material.h"
#include "Framework/Render/RenderTarget.h"
#include "Framework/Render/CsResource.h"
#include "Framework/Render/ComputeBuffer.h"

#include "Framework/Game/Transform.h"

#include "Framework/Camera/Camera.h"
#include "Framework/Camera/Freedom.h"
#include "Framework/Camera/Follow.h"

#include "Framework/Environment/Viewport.h"
#include "Framework/Environment/Perspective.h"
#include "Framework/Environment/Environment.h"
#include "Framework/Environment/Frustum.h"

#include "Framework/FBX/FbxType.h"
#include "Framework/FBX/FbxUtility.h"
#include "Framework/FBX/Exporter.h"
#include "Framework/FBX/AssimpReader.h"

#include "Framework/Model/ModelMeshPart.h"
#include "Framework/Model/ModelMesh.h"
#include "Framework/Model/ModelKeyFrame.h"
#include "Framework/Model/ModelClip.h"
#include "Framework/Model/ModelTweener.h"
#include "Framework/Model/Model.h"
#include "Framework/Model/GameModel.h"
#include "Framework/Model/GameAnimModel.h"

#include "Framework/AssimpModel/AModelMesh.h"
#include "Framework/AssimpModel/AModelClip.h"
#include "Framework/AssimpModel/AModel.h"
#include "Framework/AssimpModel/AModelRender.h"
#include "Framework/AssimpModel/AModelAnimator.h"

#include "Framework/Collision/Collision.h"
#include "Framework/Collision/BoxCollision.h"
#include "Framework/Collision/SphereCollision.h"

#include "Framework/States/BlendState.h"
#include "Framework/States/DepthStencilState.h"
#include "Framework/States/RasterizerState.h"
#include "Framework/States/SamplerState.h"

#include "AStar/Node.h"
#include "AStar/Heap.h"
#include "AStar/AStar.h"

//Object Header
#include "Objects/Sphere.h"
#include "Objects/Terrain.h"
#include "Objects/TerrainEditor.h"
#include "Objects/Tank.h"
#include "Objects/Pikachu.h"
#include "Objects/Brook.h"
#include "Objects/Billboard.h"
#include "Objects/Sky.h"
#include "Objects/SkyCube.h"
#include "Objects/TerrainData.h"
#include "Objects/QuadTreeTerrain.h"
#include "Objects/Shadow.h"

#include "Objects/PostEffect/Render2D.h"
#include "Objects/PostEffect/MonoEffect.h"
#include "Objects/PostEffect/BlurEffect.h"

#include "Objects/Scattering.h"
#include "Objects/Water.h"

#include "Objects/Particle/Particle.h"
#include "Objects/Particle/ParticleExp.h"
#include "Objects/Particle/ParticleRain.h"
#include "Objects/Particle/ParticleManager.h"

//Scene Header
#include "Scenes/Scene.h"
//#include "Scenes/TextureScene.h"
//#include "Scenes/GridScene.h"
#include "Scenes/LandscapeScene.h"
#include "Scenes/ModelScene.h"
//#include "Scenes/CollisionScene.h"
//#include "Scenes/BillbaordScene.h"
//#include "Scenes/ParticleScene.h"
//#include "Scenes/RenderTargetScene.h"
//#include "Scenes/OutlineScene.h"
#include "Scenes/CharacterScene.h"
#include "Scenes/InstancingScene.h"
#include "Scenes/FrustumScene.h"
#include "Scenes/ModelInstancingScene.h"
#include "Scenes/ComputeScene.h"
#include "Scenes/ShadowScene.h"
#include "Scenes/LightScene.h"
#include "Scenes/WaterScene.h"

//Program Header
#include "Program/Program.h"

extern HWND hWnd;
extern Vector3 mousePos;
extern float wheelValue;