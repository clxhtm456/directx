#pragma once

class Render2D;

class Shadow
{
private:
	class ShadowBuffer : public ConstBuffer
	{
	public:
		struct Data
		{
			Vector2 size;

			float bias;
			int selcted;
		}data;

		ShadowBuffer() : ConstBuffer(&data, sizeof(Data))
		{
			data.size = Vector2(0, 0);
			data.bias = -0.0005f;
			data.selcted = 0;
		}
	};

	ShadowBuffer* buffer;

	Shader* depthShader;
	Shader* renderShader;

	RenderTarget* depthTarget;
	Render2D* depthRender;

	vector<GameModel*> objs;
	Perspective* perspective;

	ViewProjectionBuffer* vpBuffer;

	SamplerState* samplerState[2];
public:
	Shadow();
	~Shadow();

	void Update();
	void PreRender();
	void Render();
	void PostRender();

	void Add(GameModel* obj);
};