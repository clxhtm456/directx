#pragma once

class Sphere : public Transform
{
private:
	typedef VertexUVNormalTangent VertexType;

	Material* material;

	vector<VertexType> vertices;
	vector<UINT> indices;

	VertexBuffer* vertexBuffer;
	IndexBuffer* indexBuffer;

	UINT stackCount;
	UINT sliceCount;
	float radius;

	Collision* collision;
public:
	Sphere(float radius, UINT stackCount = 20, UINT sliceCount = 20);
	Sphere(wstring shaderFile, float radius = 1.0f, UINT stackCount = 20, UINT sliceCount = 20);
	~Sphere();

	void Update();
	void Render();	

	void CreateData();
	void CreateTangent();

	float GetRadius() { return radius; }

	Material* GetMaterial() { return material; }

	Collision* GetCollision() { return collision; }
};