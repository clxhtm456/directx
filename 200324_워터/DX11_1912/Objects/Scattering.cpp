#include "Framework.h"

Scattering::Scattering()
	: theta(0), phi(0), radius(10), slices(32), stacks(16)
{
	rayleighTarget = new RenderTarget();
	mieTarget = new RenderTarget();

	rayleigh2D = new Render2D();
	rayleigh2D->position = { 100, 150 };
	rayleigh2D->scale = { 200, 100 };
	rayleigh2D->SetSRV(rayleighTarget->GetSRV());

	mie2D = new Render2D();
	mie2D->position = { 100, 50 };
	mie2D->scale = { 200, 100 };
	mie2D->SetSRV(mieTarget->GetSRV());

	scatterShader = Shader::Add(L"Scattering");
	targetShader = Shader::Add(L"ScatteringTarget");

	worldBuffer = new WorldBuffer();
	targetBuffer = new TargetBuffer();
	scatterBuffer = new FloatBuffer();

	CreateSphere();
	CreateQuad();

	starField = Texture::Add(L"Textures/Landscape/StarField.png");

	depthMode[0] = new DepthStencilState();
	depthMode[1] = new DepthStencilState();
	depthMode[1]->DepthEnable(false);
}

Scattering::~Scattering()
{
	delete depthMode[0];
	delete depthMode[1];

	delete worldBuffer;
	delete targetBuffer;
	delete scatterBuffer;

	delete[] quadVertices;
	delete quadBuffer;

	delete vertexBuffer;
	delete indexBuffer;

	delete rayleighTarget;
	delete mieTarget;

	delete rayleigh2D;
	delete mie2D;
}

void Scattering::Update()
{
	Matrix world;
	Vector3 camPos = CAMERA->GetPos();
	D3DXMatrixTranslation(&world, camPos.x, camPos.y, camPos.z);

	worldBuffer->SetWorld(world);
}

void Scattering::PreRender()
{
	rayleighTarget->Set();
	mieTarget->Set();

	ID3D11RenderTargetView* rtvs[2];
	rtvs[0] = rayleighTarget->GetRTV();
	rtvs[1] = mieTarget->GetRTV();

	ID3D11DepthStencilView* dsv = rayleighTarget->GetDSV();

	Device::Get()->SetRenderTargets(2, rtvs, dsv);

	quadBuffer->Set();
	IASetPT();

	targetBuffer->SetPSBuffer(11);
	targetShader->Set();

	DC->Draw(6, 0);
}

void Scattering::Render()
{
	vertexBuffer->Set();
	indexBuffer->Set();
	IASetPT();

	worldBuffer->SetVSBuffer(1);

	ID3D11ShaderResourceView* srv = rayleighTarget->GetSRV();
	DC->PSSetShaderResources(10, 1, &srv);

	srv = mieTarget->GetSRV();
	DC->PSSetShaderResources(11, 1, &srv);

	starField->Set(12);

	scatterBuffer->data.index = LIGHT->data.direction.y;
	scatterBuffer->SetPSBuffer(10);

	scatterShader->Set();

	depthMode[1]->SetState();
	DC->DrawIndexed(indexCount, 0, 0);
	depthMode[0]->SetState();
}

void Scattering::PostRender()
{
	rayleigh2D->Update();
	rayleigh2D->Render();

	mie2D->Update();
	mie2D->Render();
}

void Scattering::CreateSphere()
{
	UINT domeCount = slices;
	UINT latitude = stacks;
	UINT longitude = domeCount;

	vertexCount = longitude * latitude * 2;
	indexCount = (longitude - 1) * (latitude - 1) * 2 * 8;

	VertexUV* vertices = new VertexUV[vertexCount];

	UINT index = 0;
	for (UINT i = 0; i < longitude; i++)
	{
		float xz = 100.0f * (i / (longitude - 1.0f)) * D3DX_PI / 180.0f;

		for (UINT j = 0; j < latitude; j++)
		{
			float y = D3DX_PI * j / (latitude - 1);

			vertices[index].position.x = sinf(xz) * cosf(y);
			vertices[index].position.y = cosf(xz);
			vertices[index].position.z = sinf(xz) * sinf(y);
			vertices[index].position *= radius;

			vertices[index].uv.x = 0.5f / (float)longitude + i / (float)longitude;
			vertices[index].uv.y = 0.5f / (float)latitude + j / (float)latitude;

			index++;
		}
	}

	for (UINT i = 0; i < longitude; i++)
	{
		float xz = 100.0f * (i / (longitude - 1.0f)) * D3DX_PI / 180.0f;

		for (UINT j = 0; j < latitude; j++)
		{
			float y = (D3DX_PI * 2.0f) - (D3DX_PI * j / (latitude - 1));

			vertices[index].position.x = sinf(xz) * cosf(y);
			vertices[index].position.y = cosf(xz);
			vertices[index].position.z = sinf(xz) * sinf(y);
			vertices[index].position *= radius;

			vertices[index].uv.x = 0.5f / (float)longitude + i / (float)longitude;
			vertices[index].uv.y = 0.5f / (float)latitude + j / (float)latitude;

			index++;
		}
	}

	index = 0;
	UINT* indices = new UINT[indexCount * 3];

	for (UINT i = 0; i < longitude - 1; i++)
	{
		for (UINT j = 0; j < latitude - 1; j++)
		{
			indices[index++] = i * latitude + j;
			indices[index++] = (i + 1) * latitude + j;
			indices[index++] = (i + 1) * latitude + (j + 1);

			indices[index++] = (i + 1) * latitude + (j + 1);
			indices[index++] = i * latitude + (j + 1);
			indices[index++] = i * latitude + j;
		}
	}

	UINT offset = latitude * longitude;
	for (UINT i = 0; i < longitude - 1; i++)
	{
		for (UINT j = 0; j < latitude - 1; j++)
		{
			indices[index++] = offset + i * latitude + j;
			indices[index++] = offset + (i + 1) * latitude + (j + 1);
			indices[index++] = offset + (i + 1) * latitude + j;

			indices[index++] = offset + i * latitude + (j + 1);
			indices[index++] = offset + (i + 1) * latitude + (j + 1);
			indices[index++] = offset + i * latitude + j;
		}
	}

	vertexBuffer = new VertexBuffer(vertices, sizeof(VertexUV), vertexCount);
	indexBuffer = new IndexBuffer(indices, indexCount);

	delete[] vertices;
	delete[] indices;
}

void Scattering::CreateQuad()
{
	quadVertices = new VertexUV[6];

	quadVertices[0].position = Vector3(-1, -1, 0);
	quadVertices[1].position = Vector3(-1, 1, 0);
	quadVertices[2].position = Vector3(1, -1, 0);
	quadVertices[3].position = Vector3(1, -1, 0);
	quadVertices[4].position = Vector3(-1, 1, 0);
	quadVertices[5].position = Vector3(1, 1, 0);

	quadVertices[0].uv = Vector2(0, 1);
	quadVertices[1].uv = Vector2(0, 0);
	quadVertices[2].uv = Vector2(1, 1);
	quadVertices[3].uv = Vector2(1, 1);
	quadVertices[4].uv = Vector2(0, 0);
	quadVertices[5].uv = Vector2(1, 0);

	quadBuffer = new VertexBuffer(quadVertices, sizeof(VertexUV), 6);
}
