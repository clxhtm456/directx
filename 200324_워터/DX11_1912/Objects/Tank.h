#pragma once

class Tank : public GameModel
{
private:
	float moveSpeed;
	float rotSpeed;

	float wheelAngle;

	ModelBone* leftWheelBone;
	Matrix leftWheel;
public:
	Tank();
	~Tank();

	void Update();
	void Render();
};