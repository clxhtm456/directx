#include "Framework.h"

Terrain::Terrain()
{
	material = new Material(L"Terrain");
	material->SetDiffuseMap(L"Textures/Landscape/Stones.png");
	material->SetNormalMap(L"Textures/Landscape/Stones_normal.png");

	heightMap = Texture::Add(L"Textures/HeightMaps/ColorMap256.png");

	width = heightMap->GetWidth() - 1;
	height = heightMap->GetHeight() - 1;

	CreateData();
	CreateNormal();
	CreateTangent();

	vertexBuffer = new VertexBuffer(vertices.data(), sizeof(VertexType), vertices.size());
	indexBuffer = new IndexBuffer(indices.data(), indices.size());

	D3D11_RASTERIZER_DESC rsDesc = {};
	rsDesc.AntialiasedLineEnable = false;
	rsDesc.CullMode = D3D11_CULL_BACK;
	rsDesc.FillMode = D3D11_FILL_WIREFRAME;

	DEVICE->CreateRasterizerState(&rsDesc, &state);	
}

Terrain::~Terrain()
{
	delete material;

	delete vertexBuffer;
	delete indexBuffer;

	state->Release();
}

void Terrain::Update()
{
	UpdateWorld();
}

void Terrain::Render()
{
	vertexBuffer->Set();
	indexBuffer->Set();
	IASetPT();

	SetVS();
	material->Set();

	DC->DrawIndexed(indices.size(), 0, 0);
}

void Terrain::PostRender()
{
	Vector3 pos;
	Picking(&pos);
	ImGui::SliderFloat3("PickPos", pos, -1000, 1000);
}

bool Terrain::Picking(OUT Vector3* position)
{
	Ray ray = CAMERA->GetPickingRay();

	for (UINT z = 0; z < height; z++)
	{
		for (UINT x = 0; x < width; x++)
		{
			UINT index[4];
			index[0] = (width + 1) * z + x;
			index[1] = (width + 1) * z + x + 1;
			index[2] = (width + 1) * (z + 1) + x;
			index[3] = (width + 1) * (z + 1) + x + 1;

			Vector3 p[4];
			for (UINT i = 0; i < 4; i++)
				p[i] = vertices[index[i]].position;

			float u, v, distance;
			if (D3DXIntersectTri(&p[0], &p[1], &p[2], &ray.position, &ray.direction,
				&u, &v, &distance))
			{
				*position = p[0] + (p[1] - p[0]) * u + (p[2] - p[0]) * v;
				return true;
			}

			if (D3DXIntersectTri(&p[3], &p[1], &p[2], &ray.position, &ray.direction,
				&u, &v, &distance))
			{
				*position = p[3] + (p[1] - p[3]) * u + (p[2] - p[3]) * v;
				return true;
			}
		}
	}

	return false;
}

float Terrain::GetPosY(IN Vector3 position)
{
	UINT x = (UINT)position.x;
	UINT z = (UINT)position.z;

	if (x < 0 || x > width) return 0.0f;
	if (z < 0 || z > height) return 0.0f;

	UINT index[4];
	index[0] = (width + 1) * z + x;
	index[1] = (width + 1) * (z + 1) + x;
	index[2] = (width + 1) * z + x + 1;
	index[3] = (width + 1) * (z + 1) + (x + 1);

	Vector3 p[4];
	for (int i = 0; i < 4; i++)
		p[i] = vertices[index[i]].position;

	float u = position.x - p[0].x;
	float v = position.z - p[0].z;

	Vector3 result;
	if (u + v <= 1)
	{
		result = p[0] + (p[2] - p[0]) * u + (p[1] - p[0]) * v;
	}
	else
	{
		u = 1.0f - u;
		v = 1.0f - v;

		result = p[3] + (p[1] - p[3]) * u + (p[2] - p[3]) * v;
	}

	return result.y;
}

void Terrain::CreateData()
{
	vector<Color> pixels = heightMap->ReadPixels();

	{//Vertices
		for (UINT z = 0; z <= height; z++)
		{
			for (UINT x = 0; x <= width; x++)
			{
				VertexType vertex;
				vertex.position = Vector3(x, rand() % 2, z);
				vertex.uv = Vector2(1.0f - x / (float)width, 1.0f - z / (float)height);

				UINT index = (width + 1) * z + x;
				vertex.position.y = pixels[index].r * 20.0f;

				vertices.push_back(vertex);
			}
		}
	}

	{//Indices
		for (UINT z = 0; z < height; z++)
		{
			for (UINT x = 0; x < width; x++)
			{
				indices.push_back((width + 1) * z + x);//0
				indices.push_back((width + 1) * (z + 1) + x);//1
				indices.push_back((width + 1) * (z + 1) + x + 1);//2

				indices.push_back((width + 1) * z + x);//0				
				indices.push_back((width + 1) * (z + 1) + x + 1);//2
				indices.push_back((width + 1) * z + x + 1);//3
			}
		}
	}
}

void Terrain::CreateNormal()
{
	for (UINT i = 0; i < indices.size() / 3; i++)
	{
		UINT index0 = indices[i * 3 + 0];
		UINT index1 = indices[i * 3 + 1];
		UINT index2 = indices[i * 3 + 2];

		VertexType v0 = vertices[index0];
		VertexType v1 = vertices[index1];
		VertexType v2 = vertices[index2];

		Vector3 A = v1.position - v0.position;
		Vector3 B = v2.position - v0.position;

		Vector3 normal;
		D3DXVec3Cross(&normal, &A, &B);

		vertices[index0].normal += normal;
		vertices[index1].normal += normal;
		vertices[index2].normal += normal;
	}

	for (VertexType& vertex : vertices)
		D3DXVec3Normalize(&vertex.normal, &vertex.normal);
}

void Terrain::CreateTangent()
{
	for (UINT i = 0; i < indices.size() / 3; i++)
	{
		UINT index0 = indices[i * 3 + 0];
		UINT index1 = indices[i * 3 + 1];
		UINT index2 = indices[i * 3 + 2];

		VertexType vertex0 = vertices[index0];
		VertexType vertex1 = vertices[index1];
		VertexType vertex2 = vertices[index2];

		Vector3 p0 = vertex0.position;
		Vector3 p1 = vertex1.position;
		Vector3 p2 = vertex2.position;

		Vector2 uv0 = vertex0.uv;
		Vector2 uv1 = vertex1.uv;
		Vector2 uv2 = vertex2.uv;

		Vector3 e0 = p1 - p0;
		Vector3 e1 = p2 - p0;

		float u0 = uv1.x - uv0.x;
		float u1 = uv2.x - uv0.x;
		float v0 = uv1.y - uv0.y;
		float v1 = uv2.y - uv0.y;

		float d = 1.0f / (u0 * v1 - v0 * u1);

		Vector3 tangent;
		tangent = (v1 * e0 - v0 * e1) * d;

		vertices[index0].tangent += tangent;
		vertices[index1].tangent += tangent;
		vertices[index2].tangent += tangent;
	}

	for (VertexType& vertex : vertices)
	{
		Vector3 t = vertex.tangent;
		Vector3 n = vertex.normal;

		Vector3 temp = (t - n * D3DXVec3Dot(&n, &t));
		D3DXVec3Normalize(&temp, &temp);

		vertex.tangent = temp;
	}
}
