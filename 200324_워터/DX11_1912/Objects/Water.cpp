#include "Framework.h"

Water::Water(UINT width, UINT height, float thick, Texture* heightMap)
	: width(width), height(height), thick(thick), heightMap(heightMap), isWireFrame(false)
{
	material = new Material(L"Water");
	material->SetNormalMap(L"Textures/Landscape/WaveNormal.png");
	material->GetBuffer()->data.ambient = Color(0.25f, 0.20f, 1.0f, 1.0f);
	material->GetBuffer()->data.diffuse = Color(0.6f, 0.721f, 0.937f, 0.627f);

	vertexCount = (width + 1) * (height + 1);
	VertexUV* vertices = new VertexUV[vertexCount];

	for (UINT z = 0; z <= height; z++)
	{
		for (UINT x = 0; x <= width; x++)
		{
			UINT index = (width + 1) * z + x;

			vertices[index].position.x = x * thick;
			vertices[index].position.y = 0.0f;
			vertices[index].position.z = z * thick;

			vertices[index].uv.x = x / (float)width;
			vertices[index].uv.y = z / (float)height;
		}
	}

	indexCount = width * height * 6;
	UINT* indices = new UINT[indexCount];

	UINT index = 0;
	for (UINT z = 0; z < height; z++)
	{
		for (UINT x = 0; x < width; x++)
		{
			indices[index + 0] = (width + 1) * z + x;
			indices[index + 1] = (width + 1) * (z + 1) + x;
			indices[index + 2] = (width + 1) * z + x + 1;

			indices[index + 3] = (width + 1) * z + x + 1;
			indices[index + 4] = (width + 1) * (z + 1) + x;
			indices[index + 5] = (width + 1) * (z + 1) + x + 1;

			index += 6;
		}
	}

	vertexBuffer = new VertexBuffer(vertices, sizeof(VertexUV), vertexCount);
	indexBuffer = new IndexBuffer(indices, indexCount);

	worldBuffer = new WorldBuffer();
	vsBuffer = new VSBuffer();
	psBuffer = new PSBuffer();
	psBuffer->data.oceanSize = { width * thick, height * thick };

	rasterizerState[0] = new RasterizerState();
	rasterizerState[1] = new RasterizerState();
	rasterizerState[1]->FillMode(D3D11_FILL_WIREFRAME);

	blendState[0] = new BlendState();
	blendState[1] = new BlendState();
	blendState[1]->Alpha(true);

	delete[] vertices;
	delete[] indices;
}

Water::~Water()
{
	delete material;	
	delete vertexBuffer;
	delete indexBuffer;
	delete rasterizerState[0];
	delete rasterizerState[1];
	delete blendState[0];
	delete blendState[1];
}

void Water::Update()
{
	vsBuffer->data.runningTime += DELTA;
	UpdateWorld();
}

void Water::Render()
{
	vsBuffer->SetVSBuffer(10);
	psBuffer->SetPSBuffer(10);

	heightMap->Set(10);

	vertexBuffer->Set();
	indexBuffer->Set();
	IASetPT();

	SetVS();
	material->Set();

	if (isWireFrame)
		rasterizerState[1]->SetState();
	blendState[1]->SetState();

	DC->DrawIndexed(indexCount, 0, 0);
	blendState[0]->SetState();
	rasterizerState[0]->SetState();
}

void Water::PostRender()
{
	ImGui::Text("WaterOption");
	ImGui::ColorEdit4("WaterDiffuse", material->GetBuffer()->data.diffuse);
	ImGui::ColorEdit4("WaterSpecular", material->GetBuffer()->data.specular);
	ImGui::ColorEdit4("WaterAmbient", material->GetBuffer()->data.ambient);
}
