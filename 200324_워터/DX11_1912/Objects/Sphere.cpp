#include "Framework.h"

Sphere::Sphere(float radius, UINT stackCount, UINT sliceCount)
	: radius(radius), stackCount(stackCount), sliceCount(sliceCount)
{	
	material = new Material(L"Sphere");
	material->SetDiffuseMap(L"Textures/Landscape/Wall.png");
	material->SetSpecularMap(L"Textures/Landscape/Wall_specular.png");
	material->SetNormalMap(L"Textures/Landscape/Wall_normal.png");	

	CreateData();
	CreateTangent();

	vertexBuffer = new VertexBuffer(vertices.data(), sizeof(VertexType), vertices.size());
	indexBuffer = new IndexBuffer(indices.data(), indices.size());		

	collision = new SphereCollision(radius);
}

Sphere::Sphere(wstring shaderFile, float radius, UINT stackCount, UINT sliceCount)
	: radius(radius), stackCount(stackCount), sliceCount(sliceCount), collision(nullptr)
{
	material = new Material(shaderFile);	

	CreateData();
	CreateTangent();

	vertexBuffer = new VertexBuffer(vertices.data(), sizeof(VertexType), vertices.size());
	indexBuffer = new IndexBuffer(indices.data(), indices.size());	
}

Sphere::~Sphere()
{
	delete material;
	delete vertexBuffer;
	delete indexBuffer;

	if (collision != nullptr)
		delete collision;
}

void Sphere::Update()
{
	UpdateWorld();

	if (collision == nullptr)
		return;
	collision->position = worldPosition;	
}

void Sphere::Render()
{
	vertexBuffer->Set();
	indexBuffer->Set();
	IASetPT();

	SetVS();
	material->Set();

	DC->DrawIndexed(indices.size(), 0, 0);

	if (collision == nullptr)
		return;

	collision->Render();
}

void Sphere::CreateData()
{
	float phiStep = D3DX_PI / stackCount;
	float thetaStep = 2.0f * D3DX_PI / sliceCount;

	for (UINT i = 0; i <= stackCount; i++)
	{
		float phi = i * phiStep;

		for (UINT j = 0; j <= sliceCount; j++)
		{
			float theta = j * thetaStep;

			VertexType vertex;

			vertex.normal.x = sin(phi) * cos(theta);
			vertex.normal.y = cos(phi);
			vertex.normal.z = sin(phi) * sin(theta);

			vertex.position = vertex.normal * radius;

			vertex.uv.x = (float)j / sliceCount;
			vertex.uv.y = (float)i / stackCount;

			vertices.push_back(vertex);
		}
	}

	for (UINT i = 0; i < stackCount; i++)
	{
		for (UINT j = 0; j < sliceCount; j++)
		{
			indices.push_back((sliceCount + 1) * i + j);//0
			indices.push_back((sliceCount + 1) * i + j + 1);//1
			indices.push_back((sliceCount + 1) * (i + 1) + j);//2

			indices.push_back((sliceCount + 1) * (i + 1) + j);//2
			indices.push_back((sliceCount + 1) * i + j + 1);//1
			indices.push_back((sliceCount + 1) * (i + 1) + j + 1);//3
		}
	}	
}

void Sphere::CreateTangent()
{
	for (UINT i = 0; i < indices.size() / 3; i++)
	{
		UINT index0 = indices[i * 3 + 0];
		UINT index1 = indices[i * 3 + 1];
		UINT index2 = indices[i * 3 + 2];

		VertexType vertex0 = vertices[index0];
		VertexType vertex1 = vertices[index1];
		VertexType vertex2 = vertices[index2];

		Vector3 p0 = vertex0.position;
		Vector3 p1 = vertex1.position;
		Vector3 p2 = vertex2.position;

		Vector2 uv0 = vertex0.uv;
		Vector2 uv1 = vertex1.uv;
		Vector2 uv2 = vertex2.uv;

		Vector3 e0 = p1 - p0;
		Vector3 e1 = p2 - p0;

		float u0 = uv1.x - uv0.x;
		float u1 = uv2.x - uv0.x;
		float v0 = uv1.y - uv0.y;
		float v1 = uv2.y - uv0.y;

		float d = 1.0f / (u0 * v1 - v0 * u1);

		Vector3 tangent;
		//tangent.x = (v1 * e0.x - v0 * e1.x) * d;
		//tangent.y = (v1 * e0.y - v0 * e1.y) * d;
		//tangent.z = (v1 * e0.z - v0 * e1.z) * d;
		tangent = (v1 * e0 - v0 * e1) * d;

		vertices[index0].tangent += tangent;
		vertices[index1].tangent += tangent;
		vertices[index2].tangent += tangent;
	}

	for (VertexType& vertex : vertices)
	{
		Vector3 t = vertex.tangent;
		Vector3 n = vertex.normal;

		Vector3 temp = (t - n * D3DXVec3Dot(&n, &t));
		D3DXVec3Normalize(&temp, &temp);

		vertex.tangent = temp;
	}
}
