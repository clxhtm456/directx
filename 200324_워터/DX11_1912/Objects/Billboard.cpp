#include "Framework.h"

Billboard::Billboard(wstring diffuseFile)
{
	material = new Material(L"Billboard");
	material->SetDiffuseMap(diffuseFile);

	CreateData();

	D3D11_BLEND_DESC desc = {};

	blendState[0] = new BlendState();
	blendState[1] = new BlendState();
	blendState[1]->Additive();
}

Billboard::~Billboard()
{
	delete material;
	delete vertexBuffer;
	delete indexBuffer;

	delete blendState[0];
	delete blendState[1];
}

void Billboard::Update()
{
	//Vector2 rot = CAMERA->GetRotation();
	//rotation.x = rot.x;
	//rotation.y = rot.y;

	Vector3 camPos = CAMERA->GetPos();
	Vector3 temp = position - camPos;

	rotation.y = atan2(temp.x, temp.z);

	UpdateWorld();
}

void Billboard::Render()
{
	blendState[1]->SetState();

	vertexBuffer->Set();
	indexBuffer->Set();
	IASetPT();

	SetVS();

	material->Set();

	DC->DrawIndexed(6, 0, 0);	

	blendState[0]->SetState();
}

void Billboard::CreateData()
{
	VertexUV vertices[4];
	vertices[0].position = Vector3(-0.5f, 0.5f, 0.0f);
	vertices[1].position = Vector3(0.5f, 0.5f, 0.0f);
	vertices[2].position = Vector3(-0.5f, -0.5f, 0.0f);
	vertices[3].position = Vector3(0.5f, -0.5f, 0.0f);

	vertices[0].uv = Vector2(0, 0);
	vertices[1].uv = Vector2(1, 0);
	vertices[2].uv = Vector2(0, 1);
	vertices[3].uv = Vector2(1, 1);

	vertexBuffer = new VertexBuffer(vertices, sizeof(VertexUV), 4);

	UINT indices[] = {
		0, 1, 2,
		1, 3, 2
	};

	indexBuffer = new IndexBuffer(indices, 6);
}
