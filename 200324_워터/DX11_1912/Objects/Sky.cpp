#include "Framework.h"

Sky::Sky()
{
	sphere = new Sphere(L"Sky");

	buffer = new SkyBuffer;

	cullMode[0] = new RasterizerState();
	cullMode[1] = new RasterizerState();
	cullMode[1]->FrontCounterClockwise(true);

	depthMode[0] = new DepthStencilState();
	depthMode[1] = new DepthStencilState();
	depthMode[1]->DepthEnable(false);
}

Sky::~Sky()
{
	delete sphere;

	delete buffer;

	delete cullMode[0];
	delete cullMode[1];

	delete depthMode[0];
	delete depthMode[1];
}

void Sky::Update()
{
	sphere->position = CAMERA->GetPos();

	sphere->Update();
}

void Sky::Render()
{
	buffer->SetPSBuffer(10);

	cullMode[1]->SetState();
	depthMode[1]->SetState();

	sphere->Render();

	cullMode[0]->SetState();
	depthMode[0]->SetState();
}

void Sky::PostRender()
{
	ImGui::ColorEdit4("Center", buffer->data.center);
	ImGui::ColorEdit4("Apex", buffer->data.apex);

	ImGui::SliderFloat("Height", &buffer->data.height, 0.0f, 5.0f);
}
