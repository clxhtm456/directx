#pragma once

class Water : public Transform
{
private:
	class VSBuffer : public ConstBuffer
	{
	public:
		struct Data
		{
			float waveFrequency;
			float waveAmplitude;
			Vector2 textureScale;

			Vector2 bumpSpeed;
			float bumpHeight;
			float runningTime;
		}data;

		VSBuffer() : ConstBuffer(&data, sizeof(Data))
		{
			data.waveFrequency = 0.05f;
			data.waveAmplitude = 1.0f;
			data.textureScale = Vector2(14, 14);
			data.bumpSpeed = Vector2(0.0f, 0.05f);
			data.bumpHeight = 0.6f;
			data.runningTime = 0.0f;
		}
	};
	class PSBuffer : public ConstBuffer
	{
	public:
		struct Data
		{
			Color shallowColor;
			Color deepColor;

			float bias;
			float power;
			float amount;
			float shoreBlend;

			Vector2 oceanSize;
			float heightRatio;

			float padding;
		}data;

		PSBuffer() : ConstBuffer(&data, sizeof(Data))
		{
			data.shallowColor = Color(0.7f, 0.85f, 0.8f, 1.0f);
			data.deepColor = Color(0.2f, 0.5f, 0.95f, 1.0f);

			data.bias = 0.8f;
			data.power = 0.5f;
			data.amount = 0.5f;

			data.heightRatio = 7.5f;
			data.shoreBlend = 35.0f;
		}
	};

	Material* material;	

	VSBuffer* vsBuffer;
	PSBuffer* psBuffer;

	VertexBuffer* vertexBuffer;
	IndexBuffer* indexBuffer;

	UINT width, height;
	UINT vertexCount, indexCount;
	float thick;
	bool isWireFrame;

	RasterizerState* rasterizerState[2];
	BlendState* blendState[2];

	Texture* heightMap;

public:
	Water(UINT width, UINT height, float thick, Texture* heightMap);
	~Water();

	void Update();
	void Render();
	void PostRender();
};