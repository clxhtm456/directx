#include "Framework.h"

Brook::Brook()
	: GameAnimModel("Brook"), state(IDLE), moveSpeed(10.0f), rotSpeed(5.0f),
	accelation(10.0f), deceleration(3.0f), velocity(0, 0, 0)
{
	/*
	string name = "Brook";
	Exporter* exporter = new Exporter("FbxData/Models/" + name + ".fbx");
	exporter->ExporterMaterial(name + ".mat", name + "/");
	exporter->ExporterMesh(name + ".mesh");
	delete exporter;

	exporter = new Exporter("FbxData/Animations/Brook/Idle.fbx");
	exporter->ExporterAnimation(0, "Idle.clip", name + "/");
	delete exporter;

	exporter = new Exporter("FbxData/Animations/Brook/Run.fbx");
	exporter->ExporterAnimation(0, "Run.clip", name + "/");
	delete exporter;

	exporter = new Exporter("FbxData/Animations/Brook/Attack.fbx");
	exporter->ExporterAnimation(0, "Attack.clip", name + "/");
	delete exporter;	
	*/		

	//AddClip(L"Brook/Idle.clip");
	//AddClip(L"Brook/Run.clip");
	//AddClip(L"Brook/Attack.clip");
	//clips[ATTACK]->SetEndEvent(bind(&Brook::SetIdle, this));

	//tweener->Play(clips[0], true);	
}

Brook::~Brook()
{
}

void Brook::Update()
{
	Input();
	Move();

	position.y = terrain->GetPosY(position);
	__super::Update();
}

void Brook::Render()
{
	__super::Render();
}

void Brook::PostRender()
{
}

void Brook::SetAnimation(AnimState value, bool isRepeat)
{
	if (state != value)
	{
		state = value;
		tweener->Play(clips[value], isRepeat);
	}
}

void Brook::SetIdle()
{
	SetAnimation(IDLE, true);
}

void Brook::Input()
{
	if (state == ATTACK)
		return;

	if (KEYPRESS(VK_UP))
		velocity -= forward * accelation * DELTA;
	if (KEYPRESS(VK_DOWN))
		velocity += forward * accelation * DELTA;

	if (KEYPRESS(VK_LEFT))
		rotation.y -= rotSpeed * DELTA;
	if (KEYPRESS(VK_RIGHT))
		rotation.y += rotSpeed * DELTA;

	if (KEYDOWN(VK_SPACE))
	{
		SetAnimation(ATTACK, false);
	}
}

void Brook::Move()
{
	if (state == ATTACK)
		return;

	float magnitude = D3DXVec3Length(&velocity);

	if (magnitude > 1.0f)
		D3DXVec3Normalize(&velocity, &velocity);

	if (magnitude > 0.1f)
	{
		position += velocity * moveSpeed * DELTA;

		if (state != RUN)
		{
			state = RUN;
			tweener->Play(clips[state], true);
		}

		Vector3 zero = Vector3(0, 0, 0);

		D3DXVec3Lerp(&velocity, &velocity, &zero, deceleration * DELTA);
	}
	else
	{
		if (state == RUN)
			SetIdle();
	}
}
