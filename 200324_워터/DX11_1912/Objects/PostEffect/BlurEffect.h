#pragma once

class BlurEffect : public Render2D
{
private:
	class BlurBuffer : public ConstBuffer
	{
	public:
		struct Data
		{
			int count;
			float width;
			float height;
			int select;
		}data;

		BlurBuffer() : ConstBuffer(&data, sizeof(Data))
		{
			data.count = 1;
			data.width = 0.0f;
			data.height = 0.0f;
			data.select = 0;
		}
	};

	BlurBuffer* buffer;

public:
	BlurEffect();
	~BlurEffect();

	void Render();
};