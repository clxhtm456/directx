#include "Framework.h"

MonoEffect::MonoEffect()
	: Render2D(L"Mono")
{
	colorBuffer = new ColorBuffer();
}

MonoEffect::~MonoEffect()
{
	delete colorBuffer;
}

void MonoEffect::Render()
{
	colorBuffer->SetPSBuffer(10);

	Render2D::Render();

	ImGui::ColorEdit4("MonoColor", colorBuffer->data.color);
}
