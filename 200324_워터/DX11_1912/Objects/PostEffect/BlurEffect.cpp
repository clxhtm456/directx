#include "Framework.h"

BlurEffect::BlurEffect()
	: Render2D(L"Blur")
{
	buffer = new BlurBuffer();	
}

BlurEffect::~BlurEffect()
{
	delete buffer;
}

void BlurEffect::Render()
{
	if (buffer->data.select != 5)
	{
		buffer->data.width = scale.x;
		buffer->data.height = scale.y;
	}

	buffer->SetPSBuffer(10);

	Render2D::Render();

	ImGui::Text("Blur Option");
	ImGui::SliderInt("Count", &buffer->data.count, 1, 50);
	ImGui::SliderInt("Select", &buffer->data.select, 0, 6);

	if (buffer->data.select == 5)
	{
		ImGui::SliderFloat("Radius", &buffer->data.width, 0, 30.0f);
		ImGui::SliderFloat("Amount", &buffer->data.height, 0, 30.0f);
	}
}
