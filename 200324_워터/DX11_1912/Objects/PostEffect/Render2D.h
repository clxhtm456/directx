#pragma once

class Render2D
{
public:
	Vector2 position;
	Vector2 scale;

protected:
	Shader* shader;
	ID3D11ShaderResourceView* srv;

	WorldBuffer* worldBuffer;

	VertexBuffer* vertexBuffer;

	Matrix view;
	Matrix orthographic;

	DepthStencilState* depthState[2];

public:
	Render2D(wstring shaderFile = L"Render2D");
	virtual ~Render2D();

	virtual void Update();
	virtual void Render();

	void SetSRV(ID3D11ShaderResourceView* value) { srv = value; }
	void SetWorld(Matrix world) { worldBuffer->SetWorld(world); }
};