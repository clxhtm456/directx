#pragma once

class MonoEffect : public Render2D
{
private:
	ColorBuffer* colorBuffer;

public:
	MonoEffect();
	~MonoEffect();

	void Render();
};