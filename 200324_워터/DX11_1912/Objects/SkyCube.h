#pragma once

class SkyCube
{
private:
	Sphere* sphere;
	Texture* cubeMap;

	RasterizerState* cullMode[2];
	DepthStencilState* depthMode[2];

public:
	SkyCube();
	~SkyCube();

	void Update();
	void Render();
};