#pragma once

class Terrain : public Transform
{
private:
	typedef VertexUVNormalTangent VertexType;

	Material* material;

	VertexBuffer* vertexBuffer;
	IndexBuffer* indexBuffer;

	vector<VertexType> vertices;
	vector<UINT> indices;

	UINT width, height;

	ID3D11RasterizerState* state;

	Texture* heightMap;

public:
	Terrain();
	~Terrain();

	void Update();
	void Render();
	void PostRender();

	bool Picking(OUT Vector3* position);
	float GetPosY(IN Vector3 position);

	Vector2 GetSize() { return Vector2(width, height); }
private:
	void CreateData();
	void CreateNormal();
	void CreateTangent();
};