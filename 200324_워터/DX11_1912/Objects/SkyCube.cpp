#include "Framework.h"

SkyCube::SkyCube()
{
	sphere = new Sphere(L"SkyCube", 1000.0f);
	cubeMap = Texture::Add(L"Textures/Landscape/Snow_ENV.dds");

	cullMode[0] = new RasterizerState();
	cullMode[1] = new RasterizerState();
	cullMode[1]->FrontCounterClockwise(true);

	depthMode[0] = new DepthStencilState();
	depthMode[1] = new DepthStencilState();
	depthMode[1]->DepthEnable(false);
}

SkyCube::~SkyCube()
{
	delete sphere;

	delete cullMode[0];
	delete cullMode[1];

	delete depthMode[0];
	delete depthMode[1];
}

void SkyCube::Update()
{
	sphere->position = CAMERA->GetPos();

	sphere->Update();
}

void SkyCube::Render()
{
	cubeMap->Set(10);

	cullMode[1]->SetState();
	depthMode[1]->SetState();

	sphere->Render();

	cullMode[0]->SetState();
	depthMode[0]->SetState();
}
