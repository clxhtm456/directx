#pragma once

class Billboard : public Transform
{
private:
	Material* material;

	VertexBuffer* vertexBuffer;
	IndexBuffer* indexBuffer;

	BlendState* blendState[2];
public:
	Billboard(wstring diffuseFile);
	~Billboard();

	void Update();
	void Render();

	void CreateData();
};