#pragma once

class TerrainEditor : public Transform
{
private:
	typedef VertexUVNormalTangentAlpha VertexType;

	Material* material;

	BrushBuffer* brushBuffer;

	VertexBuffer* vertexBuffer;
	IndexBuffer* indexBuffer;

	vector<VertexType> vertices;
	vector<UINT> indices;

	UINT width, height;

	ID3D11RasterizerState* state;

	bool isUp;
	float adjustValue;

	bool isPainting;
	float paintValue;

	int selectMapNum;

	vector<float> heights;

	//ID3D11ShaderResourceView* alphaMap;
	ID3D11ShaderResourceView* secondMap;
	ID3D11ShaderResourceView* thirdMap;

	struct InputDesc
	{
		UINT index;
		Vector3 v0, v1, v2;
	};

	struct OutputDesc
	{
		UINT picked;
		float u, v, distance;
	};

	ComputeShader* computeShader;
	RayBuffer* rayBuffer;
	StructBuffer* structBuffer;
	InputDesc* input;
	OutputDesc* output;

	UINT size;
public:
	TerrainEditor(UINT width, UINT height);
	~TerrainEditor();

	void Update();
	void Render();
	void PostRender();

	bool Picking(OUT Vector3* position);
	bool ComputePicking(OUT Vector3* position);
	void AdjustY(Vector3 position, float value);
	void PaintBrush(Vector3 position, float value);
private:
	void CreateData();
	void CreateNormal();
	void CreateTangent();

	void Save();
	void Load();
};