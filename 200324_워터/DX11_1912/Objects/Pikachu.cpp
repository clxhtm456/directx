#include "Framework.h"

Pikachu::Pikachu()
	: GameAnimModel("Pikachu"), state(IDLE), moveSpeed(30.0f), rotSpeed(5.0f),
	accelation(10.0f), deceleration(3.0f), velocity(0, 0, 0)
{
	model->SetSpecularMap(L"FbxData/Materials/Pikachu/Kachujin_specular.png");
	model->SetNormalMap(L"FbxData/Materials/Pikachu/Kachujin_normal.png");

	AddClip(L"Pikachu/Idle.clip");
	AddClip(L"Pikachu/Run.clip");
	AddClip(L"Pikachu/Attack.clip");
	clips[ATTACK]->SetEndEvent(bind(&Pikachu::SetIdle, this));

	tweener->Play(clips[0], true);

	scale = Vector3(0.05f, 0.05f, 0.05f);
}

Pikachu::~Pikachu()
{
}

void Pikachu::Update()
{
	//Input();
	Move();
	Rotate();
	
	position.y = terrain->GetPosY(position);
	__super::Update();
}

void Pikachu::Render()
{
	//model->SetShader(L"ModelAnimation");
	__super::Render();
}

void Pikachu::PostRender()
{
}

void Pikachu::SetAnimation(AnimState value, bool isRepeat)
{
	if (state != value)
	{
		state = value;
		tweener->Play(clips[value], isRepeat);
	}
}

void Pikachu::SetIdle()
{	
	SetAnimation(IDLE, true);	
}

void Pikachu::Input()
{
	if (state == ATTACK)
		return;

	if (KEYPRESS(VK_UP))
		velocity -= forward * accelation * DELTA;	
	if (KEYPRESS(VK_DOWN))
		velocity += forward * accelation * DELTA;
	
	if (KEYPRESS(VK_LEFT))
		rotation.y -= rotSpeed * DELTA;
	if (KEYPRESS(VK_RIGHT))
		rotation.y += rotSpeed * DELTA;	

	if (KEYDOWN(VK_SPACE))
	{		
		SetAnimation(ATTACK, false);
	}

	if (KEYDOWN(VK_LBUTTON))
	{
		if (aStar->IsCollisionObstalce(CAMERA->GetPickingRay(), FLT_MAX))
			return;

		terrain->Picking(&destPos);

		Ray ray;
		ray.position = position;
		D3DXVec3Normalize(&ray.direction, &(destPos - position));

		path.clear();

		float distance = Distance(position, destPos);

		if (aStar->IsCollisionObstalce(ray, distance))
		{
			int startIndex = aStar->FindCloseNode(position);
			int endIndex = aStar->FindCloseNode(destPos);

			aStar->Reset();			

			path = aStar->FindPath(startIndex, endIndex);			

			aStar->MakeDirectPath(position, destPos, path);

			path.insert(path.begin(), destPos);

			int pathSize = path.size();

			while (path.size() > 2)
			{
				vector<Vector3> tempPath;
				for (int i = 1; i < path.size() - 1; i++)
					tempPath.push_back(path[i]);

				Vector3 start = path.back();
				Vector3 end = path.front();

				aStar->MakeDirectPath(start, end, tempPath);

				path.clear();
				path.push_back(end);

				for (int i = 0; i < tempPath.size(); i++)
					path.push_back(tempPath[i]);

				path.push_back(start);

				if (pathSize == path.size())
					break;
				else
					pathSize = path.size();
			}			
		}
		else
		{
			path.push_back(destPos);
		}
	}
}

void Pikachu::Move()
{
	if (state == ATTACK)
		return;

	MovePath();

	float magnitude = D3DXVec3Length(&velocity);

	if (magnitude > 1.0f)
		D3DXVec3Normalize(&velocity, &velocity);

	if (magnitude > 0.1f)
	{
		position += velocity * moveSpeed * DELTA;

		if (state != RUN)
		{
			state = RUN;
			tweener->Play(clips[state], true);
		}

		Vector3 zero = Vector3(0, 0, 0);

		D3DXVec3Lerp(&velocity, &velocity, &zero, deceleration * DELTA);
	}
	else
	{
		if (state == RUN)
			SetIdle();
	}
}

void Pikachu::MovePath()
{
	if (path.empty())
		return;

	Vector3 dest = path.back();

	Vector3 direction = dest - position;

	D3DXVec3Normalize(&velocity, &direction);

	if (D3DXVec3Length(&direction) < 0.4f)
		path.pop_back();
}

void Pikachu::Rotate()
{
	if (D3DXVec3Length(&velocity) < 0.1f)
		return;

	Vector3 start;
	D3DXVec3Normalize(&start, &forward);

	Vector3 end;
	D3DXVec3Normalize(&end, &velocity);

	float dotValue = D3DXVec3Dot(&start, &end);

	float angle = acos(dotValue);

	if (angle < 0.1f)
		return;

	Vector3 cross;
	D3DXVec3Cross(&cross, &start, &end);

	if (cross.y > 0.0f)
		rotation.y -= DELTA * rotSpeed;
	else
		rotation.y += DELTA * rotSpeed;
	
	//rotation.y = atan2(-velocity.x, -velocity.z);
}
