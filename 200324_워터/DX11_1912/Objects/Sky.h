#pragma once

class Sky
{
private:
	class SkyBuffer : public ConstBuffer
	{
	public:
		struct Data
		{
			Color center;
			Color apex;

			float height;

			float padding[3];
		}data;

		SkyBuffer() : ConstBuffer(&data, sizeof(Data))
		{
			data.center = Color(1.0f, 0.7f, 0.3f, 1.0f);
			data.apex = Color(0.5f, 0.5f, 1.0f, 1.0f);

			data.height = 1.0f;
		}
	};

	SkyBuffer* buffer;
	Sphere* sphere;

	RasterizerState* cullMode[2];
	DepthStencilState* depthMode[2];

public:
	Sky();
	~Sky();

	void Update();
	void Render();
	void PostRender();
};