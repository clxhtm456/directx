#include "Framework.h"

TerrainData::TerrainData(wstring heightFile)
{
	heightMap = Texture::Add(heightFile);

	CreateData();
	CreateNormal();
	AlignVertexData();
}

TerrainData::~TerrainData()
{
}

void TerrainData::CreateData()
{
	vector<Color> pixels = heightMap->ReadPixels();

	width = heightMap->GetWidth() - 1;
	height = heightMap->GetHeight() - 1;

	{//Vertices
		for (UINT z = 0; z <= height; z++)
		{
			for (UINT x = 0; x <= width; x++)
			{
				VertexType vertex;
				vertex.position = Vector3(x, rand() % 2, z);
				vertex.uv = Vector2(1.0f - x / (float)width, 1.0f - z / (float)height);

				UINT index = (width + 1) * z + x;
				vertex.position.y = pixels[index].r * 20.0f;

				prevData.push_back(vertex);
			}
		}
	}

	{//Indices
		for (UINT z = 0; z < height; z++)
		{
			for (UINT x = 0; x < width; x++)
			{
				indices.push_back((width + 1) * z + x);//0
				indices.push_back((width + 1) * (z + 1) + x);//1
				indices.push_back((width + 1) * (z + 1) + x + 1);//2

				indices.push_back((width + 1) * z + x);//0				
				indices.push_back((width + 1) * (z + 1) + x + 1);//2
				indices.push_back((width + 1) * z + x + 1);//3
			}
		}
	}
}

void TerrainData::CreateNormal()
{
	for (UINT i = 0; i < indices.size() / 3; i++)
	{
		UINT index0 = indices[i * 3 + 0];
		UINT index1 = indices[i * 3 + 1];
		UINT index2 = indices[i * 3 + 2];

		VertexType v0 = prevData[index0];
		VertexType v1 = prevData[index1];
		VertexType v2 = prevData[index2];

		Vector3 A = v1.position - v0.position;
		Vector3 B = v2.position - v0.position;

		Vector3 normal;
		D3DXVec3Cross(&normal, &A, &B);

		prevData[index0].normal += normal;
		prevData[index1].normal += normal;
		prevData[index2].normal += normal;
	}

	for (VertexType& vertex : prevData)
		D3DXVec3Normalize(&vertex.normal, &vertex.normal);
}

void TerrainData::AlignVertexData()
{
	vertices.resize(indices.size());

	UINT index = 0;
	for (UINT z = 0; z < height; z++)
	{
		for (UINT x = 0; x < width; x++)
		{
			UINT index1 = (width + 1) * z + x;
			UINT index2 = (width + 1) * z + x + 1;
			UINT index3 = (width + 1) * (z + 1) + x;
			UINT index4 = (width + 1) * (z + 1) + x + 1;

			vertices[index++] = prevData[index3];
			vertices[index++] = prevData[index4];
			vertices[index++] = prevData[index1];

			vertices[index++] = prevData[index1];
			vertices[index++] = prevData[index4];
			vertices[index++] = prevData[index2];
		}
	}
}
