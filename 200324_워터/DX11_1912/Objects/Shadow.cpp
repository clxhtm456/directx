#include "Framework.h"

Shadow::Shadow()
{
	depthShader = Shader::Add(L"ShadowDepth");
	renderShader = Shader::Add(L"Shadow");

	depthTarget = new RenderTarget(2048, 2048);

	depthRender = new Render2D();
	depthRender->position = Vector2(200, 200);
	depthRender->scale = Vector2(400, 400);

	depthRender->SetSRV(depthTarget->GetSRV());

	perspective = new Perspective(1, 1, D3DX_PI * 0.5f, 1, 1000);

	vpBuffer = new ViewProjectionBuffer();

	buffer = new ShadowBuffer();
	buffer->data.size = Vector2(2048, 2048);

	samplerState[0] = new SamplerState();
	samplerState[1] = new SamplerState();
	samplerState[1]->ComparisonFunc(D3D11_COMPARISON_LESS_EQUAL);
	samplerState[1]->Address(D3D11_TEXTURE_ADDRESS_MIRROR);
	samplerState[1]->Fillter(D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR);
}

Shadow::~Shadow()
{
	delete depthTarget;
	delete depthRender;
	
	delete perspective;

	delete vpBuffer;
	delete buffer;

	delete samplerState[0];
	delete samplerState[1];
}

void Shadow::Update()
{
	depthRender->Update();
}

void Shadow::PreRender()
{
	depthTarget->Set();

	Matrix V;

	Vector3 lightPos = LIGHT->data.direction * -100;
	D3DXMatrixLookAtLH(&V, &lightPos, &Vector3(0, 0, 0), &Vector3(0, 1, 0));	

	//VP->SetView(V);
	//VP->SetProjection(perspective->GetMatrix());
	//VP->SetVSBuffer(0);

	vpBuffer->SetView(V);
	vpBuffer->SetProjection(perspective->GetMatrix());
	vpBuffer->SetVSBuffer(0);

	for (GameModel* obj : objs)
	{
		obj->SetShader(L"ShadowDepth");

		obj->Update();		
		obj->Render();
	}
}

void Shadow::Render()
{
	vpBuffer->SetVSBuffer(10);
	buffer->SetPSBuffer(10);

	ID3D11ShaderResourceView* srv = depthTarget->GetSRV();
	DC->PSSetShaderResources(10, 1, &srv);

	samplerState[1]->Set(11);

	for (GameModel* obj : objs)
	{
		if (obj->IsShadowRender())
		{
			obj->SetShader(L"Shadow");
			obj->Render();
		}
	}	
}

void Shadow::PostRender()
{
	ImGui::SliderInt("Selected", &buffer->data.selcted, 0, 2);
	ImGui::SliderFloat("Shadow Bias", &buffer->data.bias, -0.1f, 0.1f);

	depthRender->Render();
}

void Shadow::Add(GameModel* obj)
{
	objs.push_back(obj);
}
