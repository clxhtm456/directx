#pragma once

class Brook : public GameAnimModel
{
private:
	enum AnimState
	{
		IDLE,
		RUN,
		ATTACK
	}state;

	float moveSpeed;
	float rotSpeed;

	float accelation;
	float deceleration;

	Vector3 velocity;

	Terrain* terrain;
public:
	Brook();
	~Brook();

	void Update();
	void Render();
	void PostRender();

	void SetAnimation(AnimState value, bool isRepeat);
	void SetIdle();

	void Input();
	void Move();

	void SetTerrain(Terrain* terrain) { this->terrain = terrain; }
};