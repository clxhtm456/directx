#include "Framework.h"

TerrainEditor::TerrainEditor(UINT width, UINT height)
	: width(width), height(height), isUp(true), adjustValue(30.0f),
	isPainting(true), paintValue(10.0f), selectMapNum(0)
{
	//material = new Material(L"TerrainBrush");
	material = new Material(L"Splatting");
	material->SetDiffuseMap(L"Textures/Landscape/Stones.png");
	material->SetNormalMap(L"Textures/Landscape/Stones_normal.png");	

	//alphaMap = Texture::LoadSRV(L"Textures/HeightMaps/AlphaMap.png");
	secondMap = Texture::LoadSRV(L"Textures/Landscape/Dirt2.png");
	thirdMap = Texture::LoadSRV(L"Textures/Landscape/Dirt3.png");

	brushBuffer = new BrushBuffer();

	CreateData();
	CreateNormal();
	CreateTangent();

	vertexBuffer = new VertexBuffer(vertices.data(), sizeof(VertexType), vertices.size());
	indexBuffer = new IndexBuffer(indices.data(), indices.size());

	D3D11_RASTERIZER_DESC rsDesc = {};
	rsDesc.AntialiasedLineEnable = false;
	rsDesc.CullMode = D3D11_CULL_BACK;
	rsDesc.FillMode = D3D11_FILL_WIREFRAME;

	DEVICE->CreateRasterizerState(&rsDesc, &state);

	computeShader = new ComputeShader(L"Intersection");

	size = indices.size() / 3;

	structBuffer = new StructBuffer(input, sizeof(InputDesc), size,
		sizeof(OutputDesc), size);

	rayBuffer = new RayBuffer();
	output = new OutputDesc[size];
}

TerrainEditor::~TerrainEditor()
{
	delete material;

	delete brushBuffer;

	delete vertexBuffer;
	delete indexBuffer;

	state->Release();

	delete computeShader;
	delete rayBuffer;
	delete structBuffer;

	delete[] input;
	delete[] output;
}

void TerrainEditor::Update()
{
	if (KEYDOWN(VK_F1))
		Save();

	if (KEYDOWN(VK_F2))
		Load();

	if (KEYPRESS(VK_LBUTTON))
	{
		if (isPainting)
		{
			if (isUp)
				PaintBrush(brushBuffer->data.location, paintValue);
			else
				PaintBrush(brushBuffer->data.location, -paintValue);
		}
		else
		{
			if (isUp)
				AdjustY(brushBuffer->data.location, adjustValue);
			else
				AdjustY(brushBuffer->data.location, -adjustValue);
		}
	}	

	if (KEYDOWN(VK_F5) || KEYUP(VK_LBUTTON))
	{
		CreateNormal();
		vertexBuffer->UpdateBuffer(vertices.data(), vertices.size());
	}

	UpdateWorld();
}

void TerrainEditor::Render()
{
	vertexBuffer->Set();
	indexBuffer->Set();
	IASetPT();

	brushBuffer->SetVSBuffer(10);
	//DC->PSSetShaderResources(10, 1, &alphaMap);
	DC->PSSetShaderResources(10, 1, &secondMap);
	DC->PSSetShaderResources(11, 1, &thirdMap);

	SetVS();
	material->Set();

	DC->DrawIndexed(indices.size(), 0, 0);
}

void TerrainEditor::PostRender()
{
	Vector3 pos;
	//Picking(&pos);
	ComputePicking(&pos);
	brushBuffer->data.location = pos;
	ImGui::SliderFloat3("PickPos", pos, -1000, 1000);
	ImGui::Text("BrushMode");
	ImGui::SliderInt("Type", &brushBuffer->data.type, 0, 2);	
	ImGui::SliderFloat("Range", &brushBuffer->data.range, 0, 20);
	ImGui::ColorEdit3("Color", brushBuffer->data.color);
	ImGui::Checkbox("IsUp", &isUp);
	ImGui::SliderInt("SelectMap", &selectMapNum, 0, 1);
}

bool TerrainEditor::Picking(OUT Vector3* position)
{
	Ray ray = CAMERA->GetPickingRay();

	for (UINT z = 0; z < height; z++)
	{
		for (UINT x = 0; x < width; x++)
		{
			UINT index[4];
			index[0] = (width + 1) * z + x;
			index[1] = (width + 1) * z + x + 1;
			index[2] = (width + 1) * (z + 1) + x;
			index[3] = (width + 1) * (z + 1) + x + 1;

			Vector3 p[4];
			for (UINT i = 0; i < 4; i++)
				p[i] = vertices[index[i]].position;

			float u, v, distance;
			if (D3DXIntersectTri(&p[0], &p[1], &p[2], &ray.position, &ray.direction,
				&u, &v, &distance))
			{
				*position = p[0] + (p[1] - p[0]) * u + (p[2] - p[0]) * v;
				return true;
			}

			if (D3DXIntersectTri(&p[3], &p[1], &p[2], &ray.position, &ray.direction,
				&u, &v, &distance))
			{
				*position = p[3] + (p[1] - p[3]) * u + (p[2] - p[3]) * v;
				return true;
			}
		}
	}

	return false;
}

bool TerrainEditor::ComputePicking(OUT Vector3* position)
{
	Ray ray = CAMERA->GetPickingRay();
	rayBuffer->data.position = ray.position;
	rayBuffer->data.direction = ray.direction;
	rayBuffer->data.size = size;
	computeShader->Set();

	rayBuffer->SetCSBuffer(0);

	DC->CSSetShaderResources(0, 1, &structBuffer->GetSrv());
	DC->CSSetUnorderedAccessViews(0, 1, &structBuffer->GetUav(), nullptr);

	UINT x = ceil((float)size / 1024.0f);

	DC->Dispatch(x, 1, 1);

	structBuffer->Copy(output, sizeof(OutputDesc) * size);

	for (UINT i = 0; i < size; i++)
	{
		OutputDesc temp = output[i];

		if (temp.picked)
		{
			*position = ray.position + ray.direction * temp.distance;
			return true;
		}
	}

	return false;
}

void TerrainEditor::AdjustY(Vector3 position, float value)
{
	switch (brushBuffer->data.type)
	{
	case 1:
	{
		for (VertexType& vertex : vertices)
		{
			Vector2 p1 = Vector2(vertex.position.x, vertex.position.z);
			Vector2 p2 = Vector2(position.x, position.z);

			float dist = D3DXVec2Length(&(p2 - p1));
			if (dist <= brushBuffer->data.range)
			{
				vertex.position.y += value * DELTA;
			}
		}
	}
	break;
	case 2:
	{
		UINT size = (UINT)brushBuffer->data.range;

		POINT min, max;
		min.x = (UINT)position.x - size;
		min.y = (UINT)position.z - size;
		max.x = (UINT)position.x + size;
		max.y = (UINT)position.z + size;

		if (min.x < 0) min.x = 0;
		if (min.y < 0) min.y = 0;
		if (max.x >= width) max.x = width;
		if (max.y >= height) max.y = height;

		for (UINT z = min.y; z <= max.y; z++)
		{
			for (UINT x = min.x; x <= max.x; x++)
			{
				UINT index = (width + 1) * z + x;

				vertices[index].position.y += value * DELTA;

				if (vertices[index].position.y > 1.0f)
					vertices[index].position.y = 1.0f;
				else if (vertices[index].position.y < 0.0f)
					vertices[index].position.y = 0.0f;
			}
		}
	}
	break;
	}

	vertexBuffer->UpdateBuffer(vertices.data(), vertices.size());
}

void TerrainEditor::PaintBrush(Vector3 position, float value)
{
	switch (brushBuffer->data.type)
	{
	case 1:
	{
		for (VertexType& vertex : vertices)
		{
			Vector2 p1 = Vector2(vertex.position.x, vertex.position.z);
			Vector2 p2 = Vector2(position.x, position.z);

			float dist = D3DXVec2Length(&(p2 - p1));
			if (dist <= brushBuffer->data.range)
			{
				vertex.alpha[selectMapNum] += value * DELTA;

				if (vertex.alpha[selectMapNum] > 1.0f)
					vertex.alpha[selectMapNum] = 1.0f;
				else if (vertex.alpha[selectMapNum] < 0.0f)
					vertex.alpha[selectMapNum] = 0.0f;
			}
		}
	}
	break;
	case 2:
	{
		UINT size = (UINT)brushBuffer->data.range;

		POINT min, max;
		min.x = (UINT)position.x - size;
		min.y = (UINT)position.z - size;
		max.x = (UINT)position.x + size;
		max.y = (UINT)position.z + size;

		if (min.x < 0) min.x = 0;
		if (min.y < 0) min.y = 0;
		if (max.x >= width) max.x = width;
		if (max.y >= height) max.y = height;

		for (UINT z = min.y; z <= max.y; z++)
		{
			for (UINT x = min.x; x <= max.x; x++)
			{
				UINT index = (width + 1) * z + x;

				vertices[index].alpha[selectMapNum] += value * DELTA;

				if (vertices[index].alpha[selectMapNum] > 1.0f)
					vertices[index].alpha[selectMapNum] = 1.0f;
				else if (vertices[index].alpha[selectMapNum] < 0.0f)
					vertices[index].alpha[selectMapNum] = 0.0f;
			}
		}
	}
	break;	
	}

	vertexBuffer->UpdateBuffer(vertices.data(), vertices.size());
}

void TerrainEditor::CreateData()
{
	{//Vertices
		for (UINT z = 0; z <= height; z++)
		{
			for (UINT x = 0; x <= width; x++)
			{
				VertexType vertex;
				vertex.position = Vector3(x, 0, z);
				vertex.uv = Vector2(1.0f - x / (float)width, 1.0f - z / (float)height);

				vertices.push_back(vertex);
			}
		}
	}

	{//Indices
		for (UINT z = 0; z < height; z++)
		{
			for (UINT x = 0; x < width; x++)
			{
				indices.push_back((width + 1) * z + x);//0
				indices.push_back((width + 1) * (z + 1) + x);//1
				indices.push_back((width + 1) * (z + 1) + x + 1);//2

				indices.push_back((width + 1) * z + x);//0				
				indices.push_back((width + 1) * (z + 1) + x + 1);//2
				indices.push_back((width + 1) * z + x + 1);//3
			}
		}
	}

	input = new InputDesc[indices.size() / 3];

	for (UINT i = 0; i < indices.size() / 3; i++)
	{
		UINT index0 = indices[i * 3 + 0];
		UINT index1 = indices[i * 3 + 1];
		UINT index2 = indices[i * 3 + 2];

		input[i].v0 = vertices[index0].position;
		input[i].v1 = vertices[index1].position;
		input[i].v2 = vertices[index2].position;

		input[i].index = i;
	}
}

void TerrainEditor::CreateNormal()
{
	for (UINT i = 0; i < indices.size() / 3; i++)
	{
		UINT index0 = indices[i * 3 + 0];
		UINT index1 = indices[i * 3 + 1];
		UINT index2 = indices[i * 3 + 2];

		VertexType v0 = vertices[index0];
		VertexType v1 = vertices[index1];
		VertexType v2 = vertices[index2];

		Vector3 A = v1.position - v0.position;
		Vector3 B = v2.position - v0.position;

		Vector3 normal;
		D3DXVec3Cross(&normal, &A, &B);

		vertices[index0].normal += normal;
		vertices[index1].normal += normal;
		vertices[index2].normal += normal;
	}

	for (VertexType& vertex : vertices)
		D3DXVec3Normalize(&vertex.normal, &vertex.normal);
}

void TerrainEditor::CreateTangent()
{
	for (UINT i = 0; i < indices.size() / 3; i++)
	{
		UINT index0 = indices[i * 3 + 0];
		UINT index1 = indices[i * 3 + 1];
		UINT index2 = indices[i * 3 + 2];

		VertexType vertex0 = vertices[index0];
		VertexType vertex1 = vertices[index1];
		VertexType vertex2 = vertices[index2];

		Vector3 p0 = vertex0.position;
		Vector3 p1 = vertex1.position;
		Vector3 p2 = vertex2.position;

		Vector2 uv0 = vertex0.uv;
		Vector2 uv1 = vertex1.uv;
		Vector2 uv2 = vertex2.uv;

		Vector3 e0 = p1 - p0;
		Vector3 e1 = p2 - p0;

		float u0 = uv1.x - uv0.x;
		float u1 = uv2.x - uv0.x;
		float v0 = uv1.y - uv0.y;
		float v1 = uv2.y - uv0.y;

		float d = 1.0f / (u0 * v1 - v0 * u1);

		Vector3 tangent;
		tangent = (v1 * e0 - v0 * e1) * d;

		vertices[index0].tangent += tangent;
		vertices[index1].tangent += tangent;
		vertices[index2].tangent += tangent;
	}

	for (VertexType& vertex : vertices)
	{
		Vector3 t = vertex.tangent;
		Vector3 n = vertex.normal;

		Vector3 temp = (t - n * D3DXVec3Dot(&n, &t));
		D3DXVec3Normalize(&temp, &temp);

		vertex.tangent = temp;
	}
}

void TerrainEditor::Save()
{
	BinaryWriter* writer = new BinaryWriter(L"TextData/Height.map");

	UINT size = (width + 1) * (height + 1);
	heights.clear();
	heights.resize(size);
	writer->UInt(size);
	for (UINT i = 0; i < size; i++)
		heights[i] = vertices[i].position.y;
	writer->Byte(heights.data(), sizeof(float) * size);

	delete writer;
}

void TerrainEditor::Load()
{
	BinaryReader* reader = new BinaryReader(L"TextData/Height.map");

	UINT size = reader->UInt();
	if (size != 0)
	{
		heights.resize(size);
		void* data = heights.data();

		reader->Byte(&data, size * sizeof(float));

		for (UINT i = 0; i < size; i++)
			vertices[i].position.y = heights[i];
	}

	delete reader;

	CreateNormal();
	vertexBuffer->UpdateBuffer(vertices.data(), vertices.size());
}
