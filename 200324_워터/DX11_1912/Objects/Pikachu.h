#pragma once

class Pikachu : public GameAnimModel
{
private:
	enum AnimState
	{
		IDLE,
		RUN,
		ATTACK
	}state;

	float moveSpeed;
	float rotSpeed;
	
	float accelation;
	float deceleration;

	Vector3 velocity;

	Vector3 destPos;
	vector<Vector3> path;

	Terrain* terrain;
	AStar* aStar;
public:
	Pikachu();
	~Pikachu();

	void Update();
	void Render();
	void PostRender();

	void SetAnimation(AnimState value, bool isRepeat);
	void SetIdle();

	void Input();
	void Move();
	void MovePath();	
	void Rotate();

	void SetTerrain(Terrain* terrain) { this->terrain = terrain; }
	void SetAStar(AStar* aStar) { this->aStar = aStar; }
};