#include "Framework.h"

Tank::Tank()
	: GameModel("Tank"), moveSpeed(20), rotSpeed(5), wheelAngle(0)
{
	leftWheelBone = model->BoneByName("l_back_wheel_geo");
	leftWheel = leftWheelBone->local;
}

Tank::~Tank()
{
}

void Tank::Update()
{
	GameModel::Update();

	Matrix R;
	wheelAngle += DELTA;
	D3DXMatrixRotationX(&R, wheelAngle);
	leftWheelBone->local = R * leftWheel;

	if(KEYPRESS(VK_UP))
		position -= forward * moveSpeed * DELTA;
	if (KEYPRESS(VK_DOWN))
		position += forward * moveSpeed * DELTA;

	if (KEYPRESS(VK_RIGHT))
		rotation.y += rotSpeed * DELTA;
	if (KEYPRESS(VK_LEFT))
		rotation.y -= rotSpeed * DELTA;
}

void Tank::Render()
{
	GameModel::Render();
}
