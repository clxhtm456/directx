#include "Framework.h"

Program::Program()
{
	//scenes.push_back(new TextureScene());
	//scenes.push_back(new GridScene());
	//scenes.push_back(new LandscapeScene());
	//scenes.push_back(new ModelScene());
	//scenes.push_back(new CollisionScene());
	//scenes.push_back(new BillboardScene());
	//scenes.push_back(new ParticleScene());
	//scenes.push_back(new RenderTargetScene());
	//scenes.push_back(new OutlineScene());
	//scenes.push_back(new CharacterScene());
	//scenes.push_back(new InstancingScene());
	//scenes.push_back(new FrustumScene());
	//scenes.push_back(new ModelInstancingScene());
	//scenes.push_back(new ComputeScene());
	//scenes.push_back(new ShadowScene());
	scenes.push_back(new LightScene());
	scenes.push_back(new WaterScene());
}

Program::~Program()
{
	for (Scene* scene : scenes)
		delete scene;
}

void Program::Update()
{
	Environment::Get()->GetViewport()->Set();

	for (Scene* scene : scenes)
		scene->Update();

	CAMERA->Update();
	VP->SetView(CAMERA->GetView());
	wheelValue = 0;
}

void Program::PreRender()
{
	VP->SetView(CAMERA->GetView());
	VP->SetProjection(Environment::Get()->GetPerspective()->GetMatrix());
	VP->SetVSBuffer(0);
	LIGHT->SetPSBuffer(0);

	for (Scene* scene : scenes)
		scene->PreRender();
}

void Program::Render()
{
	VP->SetView(CAMERA->GetView());
	VP->SetProjection(Environment::Get()->GetPerspective()->GetMatrix());
	VP->SetVSBuffer(0);
	Environment::Get()->GetViewport()->Set();

	for (Scene* scene : scenes)
		scene->Render();
}

void Program::PostRender()
{
	DirectWrite::Get()->GetDC()->BeginDraw();

	wstring fps = L"FPS : " + to_wstring((int)Timer::Get()->FPS());
	RECT rect = { WIN_WIDTH - 100, 10, WIN_WIDTH, 50 };

	DirectWrite::Get()->RenderText(fps, rect);

	//ImGui::Text("FPS : %d", (int)Timer::Get()->FPS());
	ImGui::Separator();
	ImGui::SliderFloat3("CamPos", CAMERA->GetPos(), -1000, 1000);
	ImGui::SliderFloat2("CamRot", CAMERA->GetRotation(), -1000, 1000);
	CAMERA->PostRender();
	ImGui::Separator();
	ImGui::SliderFloat3("LightDirection", LIGHT->data.direction, -1, 1);
	ImGui::SliderFloat("SpecularExp", &LIGHT->data.specExp, 0, 20.0f);
	ImGui::ColorEdit4("Ambient", LIGHT->data.ambient);
	bool temp = LIGHT->data.isSpecularMap;
	ImGui::Checkbox("IsSpecularMap", &temp);
	LIGHT->data.isSpecularMap = temp;
	temp = LIGHT->data.isNormalMap;
	ImGui::Checkbox("IsNormalMap", &temp);
	LIGHT->data.isNormalMap = temp;

	for (Scene* scene : scenes)
		scene->PostRender();

	DirectWrite::Get()->GetDC()->EndDraw();
}
