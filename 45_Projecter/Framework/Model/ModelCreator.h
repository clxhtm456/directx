#pragma once

struct ModelInstance
{
public:
	ModelAnimator* modelAnimator;
	UINT recentIndex;
};

class ModelItem;
class ModelCreator
{
public:
	static ModelCreator* Create(Shader* shader, wstring dir);

	static void Update();
	static void Render();
public:
	~ModelCreator();
protected:
	bool Init(Shader* shader, wstring dir);
public :
	ModelItem* AddModelItem();
	void AddAnimation(wstring dir);
	void AttachModel(Shader* shader, Model* model, int parentBoneIndex, Transform* transform);


private:
	Shader* shader;
	wstring modelDir;

	ModelInstance* instance;
	UINT recentIndex;

	vector<ModelItem*> itemList;
	static vector<ModelInstance*> modelAnimList;
};

class ModelItem
{
public:
	ModelItem(ModelInstance* instance);
	~ModelItem();

	void PlayClip(UINT clip, float speed = 1.0f, float takeTime = 1.0f);
	void PlayClipOnce(UINT clip,UINT nextClip, float speed = 1.0f, float takeTime = 1.0f, const function<void()>& endCallback = NULL);
	void Pass(UINT index);

	void SetPosition(Vector3 pos);
	void SetScale(Vector3 scale);

	Matrix GetAttachTransform(UINT attachPos) { return animator->GetAttachTransform(index, attachPos); }
	Transform* GetTransform() { return transform; }
	Model* GetModel() { return model; }

private:
	ModelAnimator* animator;
	UINT index;
	Transform* transform;
	Model* model;
	
};