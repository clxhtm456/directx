#include "Framework.h"
#include "ModelCreator.h"

vector<ModelInstance*> ModelCreator::modelAnimList;

ModelCreator* ModelCreator::Create(Shader* shader,wstring dir)
{
	ModelCreator* temp = new ModelCreator();
	if (!temp || !temp->Init(shader,dir))
	{
		delete temp;
		temp = nullptr;
	}
	return temp;
}

void ModelCreator::Update()
{
	for (auto instance : modelAnimList)
	{
		instance->modelAnimator->Update();
		instance->modelAnimator->UpdateTransforms();
	}
}

void ModelCreator::Render()
{
	for (auto instance : modelAnimList)
	{
		instance->modelAnimator->Render();
	}
}

ModelCreator::~ModelCreator()
{
	SafeDelete(instance);
	for (auto item : itemList)
	{
		delete item;
	}
}

bool ModelCreator::Init(Shader* shader,wstring dir)
{
	this->shader = shader;
	modelDir = dir;

	instance = new ModelInstance();
	instance->recentIndex = -1;
	instance->modelAnimator = new ModelAnimator(shader);

#if _USE_CACHE_DIR
	instance->modelAnimator->ReadModel(dir);
#else
	instance->modelAnimator->ReadMaterial(dir);
	instance->modelAnimator->ReadMesh(dir);
#endif

	modelAnimList.push_back(instance);

	return true;
}

ModelItem* ModelCreator::AddModelItem()
{
	instance->recentIndex++;
	ModelItem* item = new ModelItem(instance);

	itemList.push_back(item);
	return item;
}

void ModelCreator::AddAnimation(wstring dir)
{
#if _USE_CACHE_DIR
	instance->modelAnimator->ReadAnimation(dir);
#else
	instance->modelAnimator->ReadClip(dir);
#endif
}

void ModelCreator::AttachModel(Shader* shader, Model* model, int parentBoneIndex, Transform* transform)
{
	instance->modelAnimator->GetModel()->Attach(shader, model, parentBoneIndex, transform);
}


ModelItem::ModelItem(ModelInstance* instance)
{
	animator = instance->modelAnimator;
	index = instance->recentIndex;
	transform = animator->AddTransform();
	model = animator->GetModel();
}

ModelItem::~ModelItem()
{
}

void ModelItem::PlayClip(UINT clip, float speed, float takeTime)
{
	animator->PlayClip(index, clip, speed, takeTime);

	animator->UpdateTransforms();
}

void ModelItem::PlayClipOnce(UINT clip,UINT nextClip, float speed, float takeTime, const function<void()>& endCallback)
{
	animator->PlayClipOnce(index, clip, nextClip, speed, takeTime, endCallback);

	animator->UpdateTransforms();
}

void ModelItem::Pass(UINT index)
{
	animator->Pass(index);
}

void ModelItem::SetPosition(Vector3 pos)
{
	transform->Position(pos);
}

void ModelItem::SetScale(Vector3 scale)
{
	transform->Scale(scale);
}
