#include "stdafx.h"
#include "Convert.h"
#include "Converter.h"


void Convert::Initialize()
{
	//Airplane();
	//Tower();
	//Tank();
	Kachujin();
	//Megan();
	//Weapon();

	//Choonyung();
	//Mutant();
	//Erika();
	//Weapon();
}

void Convert::Airplane()
{
	Converter* conv = new Converter();
	conv->ReadFile(L"B787/Airplane.fbx");
	conv->ExportMaterial(L"B787/Airplane", false);
	conv->ExportMesh(L"B787/Airplane");
	SafeDelete(conv);
}

void Convert::Tower()
{
	Converter* conv = new Converter();
	conv->ReadFile(L"Tower/Tower.fbx");
	conv->ExportMaterial(L"Tower/Tower", false);
	conv->ExportMesh(L"Tower/Tower");
	SafeDelete(conv);
}

void Convert::Tank()
{
	Converter* conv = new Converter();
	conv->ReadFile(L"Tank/Tank.fbx");
	conv->ExportMaterial(L"Tank/Tank", false);
	conv->ExportMesh(L"Tank/Tank");
	SafeDelete(conv);
}

void Convert::Kachujin()
{
	Converter* conv = new Converter();
	conv->ReadFile(L"Kachujin/Mesh.fbx");
	conv->ExportMaterial(L"Kachujin/Mesh", false);
	conv->ExportMesh(L"Kachujin/Mesh");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Kachujin/Idle.fbx");
	conv->ExportAnimClip(0, L"Kachujin/Idle");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Kachujin/Running.fbx");
	conv->ExportAnimClip(0, L"Kachujin/Running");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Kachujin/Jump.fbx");
	conv->ExportAnimClip(0, L"Kachujin/Jump");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Kachujin/Hip_Hop_Dancing.fbx");
	conv->ExportAnimClip(0, L"Kachujin/Hip_Hop_Dancing");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Kachujin/Attack.fbx");
	conv->ExportAnimClip(0, L"Kachujin/Attack");
	SafeDelete(conv);
}

void Convert::Megan()
{
	Converter* conv = new Converter();
	conv->ReadFile(L"Megan/Mesh.fbx");
	conv->ExportMaterial(L"Megan/Mesh", false);
	conv->ExportMesh(L"Megan/Mesh");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Megan/Taunt.fbx");
	conv->ExportAnimClip(0, L"Megan/Taunt");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Megan/Dancing.fbx");
	conv->ExportAnimClip(0, L"Megan/Dancing");
	SafeDelete(conv);
}

void Convert::Weapon()
{
	vector<wstring> names;
	names.push_back(L"Cutter.fbx");
	names.push_back(L"Cutter2.fbx");
	names.push_back(L"Dagger_epic.fbx");
	names.push_back(L"Dagger_small.fbx");
	names.push_back(L"Katana.fbx");
	names.push_back(L"LongArrow.obj");
	names.push_back(L"LongBow.obj");
	names.push_back(L"Rapier.fbx");
	names.push_back(L"Sword.fbx");
	names.push_back(L"Sword_epic.fbx");
	names.push_back(L"Sword2.fbx");

	for (wstring name : names)
	{
		Converter* conv = new Converter();
		conv->ReadFile(L"Weapon/" + name);

		String::Replace(&name, L".fbx", L"");
		String::Replace(&name, L".obj", L"");

		conv->ExportMaterial(L"Weapon/" + name, false);
		conv->ExportMesh(L"Weapon/" + name);
		SafeDelete(conv);
	}
}

void Convert::Mutant()
{
	Converter* conv = new Converter();
	conv->ReadFile(L"Mutant/Mutant.fbx");
	conv->ExportMaterial(L"Mutant/Mesh");
	conv->ExportMesh(L"Mutant/Mesh");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Mutant/Idle.fbx");
	conv->ExportAnimClip(0, L"Mutant/Idle");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Mutant/Run.fbx");
	conv->ExportAnimClip(0, L"Mutant/Running");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Mutant/Walk.fbx");
	conv->ExportAnimClip(0, L"Mutant/Walking");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Mutant/Hit.fbx");
	conv->ExportAnimClip(0, L"Mutant/Hit");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Mutant/Attack.fbx");
	conv->ExportAnimClip(0, L"Mutant/Attack");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Mutant/Dead.fbx");
	conv->ExportAnimClip(0, L"Mutant/Dead");
	SafeDelete(conv);


}

void Convert::Erika()
{
	Converter* conv = new Converter();
	conv->ReadFile(L"Erika/Erika.fbx");
	conv->ExportMaterial(L"Erika/Mesh");
	conv->ExportMesh(L"Erika/Mesh");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Erika/Idle.fbx");
	conv->ExportAnimClip(0, L"Erika/Idle");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Erika/Run.fbx");
	conv->ExportAnimClip(0, L"Erika/Running");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Erika/Walk.fbx");
	conv->ExportAnimClip(0, L"Erika/Walking");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Erika/Hit.fbx");
	conv->ExportAnimClip(0, L"Erika/Hit");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Erika/Attack.fbx");
	conv->ExportAnimClip(0, L"Erika/Attack");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"Erika/Dead.fbx");
	conv->ExportAnimClip(0, L"Erika/Dead");
	SafeDelete(conv);
}


void Convert::Choonyung()
{
	Converter* conv = new Converter();
	conv->ReadFile(L"choonyung/choonyung.fbx");
	conv->ExportMaterial(L"choonyung/Mesh");
	conv->ExportMesh(L"choonyung/Mesh");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"choonyung/Idle.fbx");
	conv->ExportAnimClip(0, L"choonyung/Idle");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"choonyung/Walking.fbx");
	conv->ExportAnimClip(0, L"choonyung/Walking");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"choonyung/Fast Run.fbx");
	conv->ExportAnimClip(0, L"choonyung/Fast Run");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"choonyung/Jumping.fbx");
	conv->ExportAnimClip(0, L"choonyung/Jumping");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"choonyung/Hit.fbx");
	conv->ExportAnimClip(0, L"choonyung/Hit");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"choonyung/Dying.fbx");
	conv->ExportAnimClip(0, L"choonyung/Dying");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"choonyung/Slash.fbx");
	conv->ExportAnimClip(0, L"choonyung/Slash");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"choonyung/Slash2.fbx");
	conv->ExportAnimClip(0, L"choonyung/Slash2");
	SafeDelete(conv);

	conv = new Converter();
	conv->ReadFile(L"choonyung/Slash360.fbx");
	conv->ExportAnimClip(0, L"choonyung/Slash360");
	SafeDelete(conv);
}

