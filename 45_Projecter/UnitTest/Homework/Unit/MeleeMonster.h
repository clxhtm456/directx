#pragma once

#include "Monster.h"

class MeleeMonster : public Monster
{
public:
	MeleeMonster(ModelItem* model);
	~MeleeMonster();

	void Update()override;
	void Render()override;
	void Attack(Unit* target) override;

	void EnterCombat(Unit* target) override;

	void CheckCollisionEvent(Unit* target)override;

	
};