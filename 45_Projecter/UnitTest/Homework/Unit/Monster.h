#pragma once

#include "Unit.h"

#define PATROLTIMER Math::Random(0.0f,3.0f)
#define SPEED 10.0f

class Monster : public Unit
{
public:
	Monster(ModelItem* model);
	virtual ~Monster();

	virtual void Update()override;
	virtual void Render()override;
	void DamageTaken(Unit* target)override;
	void EnterCollider(Unit* target) override;

	void PatrolAlg();
	void SetCombatRange(float val);
	void DrawRecogRange();
	float GetCombatRange() { return combatRange; }
protected:
	virtual void EnterCombat(Unit* target) {};

protected:
	UINT _phase;
	Unit* _target;

	float attackTimer;
private:
	float _patrolRange;
	Vector3 _patrolPos;
	//////////////Timer/////////////
	float _patrolTimer;
	float combatRange;

};