#include "stdafx.h"
#include "RangeMonster.h"

#define ATTACK_TIMER 1.0f
#define BULLET_SPEED 20.0f

RangeMonster::RangeMonster(ModelItem * model, ModelRender* modelRender):
	Monster(model),
	bulletModelRender(modelRender)
{
}

RangeMonster::~RangeMonster()
{
	for (int i = 0; i < bulletList.size(); i++)
	{
		delete bulletList[i];
	}
}

void RangeMonster::Update()
{
	Super::Update();


	if (attackTimer > 0.0f)
		attackTimer -= Time::Delta();

	if (_phase == 2)
	{
		if (_target != nullptr)
		{
			auto pos = _target->GetPosition();
			Vector3 p2 = GetPosition();

			float dx = pos.x - p2.x;
			float dz = pos.z - p2.z;

			auto distance = sqrt(dx * dx + dz * dz);

			if (distance > GetCombatRange())
			{
				float posX = Math::MoveToward(GetPosition().x, pos.x, SPEED * Time::Delta());
				float posZ = Math::MoveToward(GetPosition().z, pos.z, SPEED * Time::Delta());
				MoveToward(Vector3(posX, GetPosition().y, posZ));
				PlayAnimation(4);
			}
			else if (attackTimer <= 0.0f)
			{
				attackTimer = ATTACK_TIMER;
				Attack(_target);
			}
		}
	}

	for (int i = 0; i < bulletList.size(); i++)
	{
		bulletList[i]->Update();
	}
}

void RangeMonster::Render()
{
	Super::Render();

	for (int i = 0; i < bulletList.size(); i++)
	{
		bulletList[i]->Render();
	}
}

void RangeMonster::Attack(Unit * target)
{
	Super::Attack(target);

	_phase = 3;

	Bullet* bullet = new Bullet(bulletModelRender);
	Vector3 startPos = GetPosition();
	startPos.y += 10.0f;
	bullet->SetPosition(startPos);
	bullet->SetScale(Vector3(0.5f, 0.5f, 0.5f));
	bullet->SetTarget(target->GetPosition());
	bullet->SetEndTimerCallback([=](Bullet* bullet)->void
		{
			for (int i = 0; i < bulletList.size(); i++)
			{
				if (bulletList[i] == bullet)
				{
					bulletList.erase(bulletList.begin() + i);
					delete bullet;
					bullet = NULL;
					return;
				}
			}
		});
	bulletList.push_back(bullet);

	
	
	PlayAnimationOnce(2, 0, 1, 1, [=]()->void
	{
		_phase = 2;
	});
}

void RangeMonster::EnterCombat(Unit* target)
{
	if (_phase == 2 || _phase == 3)
		return;
	//원거리공격을 시작합니다
	_phase = 2;
	_target = target;
}

void RangeMonster::CheckCollisionEvent(Unit* target)
{
	for (int i = 0; i < bulletList.size(); i++)
	{
		if (bulletList[i]->GetCollider()->IsIntersect(target->GetCollider()))
		{
			target->DamageTaken(this);
			delete bulletList[i];
			bulletList[i] = NULL;

			bulletList.erase(bulletList.begin() + i);
			return;
		}
	}
}

Bullet::Bullet(ModelRender * modelRender):
	xAccel(0),
	zAccel(0)
{
	bulletModelRender = modelRender;
	transform = bulletModelRender->AddTransform();
	timer = 10.0f;

	cInit = new Transform();
	cTransform = new Transform();
	cInit->Scale(0.5f, 7, 0.5f);
	cInit->Position(0, 0, 0);
	collider = new Collider(cTransform, cInit);
}

Bullet::~Bullet()
{
	delete cInit;
	delete cTransform;
	delete collider;
	bulletModelRender->DelTransform(transform);
}

void Bullet::SetPosition(Vector3 pos)
{
	transform->Position(pos);
	cTransform->Position(pos);
}

void Bullet::SetRotation(Vector3 rot)
{
	transform->Rotation(rot);
	cTransform->Rotation(rot);
}

void Bullet::SetScale(Vector3 scale)
{
	transform->Scale(scale);
}

void Bullet::Update()
{
	collider->Update();

	Vector3 originPos;
	transform->Position(&originPos);

	originPos.x += xAccel*Time::Delta();
	originPos.z += zAccel * Time::Delta();
	SetPosition(originPos);
	transform->Update();

	//collider->GetTransform()->Position(originPos);


	timer -= Time::Delta();
	if (timer < 0)
	{
		if (callback != NULL)
			callback(this);
	}
}

void Bullet::Render()
{
	collider->Render();
}

void Bullet::SetTarget(Vector3 position)
{
	Vector3 originPos;
	transform->Position(&originPos);

	destination = position;
	 destination.y = originPos.y;

	//Calc Accel;
	float dx = destination.x - originPos.x;
	float dz = destination.z-originPos.z;

	Vector3 normal;
	D3DXVec3Normalize(&normal, &Vector3(dx, 0, dz));

	xAccel = normal.x*BULLET_SPEED;
	zAccel = normal.z* BULLET_SPEED;

	//Rotation

	/*float angleY = (Math::PI / 2.0f) - (y * Math::PI) / 10.0f;
	float angleX = (x * 2 * Math::PI) / 10.0f;
	Vector3 rotation = Vector3(sinf(angleX) * cosf(angleY), sinf(angleY), cosf(angleX) * cosf(angleY));*/


	auto quaternion = Math::LookAt(originPos, destination, Vector3(0, 1, 0));
	Vector3 rotation;
	FLOAT fRot;
	D3DXQuaternionToAxisAngle(&quaternion, &rotation, &fRot);
	//transform->Rotation(0, originPos.x > destination.x ? fRot : -fRot,0);
	SetRotation(Vector3(Math::PI / 2, originPos.x > destination.x ? fRot : -fRot,0));
}

void Bullet::SetEndTimerCallback(const function<void(Bullet*)>& func)
{
	callback = func;
}
