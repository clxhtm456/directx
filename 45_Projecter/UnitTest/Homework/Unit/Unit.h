#pragma once

class Unit
{
public:
	Unit(ModelItem* model);
	virtual ~Unit();

	virtual void Attack(Unit* target);
	virtual void JustDied(Unit* target) {}
	virtual void Update();
	virtual void Render();
	virtual void DamageTaken(Unit* target){}

	virtual void CheckCollisionEvent(Unit* target) {}

	void SetPosition(float x, float  y, float z);
	Vector3 GetPosition();
	void SetScale(float x, float  y, float z);
	Vector3 GetScale();

	void PlayAnimation(UINT clip, float speed = 1.0f, float takeTime = 1.0f);
	void PlayAnimationOnce(UINT clip,UINT nextClip, float speed = 1.0f, float takeTime = 1.0f, const function<void()>& endCallback = NULL);
	

	void MoveToward(Vector3 pos);

	void SetFaction(UINT val) { faction = val; }
	UINT GetFaction() { return faction; }
public:
	Collider* GetCollider() { return collider; }
	virtual void EnterCollider(Unit* target) {}
	ModelItem* GetModelItem() { return model; }
protected:
	void UpdateTransform();
protected:
	Transform* transform;
	Transform* init;
	Collider* collider;
	ModelItem* model;
	UINT _animationClip;
protected:
	UINT faction;
	
	vector<Vector3> recogRange;
};