#pragma once

#include "Unit.h"

class Player : public Unit
{
public:
	Player(ModelItem* model);
	~Player();

	void Update()override;
	void Render()override;
	void Attack(Unit* target)override;
	void DamageTaken(Unit* target)override;

	void CheckCollisionEvent(Unit* target) override;

	void CreateWeapon(Transform* weaponModel);
	Collider* GetWeaponCollider() { return weaponCollider; }
	void StopAttackPhase();
private:
	void KeyboardControl();
	Collider* weaponCollider;
	Transform* cInit;
	Transform* cTransform;
	Transform* weaponTransform;
private:
	bool attacking;
private:
	class CameraShaker* camShaker;
};