#include "stdafx.h"
#include "Player.h"
#include "../CameraShaking/CameraShaking.h"

#define PLAYER_SPEED 20.0f

Player::Player(ModelItem* model):
	Unit(model),
	attacking(false)
{

	camShaker = new CameraShaker(Context::Get()->GetCamera());

	init->Scale(5, 15, 5);
	init->Position(0, 8, 0);

	cTransform = new Transform();
	cInit = new Transform();
	cInit->Scale(10, 10, 50);
	cInit->Position(-10, 0, -40);
	weaponCollider = new Collider(cTransform, cInit);
	StopAttackPhase();
}

Player::~Player()
{
	delete weaponCollider;
	delete cTransform;
	delete cInit;
}

void Player::Update()
{
	camShaker->Update();
	KeyboardControl();
	

	Super::Update();

	ImGui::LabelText("animation", "%d", _animationClip);

	if (attacking)
	{
		Matrix attach = model->GetAttachTransform(16);
		weaponCollider->GetTransform()->World(attach);
	}
	weaponCollider->Update();
	

	//Vector3 pos = Vector3(attach._41, attach._42, attach._43);
	/*weaponTransform->World(attach);
	weaponTransform->Scale(0.05f, 0.05f, 0.05f);*/
}

void Player::Render()
{
	Super::Render();
	weaponCollider->Render();
}

void Player::Attack(Unit* target)
{
	Super::Attack(target);
	camShaker->StartShaking(0.2f, 0.2f);
	StopAttackPhase();
}

void Player::DamageTaken(Unit * target)
{
	//PlayAnimationOnce(2, 0);
}

void Player::CheckCollisionEvent(Unit* target)
{
	if (weaponCollider->IsIntersect(target->GetCollider()))
	{
		Attack(target);
	}
}

void Player::CreateWeapon(Transform* weaponModel)
{
	weaponTransform = weaponModel;
	weaponTransform->Position(-10, 0, -10);
	weaponTransform->Scale(0.05f, 0.05f, 0.05f);
	
}

void Player::StopAttackPhase()
{
	attacking = false;
	weaponCollider->GetTransform()->Position(Vector3(-1000, -1000, -1000));
}

void Player::KeyboardControl()
{
	Vector3 tempPos = GetPosition();
	Vector3 originPos = tempPos;

	if (!Keyboard::Get()->Press(VK_DOWN) &&
		!Keyboard::Get()->Press(VK_UP) &&
		!Keyboard::Get()->Press(VK_RIGHT) &&
		!Keyboard::Get()->Press(VK_LEFT))
	{
		if(_animationClip == 0 || _animationClip == 1)
			PlayAnimation(0, 1, 1);
	}

	if (Keyboard::Get()->Down(VK_SPACE))
	{
		attacking = true;
		PlayAnimationOnce(4, 0, 1,1, [=]()->void
		{
			StopAttackPhase();
		});
	}

	if (!attacking && (Keyboard::Get()->Down(VK_DOWN) || Keyboard::Get()->Down(VK_UP) ||
		Keyboard::Get()->Down(VK_RIGHT) || Keyboard::Get()->Down(VK_LEFT)))
	{
		if (_animationClip == 0 || _animationClip == 1)
			PlayAnimation(1, 1.0f, 1.0f);
	}

	if (Keyboard::Get()->Press(VK_DOWN))
	{
		tempPos.z -= PLAYER_SPEED * Time::Delta();
	}
	else if (Keyboard::Get()->Press(VK_UP))
	{
		tempPos.z += PLAYER_SPEED * Time::Delta();
	}

	if (Keyboard::Get()->Press(VK_RIGHT))
	{
		tempPos.x += PLAYER_SPEED * Time::Delta();
	}
	else if (Keyboard::Get()->Press(VK_LEFT))
	{
		tempPos.x -= PLAYER_SPEED * Time::Delta();
	}

	if (originPos != tempPos)
	{
		auto quaternion = Math::LookAt(originPos, tempPos, Vector3(0, 1, 0));
		Vector3 rotation;
		FLOAT fRot;
		D3DXQuaternionToAxisAngle(&quaternion, &rotation, &fRot);
		transform->Rotation(0, originPos.x > tempPos.x ? fRot : -fRot, 0);
	}
	SetPosition(tempPos.x, tempPos.y, tempPos.z);
}
