#include "stdafx.h"
#include "Monster.h"



enum PhaseNum
{
	PHASE_NORMAL,
	PHASE_PATROLSTART,
	PHASE_ENTERCOMBAT,
};

Monster::Monster(ModelItem * model):
	Unit(model),
	_patrolTimer(0.0f),
	_phase(0),
	_patrolRange(500.0f),
	combatRange(1.0f),
	_patrolPos(0,0,0)
{
	init->Scale(5, 15, 5);
	init->Position(0, 8, 0);
	SetCombatRange(1.0f);
}

Monster::~Monster()
{
}

void Monster::Update()
{
	Super::Update();

	switch (_phase)
	{
	case 0:
	{
		if (_patrolTimer < 0.0f)
		{
			_phase = 1;
			PlayAnimation(4);
			auto random = Math::InsideUnitSphere();
			random.y = 0;
			_patrolPos = GetPosition() + Math::Random(-_patrolRange, _patrolRange)* random;
			if (_patrolPos.x > 50)
				_patrolPos.x = 50;
			if (_patrolPos.x < -50)
				_patrolPos.x = -50;
			if (_patrolPos.z > 50)
				_patrolPos.z = 50;
			if (_patrolPos.z < -50)
				_patrolPos.z = -50;
			_patrolTimer = PATROLTIMER;
		}
		else
			_patrolTimer -= Time::Delta();
		break;
	}
	case 1:
	{
		PatrolAlg();
		break;
	}
	case 2:
	{
		break;
	}
	}

	
}

void Monster::Render()
{
	Super::Render();
	DrawRecogRange();
}

void Monster::DamageTaken(Unit * target)
{
	PlayAnimationOnce(1, 0);
}

void Monster::EnterCollider(Unit* target)
{
	if(target->GetFaction() != GetFaction())
		EnterCombat(target);
}

void Monster::PatrolAlg()
{
	Vector3 p1 = GetPosition();
	Vector3 p2 = _patrolPos;

	float dx = p1.x - p2.x;
	float dz = p1.z - p2.z;

	auto distance = sqrt(dx * dx + dz * dz);
	if (distance > 0.1f)
	{
		Vector3 temp = Vector3(
			Math::MoveToward(GetPosition().x, _patrolPos.x, SPEED * Time::Delta()),
			Math::MoveToward(GetPosition().y, _patrolPos.y, SPEED * Time::Delta()),
			Math::MoveToward(GetPosition().z, _patrolPos.z, SPEED * Time::Delta()));
		MoveToward(temp);
	}
	else
	{
		_phase = 0;
		PlayAnimation(0);
	}
}

void Monster::SetCombatRange(float val)
{
	combatRange = val;

	recogRange.clear();

	for (int x = 0; x < 10; x++)
	{
		float angleX = (x * 2 * Math::PI) / 10.0f;
		Vector3 dot = combatRange * Vector3(sinf(angleX), 0, cosf(angleX));
		recogRange.push_back(dot);
	}
}

void Monster::DrawRecogRange()
{
	Vector3 dest[10];

	Transform temp;
	temp.World(transform->World());

	Matrix world = temp.World();

	for (UINT i = 0; i < 10; i++)
		D3DXVec3TransformCoord(&dest[i], &recogRange[i], &world);

	for (int x = 0; x < 10; x++)
	{
		int destX = (x + 1) % (10);
		DebugLine::Get()->RenderLine(dest[x], dest[destX], Color(0, 1, 0, 1));
	}
}
