#include "stdafx.h"
#include "MeleeMonster.h"

#define ATTACK_TIMER 1.0f;

MeleeMonster::MeleeMonster(ModelItem* model):
	Monster(model)
{
}

MeleeMonster::~MeleeMonster()
{
}

void MeleeMonster::Update()
{
	Super::Update();


	if (attackTimer > 0.0f)
		attackTimer -= Time::Delta();
	
	if (_phase == 2)
	{
		if (_target != nullptr)
		{
			auto pos = _target->GetPosition();
			Vector3 p2 = GetPosition();

			float dx = pos.x - p2.x;
			float dz = pos.z - p2.z;

			auto distance = sqrt(dx * dx + dz * dz);

			if (distance > 10.0f)
			{
				float posX = Math::MoveToward(GetPosition().x, pos.x, SPEED * Time::Delta());
				float posZ = Math::MoveToward(GetPosition().z, pos.z, SPEED * Time::Delta());
				MoveToward(Vector3(posX, GetPosition().y, posZ));
				PlayAnimation(4);
			}
			else if(attackTimer <= 0.0f)
			{
				attackTimer = ATTACK_TIMER;
				Attack(_target);
			}
		}
	}
}

void MeleeMonster::Render()
{
	Super::Render();
}

void MeleeMonster::Attack(Unit* target)
{
	Super::Attack(target);
	
	_phase = 3;
	PlayAnimationOnce(2, 0, 1, 1, [=]()->void
		{
			_phase = 2;
		});
}

void MeleeMonster::EnterCombat(Unit* target)
{
	if (_phase == 2 || _phase==3)
		return;
	//밀리공격을 시작합니다
	_phase = 2;
	_target = target;
}

void MeleeMonster::CheckCollisionEvent(Unit* target)
{
}
