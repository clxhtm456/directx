#include "stdafx.h"
#include "Unit.h"

Unit::Unit(ModelItem* model):
	faction(0), _animationClip(0)
{
	model->PlayClip(0);
	model->Pass(2);
	this->model = model;
	transform = new Transform();
	init = new Transform();
	collider = new Collider(transform, init);
	
}

Unit::~Unit()
{
	delete collider;
	delete transform;
	delete init;
}

void Unit::Attack(Unit * target)
{
	target->DamageTaken(this);
}

void Unit::Update()
{
	collider->Update();
}

void Unit::Render()
{
	collider->Render();
	
}

void Unit::UpdateTransform()
{
	Vector3 position;
	transform->Position(&position);
	Vector3 rotation;
	transform->Rotation(&rotation);

	auto modelTransform = model->GetTransform();
	modelTransform->Position(position);
	auto colTransform = collider->GetTransform();
	colTransform->Position(position);

	modelTransform->Rotation(rotation);
	colTransform->Rotation(rotation);
}

void Unit::SetPosition(float x, float y, float z)
{
	if (model == nullptr)
		return;

	transform->Position(x, y, z);
	UpdateTransform();
}

Vector3 Unit::GetPosition()
{
	Vector3 position;
	transform->Position(&position);

	return position;
}

void Unit::SetScale(float x, float y, float z)
{
	if (model == nullptr)
		return;

	Vector3 scale = Vector3(x,y,z);
	auto modelTransform = model->GetTransform();

	modelTransform->Scale(scale);
}

Vector3 Unit::GetScale()
{
	Vector3 scale;
	transform->Scale(&scale);

	return scale;
}

void Unit::PlayAnimation(UINT clip, float speed, float takeTime)
{
	if (_animationClip == clip)
		return;

	_animationClip = clip;
	model->PlayClip(clip, speed, takeTime);
}

void Unit::PlayAnimationOnce(UINT clip,UINT nextClip, float speed, float takeTime, const function<void()>& endCallback)
{
	_animationClip = clip;

	function<void()> callback = NULL;
	if (endCallback != NULL)
	{
		callback = [=]()->void {
			_animationClip = nextClip;
			if (endCallback != NULL)
				endCallback();
		};
	}
	
	model->PlayClipOnce(clip,nextClip, speed, takeTime, callback);
}



void Unit::MoveToward(Vector3 pos)
{
	auto originPos = GetPosition();

	

	if (originPos != pos)
	{
		auto quaternion = Math::LookAt(originPos, pos, Vector3(0, 1, 0));
		Vector3 rotation;
		FLOAT fRot;
		D3DXQuaternionToAxisAngle(&quaternion, &rotation, &fRot);
		transform->Rotation(0, originPos.x > pos.x ? fRot : -fRot, 0);
	}

	SetPosition(pos.x, pos.y, pos.z);
}

