#pragma once

#include "Monster.h"

class Bullet;

class RangeMonster : public Monster
{
public:
	RangeMonster(ModelItem* model,ModelRender* modelRender);
	~RangeMonster();

	void Update()override;
	void Render()override;
	void Attack(Unit* target) override;

	void EnterCombat(Unit* target) override;
	void CheckCollisionEvent(Unit* target) override;

private:
	ModelRender * bulletModelRender;
	vector<Bullet*> bulletList;
};

class Bullet
{
public:
	Bullet(ModelRender* modelRender);
	~Bullet();

	void SetPosition(Vector3 pos);
	void SetRotation(Vector3 rot);
	void SetScale(Vector3 scale);

	void Update();
	void Render();

	void SetTarget(Vector3 position);

	void SetEndTimerCallback(const function<void(Bullet*)>& func);

	Collider* GetCollider() { return collider; }
private:
	Transform* transform;
	Vector3 destination;
	Collider* collider;
	ModelRender* bulletModelRender;
	int index;
private:
	float timer;
	function<void(Bullet*)> callback;
	Transform* cTransform;
	Transform* cInit;
	float xAccel;
	float zAccel;
};