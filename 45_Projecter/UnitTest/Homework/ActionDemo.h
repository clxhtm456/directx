#pragma once

#include "Systems/IExecute.h"

class ActionDemo : public IExecute
{
public:
	virtual void Initialize() override;
	virtual void Ready() override {};
	virtual void Destroy() override;
	virtual void Update() override;
	virtual void PreRender() override {};
	virtual void Render() override;
	virtual void PostRender() override {};
	virtual void ResizeScreen() override {};
private:
	void Mesh();
	//void AirPlane();
	void CreateModelItem();
	void CreatePlayer();
	void CreateMonster();

private:
	void CheckCollision();
	void CheckAttack();
	vector<Collider*> colliderList;
	void AddCollider(Collider* target);

private:
	Shader * shader;

	map<UINT, ModelAnimator*> modelManager;

	struct ColliderDesc
	{
		Transform* Init;
		Transform* Transform;
		Collider* Collider;
	} collider[4];

private:
	

private:

	class SkyCube* sky;
	Material* floor;
	Material* stone;
	Material* brick;
	Material* wall;

	

	MeshRender* sphere;
	MeshRender* cylinder;
	MeshRender* grid;

	Model* weapon = nullptr;

	class Unit* player;
	class Unit* monster[4] = {NULL};

	//Unit* Monster[4];*/

	ModelCreator* kachujinModel;
	ModelCreator* mutantModel;
	ModelCreator* erikaModel;
	ModelRender* arrowModel;

	vector<MeshRender*> meshes;
	vector<ModelRender*> models;
	vector<ModelAnimator*> animators;
};
