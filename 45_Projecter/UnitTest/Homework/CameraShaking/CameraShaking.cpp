#include "stdafx.h"
#include "CameraShaking.h"

CameraShaker::CameraShaker(Camera* cam)
{
	camera = cam;
	camera->Position(&_initialPosition);
}

CameraShaker::~CameraShaker()
{
}

void CameraShaker::StartShaking(float shakeAmount, float time)
{
	camera->Position(&_initialPosition);
	_shakeAmount = shakeAmount;
	_shakeTime = time;
	_startShaking = true;
}

void CameraShaker::Update()
{
	if (!_startShaking)
		return;

	if (_shakeTime > 0)
	{
		Vector3 P;
		camera->Position(&P);

		P = Math::InsideUnitSphere() * _shakeAmount + _initialPosition;
		camera->Position(P);
		_shakeTime -= Time::Delta();
	}
	else
	{
		_shakeTime = 0.0f;
		camera->Position(_initialPosition);
		_startShaking = false;
	}
}

