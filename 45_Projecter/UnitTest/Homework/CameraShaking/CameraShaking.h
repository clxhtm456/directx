#pragma once


class CameraShaker
{
public:
	CameraShaker(Camera* cam);
	~CameraShaker();

	void StartShaking(float shakeAmount, float time);

	void Update();

private:

	Camera* camera;
	Vector3 _initialPosition;
	float _shakeAmount;
	bool _startShaking;
	float _shakeTime;
};