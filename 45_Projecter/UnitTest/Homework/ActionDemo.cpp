#include "stdafx.h"
#include "ActionDemo.h"
#include "Viewer/Freedom.h"
#include "Environment/Terrain.h"
#include "Environment/SkyCube.h"
#include "Unit/Player.h"
#include "Unit/Monster.h"
#include "Unit/MeleeMonster.h"
#include "Unit/RangeMonster.h"

void ActionDemo::Initialize()
{
	Context::Get()->GetCamera()->Position(0, 50, -100);
	Context::Get()->GetCamera()->RotationDegree(34, 0, 0);
	dynamic_cast<Freedom*>(Context::Get()->GetCamera())->Speed(50, 5);


	shader = new Shader(L"38_Lighting.fx");
	sky = new SkyCube(L"Environment/GrassCube1024.dds");

	Mesh();

	CreateModelItem();
	CreatePlayer();
	CreateMonster();
}

void ActionDemo::Destroy()
{
	SafeDelete(shader);
	SafeDelete(sky);

	delete kachujinModel;
	delete erikaModel;
	delete mutantModel;
	delete arrowModel;


	delete player;

}

void ActionDemo::Update()
{
	grid->Update();
	arrowModel->Update();
	arrowModel->UpdateTransforms();
	
	player->Update();
	for (int i = 0; i < 4; i++)
	{
		if (monster[i] == NULL)
			continue;
		
		monster[i]->Update();
	}
	CheckCollision();
	CheckAttack();

	ModelCreator::Update();
	sky->Update();
}

void ActionDemo::Render()
{
	sky->Render();


	floor->Render();
	grid->Render();
	//weapon->Render();
	arrowModel->Render();

	player->Render();

	for (int i = 0; i < 4; i++)
	{
		if (monster[i] == NULL)
			continue;
		
		monster[i]->Render();
	}

	ModelCreator::Render();
}

void ActionDemo::Mesh()
{
	//Create Material
	{
		floor = new Material(shader);
		floor->DiffuseMap("Floor.png");
		floor->SpecularMap("Floor_Specular.png");
		floor->NormalMap("Floor_Normal.png");
		floor->Specular(1, 1, 1, 20);
	}

	//Create Mesh
	{
		Transform* transform = NULL;

		grid = new MeshRender(shader, new MeshGrid(5, 5));
		transform = grid->AddTransform();
		transform->Position(0, 0, 0);
		transform->Scale(12, 1, 12);
		
	}
	grid->UpdateTransforms();

	meshes.push_back(grid);
}


void ActionDemo::CreateModelItem()
{
	kachujinModel = ModelCreator::Create(shader, L"Kachujin/Mesh");
	kachujinModel->AddAnimation(L"Kachujin/Idle");
	kachujinModel->AddAnimation(L"Kachujin/Running");
	kachujinModel->AddAnimation(L"Kachujin/Jump");
	kachujinModel->AddAnimation(L"Kachujin/Hip_Hop_Dancing");
	kachujinModel->AddAnimation(L"Kachujin/Attack");

	weapon = new Model();
	weapon->ReadMaterial(L"Weapon/Sword");
	weapon->ReadMesh(L"Weapon/Sword");

	Transform attachTransform;

	attachTransform.Position(-10, 0, -10);
	attachTransform.Scale(0.5f, 0.5f, 0.5f);
	kachujinModel->AttachModel(shader, weapon, 16, &attachTransform);

	delete weapon;

	mutantModel = ModelCreator::Create(shader, L"Mutant/Mesh");
	mutantModel->AddAnimation(L"Mutant/Idle");
	mutantModel->AddAnimation(L"Mutant/Hit");
	mutantModel->AddAnimation(L"Mutant/Attack");
	mutantModel->AddAnimation(L"Mutant/Dead");
	mutantModel->AddAnimation(L"Mutant/Running");
	mutantModel->AddAnimation(L"Mutant/Walking");

	erikaModel = ModelCreator::Create(shader, L"Erika/Mesh");
	erikaModel->AddAnimation(L"Erika/Idle");
	erikaModel->AddAnimation(L"Erika/Hit");
	erikaModel->AddAnimation(L"Erika/Attack");
	erikaModel->AddAnimation(L"Erika/Dead");
	erikaModel->AddAnimation(L"Erika/Running");
	erikaModel->AddAnimation(L"Erika/Walking");

	arrowModel = new ModelRender(shader);
	arrowModel->ReadMaterial(L"Weapon/LongArrow");
	arrowModel->ReadMesh(L"Weapon/LongArrow");

}

void ActionDemo::CreatePlayer()
{
	player = new Player(kachujinModel->AddModelItem());
	player->SetPosition(-25, 0, -30);
	player->SetScale(0.075f, 0.075f, 0.075f);
	player->SetFaction(1);
}

void ActionDemo::CreateMonster()
{
	monster[0] = new RangeMonster(erikaModel->AddModelItem(),arrowModel);
	monster[0]->SetPosition(50, 0, 30);
	monster[0]->SetScale(0.075f, 0.075f, 0.075f);
	dynamic_cast<Monster*>(monster[0])->SetCombatRange(20.0f);

	monster[1] = new MeleeMonster(mutantModel->AddModelItem());
	monster[1]->SetPosition(70, 0, 60);
	monster[1]->SetScale(0.075f, 0.075f, 0.075f);
	dynamic_cast<Monster*>(monster[1])->SetCombatRange(20.0f);
}


void ActionDemo::CheckCollision()
{
	for (int i = 0; i < 4; i++)
	{
		if (monster[i] == NULL)
			continue;
		Vector3 p1 = player->GetPosition();
		Vector3 p2 = monster[i]->GetPosition();
		auto dMonster = dynamic_cast<Monster*>(monster[i]);

		float dx = p1.x - p2.x;
		float dz = p1.z - p2.z;

		auto distance = sqrt(dx * dx + dz * dz);

		if (dMonster->GetCombatRange() > distance)
			monster[i]->EnterCollider(player);
	}
}

void ActionDemo::CheckAttack()
{
	Collider* weapon = dynamic_cast<Player*>(player)->GetWeaponCollider();

	for (int i = 0; i < 4; i++)
	{
		if (monster[i] == NULL)
			continue;
		player->CheckCollisionEvent(monster[i]);
		monster[i]->CheckCollisionEvent(player);
	}
}

void ActionDemo::AddCollider(Collider * target)
{
	colliderList.push_back(target);
}

