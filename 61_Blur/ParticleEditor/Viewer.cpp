#include "stdafx.h"
#include "Viewer.h"
#include "Environment/Sky/Sky.h"

void Viewer::Initialize()
{
	Context::Get()->GetCamera()->RotationDegree(20, 0, 0);
	Context::Get()->GetCamera()->Position(1, 25, -50);

	shader = new Shader(L"57_ParticleViewer.fxo");
	shadow = new Shadow(shader, Vector3(0, 0, 0), 65);

	sky = new Sky(shader);
	sky->ScatteringPass(3);
	sky->RealTime(false, Math::PI - 1e-6f, 0.5f);

	particleSystem = new ParticleSystem(L"Explosion");
	particleSystem->Pass(0);

	Mesh();
}

void Viewer::Destroy()
{
	delete shader;
	delete shadow;

	delete sky;

	delete floor;
	delete stone;

	delete sphere;
	delete grid;

	delete particleSystem;
}

void Viewer::Update()
{
	sky->Update();
	grid->Update();
	sphere->Update();

	Vector3 p;
	sphere->GetTransform(0)->Position(&p);
	float moveSpeed = 20.0f;

	if (Mouse::Get()->Press(1) == false)
	{
		const Vector3& F =Context::Get()->GetCamera()->Foward();
		const Vector3& R = Context::Get()->GetCamera()->Right();
		const Vector3& U = Context::Get()->GetCamera()->Up();

		Vector3 tempForward = F;
		tempForward.y = 0;
		D3DXVec3Normalize(&tempForward, &tempForward);
		if (Keyboard::Get()->Press('W'))
			p += tempForward * moveSpeed*Time::Delta();
		else if (Keyboard::Get()->Press('S'))
			p -= tempForward * moveSpeed*Time::Delta();

		if (Keyboard::Get()->Press('A'))
			p -= R * moveSpeed*Time::Delta();
		else if (Keyboard::Get()->Press('D'))
			p += R * moveSpeed*Time::Delta();

	}

	sphere->GetTransform(0)->Position(p);
	sphere->UpdateTransforms();

	particleSystem->Add(p);
	particleSystem->Update();
}

void Viewer::PreRender()
{
	sky->PreRender();

	//Depth
	{
		shadow->Set();
		Pass(0);
		sphere->Render();

	}
}

void Viewer::Render()
{
	sky->Pass(4, 5, 6);
	sky->Render();

	Pass(7);
	stone->Render();
	sphere->Render();

	floor->Render();
	grid->Render();

	particleSystem->Render();

}

void Viewer::Mesh()
{
	//CreateMaterial
	floor = new Material(shader);
	floor->DiffuseMap("Floor.png");
	floor->SpecularMap("Floor_Specular.png");
	floor->NormalMap("Floor_Normal.png");
	floor->Specular(1, 1, 1, 20);
	floor->Emissive(0.3f, 0.3f, 0.3f, 0.3f);

	stone = new Material(shader);
	stone->DiffuseMap("Bricks.png");
	stone->SpecularMap("Bricks_Specular.png");
	stone->NormalMap("Bricks_Normal.png");
	stone->Specular(1, 0, 0, 20);
	stone->Emissive(0.3f, 0.3f, 0.3f, 0.3f);

	//CreateMesh
	Transform* transform = NULL;

	grid = new MeshRender(shader, new MeshGrid(10, 10));
	transform = grid->AddTransform();
	transform->Position(0, 0, 0);
	transform->Scale(20, 1, 20);

	sphere = new MeshRender(shader, new MeshSphere(0.5f, 20, 20));
	transform = sphere->AddTransform();
	transform->Position(0, 5, 0);
	transform->Scale(5, 5,5);

	sphere->UpdateTransforms();
	grid->UpdateTransforms();

	meshes.push_back(sphere);
	meshes.push_back(grid);
}

void Viewer::Pass(UINT meshPass)
{
	for (MeshRender* temp : meshes)
		temp->Pass(meshPass);
}


