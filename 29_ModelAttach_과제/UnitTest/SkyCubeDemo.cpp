#include "stdafx.h"
#include "SkyCubeDemo.h"
#include "Viewer/Freedom.h"
#include "Environment/Terrain.h"
#include "Environment/SkyCube.h"

void SkyCubeDemo::Initialize()
{
	Context::Get()->GetCamera()->Position(0, 32, -67);
	Context::Get()->GetCamera()->RotationDegree(23, 0, 0);
	dynamic_cast<Freedom*>(Context::Get()->GetCamera())->Speed(50, 5);

	shader = new Shader(L"13_Mesh.fx");

	cube = new MeshCube(shader);
	cube->GetTransform()->Position(0, 5, 0);
	cube->GetTransform()->Scale(20, 10, 20);
	cube->DiffuseMap(L"Stones.png");

	grid = new MeshGrid(shader, 5, 5);
	grid->GetTransform()->Scale(20, 1, 20);
	grid->DiffuseMap(L"Floor.png");	

	for (UINT i = 0; i < 5; i++)
	{
		//Cylinder
		cylider[i * 2] = new MeshCylinder(shader, 0.5f, 3.0f, 20, 20);
		cylider[i * 2]->GetTransform()->Position(-30, 6, -15.0f + (float)i * 15.0f);
		cylider[i * 2]->GetTransform()->Scale(5, 5, 5);
		cylider[i * 2]->DiffuseMap(L"Bricks.png");

		cylider[i * 2 + 1] = new MeshCylinder(shader, 0.5f, 3.0f, 20, 20);
		cylider[i * 2 + 1]->GetTransform()->Position(30, 6, -15.0f + (float)i * 15.0f);
		cylider[i * 2 + 1]->GetTransform()->Scale(5, 5, 5);
		cylider[i * 2 + 1]->DiffuseMap(L"Bricks.png");

		//Sphere
		sphere[i * 2] = new MeshSphere(shader, 0.5f, 20, 20);
		sphere[i * 2]->GetTransform()->Position(-30, 15.5, -15.0f + (float)i * 15.0f);
		sphere[i * 2]->GetTransform()->Scale(5, 5, 5);
		sphere[i * 2]->DiffuseMap(L"Wall.png");

		sphere[i * 2 + 1] = new MeshSphere(shader, 0.5f, 20, 20);
		sphere[i * 2 + 1]->GetTransform()->Position(30, 15.5, -15.0f + (float)i * 15.0f);
		sphere[i * 2 + 1]->GetTransform()->Scale(5, 5, 5);
		sphere[i * 2]->DiffuseMap(L"Wall.png");
	}

	sky = new SkyCube(L"Environment/GrassCube1024.dds");

	terrainShader = new Shader(L"12_Terrain.fx");
	terrain = new Terrain(terrainShader, L"Terrain/Gray256.png");
	terrain->BaseMap(L"Terrain/Dirt3.png");
	
}

void SkyCubeDemo::Destroy()
{
	SafeDelete(shader);
	
	SafeDelete(grid);
	SafeDelete(cube);

	for (MeshCylinder* i : cylider)
		SafeDelete(i);
	for (MeshSphere* i : sphere)
		SafeDelete(i);

	SafeDelete(sky);
	SafeDelete(terrainShader);
	SafeDelete(terrain);
	
}

void SkyCubeDemo::Update()
{
	terrain->Update();
	sky->Update();
}

void SkyCubeDemo::Render()
{
	sky->Render();
	

	for (MeshCylinder* i : cylider)
		i->Render();
	for (MeshSphere* i : sphere)
		i->Render();


	grid->Render();
	cube->Render();

	terrain->Render();
}


