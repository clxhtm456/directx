#pragma once

#include "Systems/IExecute.h"

class SkyCubeDemo : public IExecute
{
public:
	virtual void Initialize() override;
	virtual void Ready() override {};
	virtual void Destroy() override;
	virtual void Update() override;
	virtual void PreRender() override {};
	virtual void Render() override;
	virtual void PostRender() override {};
	virtual void ResizeScreen() override {};

private:
	Shader * shader;
	
	MeshGrid* grid;

	MeshCube* cube;
	MeshCylinder* cylider[10];
	MeshSphere* sphere[10];

	Shader* terrainShader;
	class SkyCube* sky;
	class Terrain* terrain;
	
};
