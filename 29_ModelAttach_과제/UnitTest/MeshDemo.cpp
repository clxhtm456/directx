#include "stdafx.h"
#include "MeshDemo.h"
#include "Viewer/Freedom.h"


void MeshDemo::Initialize()
{
	Context::Get()->GetCamera()->Position(0, 32, -67);
	Context::Get()->GetCamera()->RotationDegree(23, 0, 0);
	dynamic_cast<Freedom*>(Context::Get()->GetCamera())->Speed(50, 5);

	shader = new Shader(L"10_Mesh.fx");

	cube = new MeshCube(shader);
	cube->GetTransform()->Position(0, 5, 0);
	cube->GetTransform()->Scale(20, 10, 20);

	grid = new MeshGrid(shader);
	grid->GetTransform()->Scale(20, 1, 20);	

	for (UINT i = 0; i < 5; i++)
	{
		//Cylinder
		cylider[i * 2] = new MeshCylinder(shader, 0.5f, 3.0f, 20, 20);
		cylider[i * 2]->GetTransform()->Position(-30, 6, -15.0f + (float)i * 15.0f);
		cylider[i * 2]->GetTransform()->Scale(5, 5, 5);

		cylider[i * 2 + 1] = new MeshCylinder(shader, 0.5f, 3.0f, 20, 20);
		cylider[i * 2 + 1]->GetTransform()->Position(30, 6, -15.0f + (float)i * 15.0f);
		cylider[i * 2 + 1]->GetTransform()->Scale(5, 5, 5);

		//Sphere
		sphere[i * 2] = new MeshSphere(shader, 0.5f, 20, 20);
		sphere[i * 2]->GetTransform()->Position(-30, 15.5, -15.0f + (float)i * 15.0f);
		sphere[i * 2]->GetTransform()->Scale(5, 5, 5);

		sphere[i * 2 + 1] = new MeshSphere(shader, 0.5f, 20, 20);
		sphere[i * 2 + 1]->GetTransform()->Position(30, 15.5, -15.0f + (float)i * 15.0f);
		sphere[i * 2 + 1]->GetTransform()->Scale(5, 5, 5);
	

	}
	
}

void MeshDemo::Destroy()
{
	SafeDelete(shader);
	
	SafeDelete(grid);
	SafeDelete(cube);

	for (MeshCylinder* i : cylider)
		SafeDelete(i);

	for (MeshSphere* i : sphere)
		SafeDelete(i);
	
}

void MeshDemo::Update()
{
	
}

void MeshDemo::Render()
{
	static int pass = 0;
	ImGui::SliderInt("Pass", &pass, 0, 1);

	grid->Pass(pass);
	grid->Render();

	cube->Pass(pass);
	cube->Render();

	for (MeshCylinder* i : cylider)
	{
		i->Pass(pass);
		i->Render();
	}

	for (MeshSphere* i : sphere)
	{
		i->Pass(pass);
		i->Render();
	}
}


