//cbuffer
//-----------------------------------------------
cbuffer CB_PerFrame
{
    matrix View;
    matrix ViewInverse;
    matrix Projection;
    matrix VP;
    float Time;
   
};

cbuffer CB_World
{
    matrix World;
};

Texture2D DiffuseMap;
Texture2D SpecularMap;
Texture2D NormalMap;

TextureCube SkyCubeMap;

//Function
//-----------------------------------------------
float4 WorldPostion(float4 position)
{
    return mul(position, World);
}

float4 ViewProjetion(float4 position)
{
    position = mul(position, View);
    return mul(position, Projection);
}

float3 WorldNormal(float3 normal)
{
    return mul(normal, (float3x3) World);
}

//Mesh
//-----------------------------------------------
struct MeshOutput
{
    float4 Position : SV_Position0;
    float4 wvpPosition : Position1;
    float3 oPosition : Position2;
    float3 wPosition : Position3;

    float2 Uv : Uv0;
    float3 Normal : Normal0;
};




//Input
//-----------------------------------------------
struct Vertex
{
    float4 Position : POSITION0;
};

struct VertexColor
{
    float4 Postion : POSITION0;
    float4 Color : COLOR0;
};

struct VertexNormal
{
    float4 Position : POSITION0;
    float3 Normal : NORMAL0;
};

struct VertexColorNormal
{
    float4 Position : POSITION0;
    float4 Color : COLOR0;
    float3 Normal : NORMAL0;
};

struct VertexTextrueNormal
{
    float4 Position : Position0;
    float2 Uv : Uv;
    float3 Normal : Normal;
};

//State
//-----------------------------------------------
SamplerState LinearSampler
{
    Filter = MIN_MAG_MIP_LINEAR;

    AddressU = Wrap;
    AddressV = Wrap;
};

RasterizerState FillMode_WireFrame
{
    FillMode = WireFrame;
};

RasterizerState FrontCounterCloskwise_True
{
    FrontCounterClockwise = true;
};

DepthStencilState DepthEnable_False
{
    DepthEnable = false;
};

//VS GENERATE
//-----------------------------------------------
#define VS_GENERATE \
output.oPosition = input.Position.xyz; \
output.Position = WorldPostion(input.Position); \
output.wPosition = output.Position.xyz; \
\
output.Position = ViewProjetion(output.Position); \
output.wvpPosition = output.Position; \
output.Normal = WorldNormal(input.Normal); \
output.Uv = input.Uv;


//Pass Macro
//-----------------------------------------------
#define P_VP(name, vs, ps) \
pass name \
{ \
    SetVertexShader(CompileShader(vs_5_0, vs())); \
    SetPixelShader(CompileShader(ps_5_0, ps())); \
}

#define P_RS_VP(name, rs, vs, ps) \
pass name \
{ \
    SetRasterizerState(rs); \
    SetVertexShader(CompileShader(vs_5_0, vs())); \
    SetPixelShader(CompileShader(ps_5_0, ps())); \
}

#define P_RS_DSS_VP(name, rs, dss, vs, ps) \
pass name \
{ \
    SetRasterizerState(rs); \
    SetDepthStencilState(dss, 0); \
    SetVertexShader(CompileShader(vs_5_0, vs())); \
    SetPixelShader(CompileShader(ps_5_0, ps())); \
}