#include "stdafx.h"
#include "DrawModel.h"
#include "Viewer/Freedom.h"
#include "ModelSelector/ModelSelector.h"

void DrawModel::Initialize()
{
	Context::Get()->GetCamera()->RotationDegree(0, 0, 0);
	Context::Get()->GetCamera()->Position(0, 0, -50);
	dynamic_cast<Freedom*>(Context::Get()->GetCamera())->Speed(20, 2);

	shader = new Shader(L"25_Model.fx");

	modelSelector = new ModelSelector();

	AirPlane();
	Tower();
	Tank();

}

void DrawModel::Destroy()
{
	delete airPlane;
	delete tower;
	delete tank;

	delete modelSelector;

	delete shader;
}

void DrawModel::Update()
{
	airPlane->Update();
	tower->Update();
	/*{
		static UINT boneIndex = 0;
		static UINT max = tank->GetModel()->BoneCount() - 1;
		ImGui::SliderInt("Bone", (int*)&boneIndex, 0, max);
		ModelBone* bone = tank->GetModel()->BoneByIndex(10);

		Transform transform;
		transform.RotationDegree(0, cosf(Time::Get()->Running())*45*Time::Delta(), 0);

		tank->UpdateTransform(bone, transform.World());
	}*/
	tank->Update();

	modelSelector->Update();
}

void DrawModel::Render()
{
	airPlane->Render();
	tower->Render();
	tank->Render();
}

void DrawModel::AirPlane()
{
	airPlane = new ModelRender(shader);
	airPlane->ReadMaterial(L"B787/AirPlane");
	airPlane->ReadMesh(L"B787/AirPlane");

	airPlane->GetTransform()->Scale(0.005f, 0.005f, 0.005f);
	airPlane->Pass(1);

	modelSelector->AddModelList(airPlane);
	
}

void DrawModel::Tower()
{
	tower = new ModelRender(shader);
	tower->ReadMaterial(L"Tower/Tower");
	tower->ReadMesh(L"Tower/Tower");
	tower->GetTransform()->Position(-20, 0, 0);
	tower->GetTransform()->Scale(0.01f, 0.01f, 0.01f);
	tower->Pass(1);

	modelSelector->AddModelList(tower);
}

void DrawModel::Tank()
{
	tank = new ModelRender(shader);
	tank->ReadMaterial(L"Tank/Tank");
	tank->ReadMesh(L"Tank/Tank");
	tank->GetTransform()->Position(20, 0, 0);
	tank->Pass(1);

	modelSelector->AddModelList(tank);
}