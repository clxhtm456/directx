#include "stdafx.h"
#include "DrawAnimation.h"
#include "Viewer/Freedom.h"
#include "ModelSelector/ModelSelector.h"

void DrawAnimation::Initialize()
{
	Context::Get()->GetCamera()->RotationDegree(0, 0, 0);
	Context::Get()->GetCamera()->Position(0, 0, -50);
	dynamic_cast<Freedom*>(Context::Get()->GetCamera())->Speed(20, 2);

	shader = new Shader(L"26_Animation.fx");
	//shader = new Shader(L"25_Model.fx");

	weapon = new Model();
	weapon->ReadMaterial(L"Weapon/Sword");
	weapon->ReadMesh(L"Weapon/Sword");

	Kachujin();
	//Kaya();
	kachujin->Pass(2);
	

	//kachujin->GetModel()->Detach(weapon, 35);
}

void DrawAnimation::Destroy()
{
	delete kachujin;
	delete kaya;

	delete shader;
}

void DrawAnimation::Update()
{
	static UINT clip = 0;
	if (kachujin != NULL)
	{
		if (Keyboard::Get()->Down(VK_SPACE))
		{
			++clip;
			clip %= 4;
			kachujin->PlayClip(clip, 1.0f, 0.7f);
		}
		kachujin->Update();
	}
	/*Vector3 pos;
	kachujin->GetTransform()->Position(&pos);
	pos.x += Time::Delta() * 2.0f;
	kachujin->GetTransform()->Position(pos);*/
	//kaya->Update();
}

void DrawAnimation::Render()
{
	kachujin->Render();

	if (ImGui::Button("Test"))
	{
		
		Transform attachTransform;
		attachTransform.Position(-10, 0, -10);
		attachTransform.Scale(0.5f, 0.5f, 0.5f);
		kachujin->GetModel()->Attach(shader, weapon, 35, &attachTransform);
		kachujin->CreateTexture();
		kachujin->Pass(2);
	}
	//kaya->Render();
}

void DrawAnimation::Kachujin()
{
	kachujin = new ModelAnimator(shader);
	kachujin->ReadMaterial(L"Kachujin/Mesh");
	kachujin->ReadMesh(L"Kachujin/Mesh");

	kachujin->ReadClip(L"Kachujin/Idle");
	kachujin->ReadClip(L"Kachujin/Running");
	kachujin->ReadClip(L"Kachujin/Jump");
	kachujin->ReadClip(L"Kachujin/Hip_Hop_Dancing");
	

	kachujin->GetTransform()->Position(3, 0, -20);
	kachujin->GetTransform()->Scale(0.025f, 0.025f, 0.025f);

	

}

void DrawAnimation::Kaya()
{
	kaya = new ModelAnimator(shader);
	kaya->ReadMaterial(L"kaya/Mesh");
	kaya->ReadMesh(L"kaya/Mesh");
	kaya->ReadClip(L"kaya/Capoeira");
	kaya->Pass(2);

	kaya->GetTransform()->Position(-3, 0, -20);
	kaya->GetTransform()->Scale(0.025f, 0.025f, 0.025f);
}
