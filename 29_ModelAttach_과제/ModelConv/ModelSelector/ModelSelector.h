#pragma once

class ModelSelector
{
public:
	ModelSelector();
	~ModelSelector();
public:
	void Update();
public:
	void AddModelList(ModelRender* model);
private:
	int selectedNum;
	vector<ModelRender*> modelList;
};