#include "stdafx.h"
#include "ModelSelector.h"
#include "Model/Model.h"

ModelSelector::ModelSelector()
{
	selectedNum = -1;
	modelList.clear();
}

ModelSelector::~ModelSelector()
{
	modelList.clear();
}

void ModelSelector::Update()
{
	ImGui::Separator();
	ImGui::LabelText("ModelSelector", "ModelSelector");

	for (int i = 0; i < modelList.size(); i++)
	{
		string label = String::Format("Model%d",i+1);
		if (ImGui::Button(label.c_str()) == true)
		{
			selectedNum = i;
		}
	}

	if (selectedNum != -1)
	{
		ImGui::Separator();
		ImGui::Text("Transform");
		{
			Vector3 position;
			modelList[selectedNum]->GetTransform()->Position(&position);
			float pos[3] = { position.x, position.y, position.z };
			ImGui::InputFloat3("Position", pos);
		}

		{
			Vector3 scale;
			modelList[selectedNum]->GetTransform()->Scale(&scale);
			float scal[3] = { scale.x, scale.y, scale.z };
			ImGui::InputFloat3("Scale", scal);
		}

		{
			Vector3 rotation;
			modelList[selectedNum]->GetTransform()->RotationDegree(&rotation);
			float rot[3] = { rotation.x, rotation.y, rotation.z };
			ImGui::InputFloat3("Rotation", rot);
		}
	}
	
}

void ModelSelector::AddModelList(ModelRender* model)
{
	modelList.push_back(model);
}
