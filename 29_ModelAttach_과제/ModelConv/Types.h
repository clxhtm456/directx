#pragma once

#include "stdafx.h"

struct asMaterial
{
	string Name;

	Color Ambient;
	Color Diffuse;
	Color Specular;

	string DiffuseFile;
	string SpecularFile;
	string NormalFile;
};

struct asBone
{
	int Index;
	string Name;

	int Parent;
	Matrix Transform;
};

struct asMesh
{
	string Name;
	int BoneIndex;

	aiMesh* Mesh;

	string MaterialName;
	vector<Model::ModelVertex> Verticies;
	vector<UINT> Indices;
};

struct asBlendWeight
{
	Vector4 Indices = Vector4(0, 0, 0, 0);
	Vector4 Weight = Vector4(0, 0, 0, 0);

	void Set(UINT index, UINT boneIndex, float weight)
	{
		float i = (float)boneIndex;
		float w = weight;

		switch (index)
		{
		case 0:Indices.x = i;Weight.x = w;break;
		case 1:Indices.y = i;Weight.y = w;break;
		case 2:Indices.z = i;Weight.z = w;break;
		case 3:Indices.w = i;Weight.w = w;break;
		}
	}
};

struct asBoneWeight
{
private:
	typedef pair<UINT, float> Pair;
	vector<Pair> BoneWeights;
public:
	void AddWeight(UINT boneIndex, float boneWeights)
	{
		if (boneWeights <= 0.0f) 
			return;

		bool bAdd = false;

		auto iter = BoneWeights.begin();
		while (iter != BoneWeights.end())
		{
			if (boneWeights > iter->second)
			{
				BoneWeights.insert(iter, Pair(boneIndex, boneWeights));
				bAdd = true;

				break;
			}
			iter++;
		}
		if (bAdd == false)
			BoneWeights.push_back(Pair(boneIndex, boneWeights));
	}

	void GetBlendWeight(asBlendWeight& blendWeight)
	{
		for (UINT i = 0; i < BoneWeights.size(); i++)
		{
			if (i >= 4)
				return;
			
			blendWeight.Set(i, BoneWeights[i].first, BoneWeights[i].second);
		}
		
	}

	void Normalize()
	{
		float totalWeight = 0.0f;

		int i = 0;
		auto iter = BoneWeights.begin();
		while (iter != BoneWeights.end())
		{
			if (i < 4)
			{
				totalWeight += iter->second;
				i++;
				iter++;
			}
			else
				iter = BoneWeights.erase(iter);
		}
		float scale = 1.0f / totalWeight;

		iter = BoneWeights.begin();
		while (iter != BoneWeights.end())
		{
			iter->second *= scale;
			iter++;
		}
	}
};

struct asKeyframeData
{
	float Time;

	Vector3 Scale;
	Quaternion Rotation;
	Vector3 Translation;
};

struct asKeyframe
{
	string BoneName;
	vector<asKeyframeData> Transforms;
};

struct asClipNode
{
	vector<asKeyframeData> Keyframe;
	aiString Name;
};

struct asClip
{
	string Name;

	UINT FrameCount;
	float FrameRate;
	float Duration;

	vector<asKeyframe*> Keyframes;
};