#include "stdafx.h"
#include "Converter.h"
#include "Types.h"
#include "Utilities/Xml.h"
#include "Utilities/BinaryFile.h"

Converter::Converter()
{
	importer = new Assimp::Importer();
}

Converter::~Converter()
{
	delete importer;
	/*for (auto material : materials)
	{
		SafeDelete(material);
	}

	for (auto bone : bones)
	{
		SafeDelete(bone);
	}

	for (auto mesh : meshes)
	{
		SafeDelete(mesh);
	}*/
}

void Converter::ReadFile(wstring file)//1)
{
	this->file = L"../../_Assets/" + file;
	

	scene = importer->ReadFile
	(
		String::ToString(this->file),
		aiProcess_ConvertToLeftHanded|
		aiProcess_Triangulate|
		aiProcess_GenUVCoords|
		aiProcess_GenNormals|
		aiProcess_CalcTangentSpace
	);//importer를 이용해 fbx파일을 읽어옴

	assert(scene != NULL);

	
}

void Converter::ExportMaterial(wstring savePath, bool bOverwrite)//2)
{
	savePath = L"../../_Textures/" + savePath + L".material";

	ReadMaterial();
	WriteMaterial(savePath, bOverwrite);
}



void Converter::ReadMaterial()//3)
{
	//불러온 fbx의 파일의 Materials수 만큼 반복 
	for (UINT i = 0; i < scene->mNumMaterials; i++)
	{
		aiMaterial* srcMaterial = scene->mMaterials[i];
		asMaterial* material = new asMaterial();

		//material을 생성해 mMaterials의 인자정보를 입력함
		material->Name = srcMaterial->GetName().C_Str();

		aiColor3D color;

		srcMaterial->Get(AI_MATKEY_COLOR_AMBIENT, color);
		material->Ambient = Color(color.r, color.g, color.b, 1.0f);

		srcMaterial->Get(AI_MATKEY_COLOR_DIFFUSE, color);
		material->Diffuse = Color(color.r, color.g, color.b, 1.0f);

		srcMaterial->Get(AI_MATKEY_COLOR_SPECULAR, color);
		material->Specular = Color(color.r, color.g, color.b, 1.0f);

		srcMaterial->Get(AI_MATKEY_SHININESS, material->Specular.a);

		aiString file;

		srcMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &file);
		material->DiffuseFile = file.C_Str();
		//diffuse타입의 텍스쳐 읽어오고 해당정보를 file에 입력 이후 DiffuseFile에 file의 이름을 입력

		srcMaterial->GetTexture(aiTextureType_SPECULAR, 0, &file);
		material->SpecularFile = file.C_Str();
		//이하반복

		srcMaterial->GetTexture(aiTextureType_NORMALS, 0, &file);
		material->NormalFile = file.C_Str();

		materials.push_back(material);
		//만들어진 material을 materials vector에 입력
	}
}

void Converter::WriteMaterial(wstring savePath, bool bOverWrite)//4)
{
	if (bOverWrite == false && Path::ExistFile(savePath) == true)
		return;
	//파일 덮어쓰기 옵션을 체크

	string folder = String::ToString(Path::GetDirectoryName(savePath));
	string file = String::ToString(Path::GetFileName(savePath));

	Path::CreateFolders(folder);
	//경로상 폴더를 생성한후 XML파일을 생성

	Xml::XMLDocument* document = new Xml::XMLDocument();
	Xml::XMLDeclaration* decl = document->NewDeclaration();
	document->LinkEndChild(decl);

	Xml::XMLElement* root = document->NewElement("Materials");
	root->SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
	root->SetAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");

	document->LinkEndChild(root);
	
	//만들어둔 vector materials의 개수만큼 XML 항목을 작성
	for (auto material : materials)
	{
		Xml::XMLElement* node = document->NewElement("Material");
		root->LinkEndChild(node);

		Xml::XMLElement* element = NULL;

		element = document->NewElement("Name");
		element->SetText(material->Name.c_str());
		node->LinkEndChild(element);

		element = document->NewElement("DiffuseFile");
		element->SetText(WriteTexture(folder, material->DiffuseFile).c_str());//파일 경로 입력
		node->LinkEndChild(element);

		element = document->NewElement("SpecularFile");
		element->SetText(WriteTexture(folder, material->SpecularFile).c_str());
		node->LinkEndChild(element);

		element = document->NewElement("NormalFile");
		element->SetText(WriteTexture(folder, material->NormalFile).c_str());
		node->LinkEndChild(element);

		element = document->NewElement("Ambient");
		element->SetAttribute("R", material->Ambient.r);
		element->SetAttribute("G", material->Ambient.g);
		element->SetAttribute("B", material->Ambient.b);
		element->SetAttribute("A", material->Ambient.a);
		node->LinkEndChild(element);

		element = document->NewElement("Diffuse");
		element->SetAttribute("R", material->Diffuse.r);
		element->SetAttribute("G", material->Diffuse.g);
		element->SetAttribute("B", material->Diffuse.b);
		element->SetAttribute("A", material->Diffuse.a);
		node->LinkEndChild(element);

		element = document->NewElement("Specular");
		element->SetAttribute("R", material->Specular.r);
		element->SetAttribute("G", material->Specular.g);
		element->SetAttribute("B", material->Specular.b);
		element->SetAttribute("A", material->Specular.a);
		node->LinkEndChild(element);

		delete material;
		//element = document->NewElement()
	}

	document->SaveFile((folder + file).c_str());
	//이후 파일을 생성

	delete document;
}

string Converter::WriteTexture(string savePath, string file)//file은 Type에 맞는 텍스쳐 file의 이름
{
	if (file.length() < 1)
	{
		return "";
	}
	string fileName = Path::GetFileName(file);
	
	const aiTexture* texture = scene->GetEmbeddedTexture(file.c_str());//texture에 file명에 맞는 텍스쳐를 입력

	string path = "";
	if (texture != NULL)
	{
		path = savePath + Path::GetFileNameWithoutExtension(file) + ".png";
		//파일 경로를 설정후 Texture의 mHeight에 따라 다른 명령수행
		if (texture->mHeight < 1)
		{
			BinaryWriter w;
			w.Open(String::ToWString(path));
			w.BYTE(texture->pcData, texture->mWidth);
			w.Close();
			//mHeight가 1 미만인경우 BinaryWriter를 이용해 Byte데이터로 파일생성
		}
		else
		{
			ID3D11Texture2D* dest;
			D3D11_TEXTURE2D_DESC destDesc;
			ZeroMemory(&destDesc, sizeof(D3D11_TEXTURE2D_DESC));
			destDesc.Width = texture->mWidth;
			destDesc.Height = texture->mHeight;
			destDesc.MipLevels = 1;//확대,축소
			destDesc.ArraySize = 1;
			destDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
			destDesc.SampleDesc.Count = 1;
			destDesc.SampleDesc.Quality = 0;
			destDesc.Usage = D3D11_USAGE_IMMUTABLE;

			D3D11_SUBRESOURCE_DATA subResource = { 0 };
			subResource.pSysMem = texture->pcData;

			HRESULT hr;
			hr = D3D::GetDevice()->CreateTexture2D(&destDesc, &subResource, &dest);
			Check(hr);

			D3DX11SaveTextureToFile(D3D::GetDC(), dest, D3DX11_IFF_PNG,savePath.c_str());
		}
	}
	else//텍스쳐가 없는경우
	{
		string directory = Path::GetDirectoryName(String::ToString(this->file));
		string origin = directory + file;
		String::Replace(&origin, "\\", "/");
		//파일경로 생성

		if (Path::ExistFile(origin) == false)
		{
			return "";
		}//원본폴더에 파일이없는경우 null 리턴

		path = savePath + fileName;
		CopyFileA(origin.c_str(), path.c_str(), FALSE);
		//원본폴더에 파일이있으면 _textures폴더에 해당파일을 복사함

		String::Replace(&path, "../../_Textures", "");
		//경로 정리
	}

	return Path::GetFileName(path);//경로에 해당하는 파일명 리턴
}

void Converter::ExportMesh(wstring savePath, bool bOverWrite)
{
	savePath = L"../../_Models/" + savePath + L".mesh";

	ReadBoneData(scene->mRootNode, -1, -1);
	ReadSkinData();
	WriteMeshData(savePath, bOverWrite);
}

void Converter::ReadBoneData(aiNode * node, int index, int parent)
{
	asBone* bone = new asBone();
	bone->Index = index;
	bone->Name = node->mName.C_Str();
	bone->Parent = parent;
	
	Matrix transform(node->mTransformation[0]);
	D3DXMatrixTranspose(&bone->Transform, &transform);

	Matrix matParent;
	if (parent < 0)//부모가 없는 bone(-1)인 경우
	{
		D3DXMatrixIdentity(&matParent);//위치정보는 그대로
	}
	else
	{
		matParent = bones[parent]->Transform;//부모가 있을경우 부모Bone위치를 따라감
	}

	bone->Transform = bone->Transform*matParent;

	bones.push_back(bone);

	ReadMeshData(node, index);

	for (UINT i = 0; i < node->mNumChildren; i++)
	{
		ReadBoneData(node->mChildren[i], bones.size(), index);
	}
}

void Converter::ReadMeshData(aiNode * node, int bone)
{
	if (node->mNumMeshes < 1)
		return;

	asMesh* mesh = new asMesh();
	mesh->Name = node->mName.C_Str();
	mesh->BoneIndex = bone;

	for (UINT i = 0; i < node->mNumMeshes; i++)
	{
		UINT index = node->mMeshes[i];
		aiMesh* srcMesh = scene->mMeshes[index];

		aiMaterial* material = scene->mMaterials[srcMesh->mMaterialIndex];
		mesh->MaterialName = material->GetName().C_Str();

		UINT startVertex = mesh->Verticies.size();
		//Vertex 생성
		for (UINT v = 0; v < srcMesh->mNumVertices; v++)
		{
			Model::ModelVertex vertex;
			memcpy(&vertex.Position, &srcMesh->mVertices[v], sizeof(Vector3));

			if (srcMesh->HasTextureCoords(0))//UV
			{
				memcpy(&vertex.Uv, &srcMesh->mTextureCoords[0][v], sizeof(Vector2));
			}

			if (srcMesh->HasNormals())
			{
				memcpy(&vertex.Normal, &srcMesh->mNormals[v], sizeof(Vector3));
			}

			if (srcMesh->HasTangentsAndBitangents())
			{
				memcpy(&vertex.Tangent, &srcMesh->mTangents[v], sizeof(Vector3));
			}

			mesh->Verticies.push_back(vertex);
		}

		for (UINT f = 0; f < srcMesh->mNumFaces; f++)
		{
			aiFace& face = srcMesh->mFaces[f];

			for (UINT k = 0; k < face.mNumIndices; k++)
			{
				mesh->Indices.push_back(face.mIndices[k]);
				mesh->Indices.back() += startVertex;
			}
		}
		meshes.push_back(mesh);
	}

}

void Converter::ReadSkinData()
{
	for (UINT i = 0; i < scene->mNumMeshes; i++)
	{
		aiMesh* aiMesh = scene->mMeshes[i];

		if (aiMesh->HasBones() == false)
		{
			continue;
		}

		asMesh* mesh = meshes[i];

		vector<asBoneWeight> boneWeights;
		boneWeights.assign(mesh->Verticies.size(), asBoneWeight());

		for (UINT b = 0; b < aiMesh->mNumBones; b++)
		{
			aiBone* aiBone = aiMesh->mBones[b];

			UINT boneIndex = 0;
			for (auto bone :bones)
			{
				if (bone->Name == (string)aiBone->mName.C_Str())
				{
					boneIndex = bone->Index;
					break;
				}
			}

			for (UINT w = 0; w < aiBone->mNumWeights; w++)
			{
				UINT index = aiBone->mWeights[w].mVertexId;
				float weight = aiBone->mWeights[w].mWeight;

				boneWeights[index].AddWeight(boneIndex, weight);
			}
		}

		for (UINT w = 0; w < boneWeights.size(); w++)
		{
			boneWeights[w].Normalize();

			asBlendWeight blendWeights;

			boneWeights[w].GetBlendWeight(blendWeights);

			mesh->Verticies[w].BlendIndices = blendWeights.Indices;
			mesh->Verticies[w].BlendWeights = blendWeights.Weight;
		}
	}
}

void Converter::WriteMeshData(wstring savePath, bool bOverWrite)
{
	if (bOverWrite == false && Path::ExistFile(savePath) == true)
		return;

	auto dir = Path::GetDirectoryName(savePath);
	Path::CreateFolders(Path::GetDirectoryName(savePath));

	BinaryWriter* w = new BinaryWriter();
	w->Open(savePath);

	w->UInt(bones.size());
	for (auto bone : bones)
	{
		w->Int(bone->Index);
		w->String(bone->Name);
		w->Int(bone->Parent);
		 
		w->Matrix(bone->Transform);

		delete bone;
	}

	w->UInt(meshes.size());
	for (auto mesh: meshes)
	{
		w->String(mesh->Name);
		w->Int(mesh->BoneIndex);
		 
		w->String(mesh->MaterialName);
		 
		w->UInt(mesh->Verticies.size());
		w->BYTE(&mesh->Verticies[0], sizeof(Model::ModelVertex)*mesh->Verticies.size());
		 
		w->UInt(mesh->Indices.size());
		w->BYTE(&mesh->Indices[0], sizeof(UINT)*mesh->Indices.size());

		delete mesh;
	}

	w->Close();
	delete w;
}

void Converter::ClipList(vector<wstring>& list)
{
	for (UINT i = 0; i < scene->mNumAnimations; i++)
	{
		aiAnimation* anim = scene->mAnimations[i];
		list.push_back(String::ToWString(anim->mName.C_Str()));
	}
}

void Converter::ExportAnimClip(UINT index, wstring savePath, bool bOverwrite)
{
	savePath = L"../../_Models/" + savePath + L".clip";

	asClip* clip = ReadClipData(scene->mAnimations[index]);
	WriteClipData(clip, savePath, bOverwrite);

}

asClip * Converter::ReadClipData(aiAnimation * animation)
{
	asClip* clip		= new asClip();
	clip->Name			= animation->mName.C_Str();
	clip->FrameRate		= (float)animation->mTicksPerSecond;
	clip->FrameCount	= animation->mDuration + 1;

	vector<asClipNode> aniNodeInfos;

	for (UINT i = 0; i < animation->mNumChannels; i++)
	{
		auto aniNode = animation->mChannels[i];

		asClipNode aniNodeInfo;
		aniNodeInfo.Name = aniNode->mNodeName;

		UINT keyCount = max(aniNode->mNumPositionKeys,aniNode->mNumRotationKeys);
		keyCount = max(keyCount, aniNode->mNumScalingKeys);

		
		asKeyframeData frameData;
		for (UINT k = 0; k < keyCount; k++)
		{
			bool bFound = false;
			UINT t = aniNodeInfo.Keyframe.size();

			//T
			if (fabsf((float)aniNode->mPositionKeys[k].mTime - (float)t) <= D3DX_16F_EPSILON)//거의같다는 표현 10^-16승보다 차가 적다
			{
				aiVectorKey key = aniNode->mPositionKeys[k];
				memcpy_s(&frameData.Translation, sizeof(Vector3), &key.mValue, sizeof(aiVector3D));

				frameData.Time = (float)aniNode->mPositionKeys[k].mTime;

				bFound = true;
			}

			//R
			if (fabsf((float)aniNode->mRotationKeys[k].mTime - (float)t) <= D3DX_16F_EPSILON)
			{
				aiQuatKey key = aniNode->mRotationKeys[k];

				frameData.Rotation.x = key.mValue.x;
				frameData.Rotation.y = key.mValue.y;
				frameData.Rotation.z = key.mValue.z;
				frameData.Rotation.w = key.mValue.w;

				frameData.Time = (float)aniNode->mRotationKeys[k].mTime;

				bFound = true;
			}

			//S
			if (fabsf((float)aniNode->mScalingKeys[k].mTime - (float)t) <= D3DX_16F_EPSILON)
			{
				aiVectorKey key = aniNode->mScalingKeys[k];
				memcpy_s(&frameData.Scale, sizeof(Vector3), &key.mValue, sizeof(aiVector3D));

				frameData.Time = (float)aniNode->mScalingKeys[k].mTime;

				bFound = true;
			}

			if (bFound == true)
				aniNodeInfo.Keyframe.push_back(frameData);
		}

		if (aniNodeInfo.Keyframe.size() < clip->FrameCount)
		{
			UINT count = clip->FrameCount - aniNodeInfo.Keyframe.size();

			asKeyframeData keyFrame = aniNodeInfo.Keyframe.back();

			for (UINT n = 0; n < count; n++)
			{
				aniNodeInfo.Keyframe.push_back(keyFrame);
			}
		}
		clip->Duration = max(clip->Duration, aniNodeInfo.Keyframe.back().Time);

		aniNodeInfos.push_back(aniNodeInfo);
	}
	ReadKeyFrameData(clip, scene->mRootNode, aniNodeInfos);

	return clip;
}

void Converter::ReadKeyFrameData(asClip * clip, aiNode * node, vector<struct asClipNode>& aiNodeInfos)
{
	asKeyframe* keyframe = new asKeyframe();

	keyframe->BoneName = node->mName.C_Str();

	for (UINT i = 0; i < clip->FrameCount; i++)
	{
		asClipNode* asClipNode = nullptr;
		for (UINT n = 0; n < aiNodeInfos.size(); n++)
		{
			if (aiNodeInfos[n].Name == node->mName)
			{
				asClipNode = &aiNodeInfos[n];
			}
		}

		asKeyframeData frameData;
		if (asClipNode == nullptr)//같은이름을 못찾은경우
		{
			Matrix transform(node->mTransformation[0]);
			D3DXMatrixTranspose(&transform, &transform);//역행렬화

			D3DXMatrixDecompose(&frameData.Scale, &frameData.Rotation, &frameData.Translation, &transform);//world를 다시 S,R,T로 나눔
			frameData.Time = (float)i;
		}
		else
		{
			frameData = asClipNode->Keyframe[i];
		}
		keyframe->Transforms.push_back(frameData);
	}
	clip->Keyframes.push_back(keyframe);

	for (UINT i = 0; i < node->mNumChildren; i++)
	{
		ReadKeyFrameData(clip, node->mChildren[i], aiNodeInfos);
	}
}

void Converter::WriteClipData(asClip * clip, wstring savePath, bool bOverwrite)
{
	if (bOverwrite == false && Path::ExistFile(savePath) == true)
	{
		return;
	}

	Path::CreateFolders(Path::GetDirectoryName(savePath));

	BinaryWriter* w = new BinaryWriter();
	w->Open(savePath);

	w->String(clip->Name);
	w->Float(clip->Duration);
	w->Float(clip->FrameRate);
	w->UInt(clip->FrameCount);

	w->UInt(clip->Keyframes.size());
	for (asKeyframe* keyframe : clip->Keyframes)
	{
		w->String(keyframe->BoneName);

		w->UInt(keyframe->Transforms.size());
		w->BYTE(&keyframe->Transforms[0], sizeof(asKeyframeData)*keyframe->Transforms.size());

		SafeDelete(keyframe);
	}

	w->Close();
	delete w;
}
