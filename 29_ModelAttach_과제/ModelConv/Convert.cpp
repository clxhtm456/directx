#include "stdafx.h"
#include "Convert.h"
#include "Converter.h"
#include "Utilities/Xml.h"


void Convert::Initialize()
{
	/*AirPlane();
	Tower();
	Tank();*/
	//Kachujin();
	//Kaya();

	//Weapon();
}

void Convert::AirPlane()
{
	Converter* conv = new Converter();
	conv->ReadFile(L"B787/Airplane.fbx");
	conv->ExportMaterial(L"B787/Airplane");
	conv->ExportMesh(L"B787/Airplane");
	delete conv;
}

void Convert::Tower()
{
	Converter* conv = new Converter();
	conv->ReadFile(L"Tower/Tower.fbx");
	conv->ExportMaterial(L"Tower/Tower");
	conv->ExportMesh(L"Tower/Tower");
	delete conv;
}

void Convert::Tank()
{
	Converter* conv = new Converter();
	conv->ReadFile(L"Tank/Tank.fbx");
	conv->ExportMaterial(L"Tank/Tank",false);
	conv->ExportMesh(L"Tank/Tank");
	delete conv;
}

void Convert::Kachujin()
{
	Converter* conv = new Converter();
	conv->ReadFile(L"Kachujin/Mesh.fbx");
	conv->ExportMaterial(L"Kachujin/Mesh");
	conv->ExportMesh(L"Kachujin/Mesh");
	delete conv;

	conv = new Converter();
	conv->ReadFile(L"Kachujin/Idle.fbx");
	conv->ExportAnimClip(0,L"Kachujin/Idle");
	delete conv;
	conv = new Converter();
	conv->ReadFile(L"Kachujin/Running.fbx");
	conv->ExportAnimClip(0, L"Kachujin/Running");
	delete conv;
	conv = new Converter();
	conv->ReadFile(L"Kachujin/Jump.fbx");
	conv->ExportAnimClip(0, L"Kachujin/Jump");
	delete conv;
	conv = new Converter();
	conv->ReadFile(L"Kachujin/Hip_Hop_Dancing.fbx");
	conv->ExportAnimClip(0, L"Kachujin/Hip_Hop_Dancing");
	delete conv;
}

void Convert::Kaya()
{
	Converter* conv = new Converter();
	conv->ReadFile(L"Kaya/Mesh.fbx");
	conv->ExportMaterial(L"Kaya/Mesh");
	conv->ExportMesh(L"Kaya/Mesh");
	delete conv;

	conv = new Converter();
	conv->ReadFile(L"Kaya/Capoeira.fbx");
	conv->ExportAnimClip(0, L"Kaya/Capoeira");
	delete conv;

	conv = new Converter();
	conv->ReadFile(L"Kaya/Jumping.fbx");
	conv->ExportAnimClip(0, L"Kaya/Jumping");
	delete conv;
}

void Convert::Weapon()
{
	vector<wstring> names;
	names.push_back(L"Weapon/Cutter.FBX");
	names.push_back(L"Weapon/Cutter2.FBX");
	names.push_back(L"Weapon/Dagger_epic.FBX");
	names.push_back(L"Weapon/Dagger_small.FBX");
	names.push_back(L"Weapon/Katana.FBX");
	names.push_back(L"Weapon/LongArrow.obj");
	names.push_back(L"Weapon/LongBow.obj");
	names.push_back(L"Weapon/Rapier.FBX");
	names.push_back(L"Weapon/Sword.FBX");
	names.push_back(L"Weapon/Sword_epic.FBX");
	names.push_back(L"Weapon/Sword2.FBX");

	for (auto name :names)
	{
		auto conv = new Converter();
		conv->ReadFile(name);

		String::Replace(&name, L".fbx", L"");
		String::Replace(&name, L".FBX", L"");
		conv->ExportMaterial(name, false);
		conv->ExportMesh(name);
		delete conv;
	}
	
}
