#pragma once

#include "Systems/IExecute.h"

class Scene01 : public IExecute
{
public:
	virtual void Initialize()	override;
	virtual void Ready()		override {};
	virtual void Destroy()		override;
	virtual void Update()		override;
	virtual void PreRender()	override {};
	virtual void Render()		override;
	virtual void PostRender()	override {};
	virtual void ResizeScreen()	override {};

private:
private:
	Shader* shader;
	Shader* floorShader;

	Material* gridMaterial;
	MeshGrid* grid;

	class Player* player;
	class Enemy* enemy[3];
};