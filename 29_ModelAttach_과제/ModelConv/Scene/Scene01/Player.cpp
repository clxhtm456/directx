#include "stdafx.h"
#include "Player.h"

#define MODEL_HEIGHT	-2.0f
#define PLAYER_SPEED	2.0f

Player::Player(Shader* shader):
	shader(shader)
{
	t = new Transform();
	t->Position(0, 2, 0);
	t->Scale(1, 4, 1);
	model = new ModelAnimator(shader);

	model->ReadMaterial(L"Kachujin/Mesh");
	model->ReadMesh(L"Kachujin/Mesh");

	model->ReadClip(L"Kachujin/Idle");
	model->ReadClip(L"Kachujin/Running");


	Vector3 playerPos;
	t->Position(&playerPos);

	model->GetTransform()->Position(playerPos.x, playerPos.y+ MODEL_HEIGHT, playerPos.z);
	model->GetTransform()->Scale(0.025f, 0.025f, 0.025f);
	model->Pass(2);
	
	

	collider = new Collider(t);

	running = false;

	
}

Player::~Player()
{
	delete t;
	delete model;
	delete collider;
}

void Player::Update()
{
	KeyboardControl();

	{
		Vector3 playerPos;
		t->Position(&playerPos);

		Vector3 playerRot;
		t->Rotation(&playerRot);

		model->GetTransform()->Position(playerPos.x, playerPos.y + MODEL_HEIGHT, playerPos.z);
		model->GetTransform()->Rotation(playerRot);
	}
	
	model->Update();
	collider->Update();
}

void Player::Render()
{
	
	model->Render();
	collider->Render(Color(1,0,0,1));

}

void Player::KeyboardControl()
{
	Vector3 tempPos;
	t->Position(&tempPos);
	Vector3 originPos = tempPos;

	if (running &&
		!Keyboard::Get()->Press(VK_DOWN) &&
		!Keyboard::Get()->Press(VK_UP) &&
		!Keyboard::Get()->Press(VK_RIGHT) &&
		!Keyboard::Get()->Press(VK_LEFT))
	{
		running = false;
		model->PlayClip(0, 1.0f, 1.0f);
	}

	if (!running && (Keyboard::Get()->Down(VK_DOWN) || Keyboard::Get()->Down(VK_UP) ||
		Keyboard::Get()->Down(VK_RIGHT) || Keyboard::Get()->Down(VK_LEFT)))
	{
		model->PlayClip(1, 1.0f, 1.0f);
		running = true;
	}

	if (Keyboard::Get()->Press(VK_DOWN))
	{
		tempPos.z -= PLAYER_SPEED * Time::Delta();
	}
	else if (Keyboard::Get()->Press(VK_UP))
	{
		tempPos.z += PLAYER_SPEED * Time::Delta();
	}

	if (Keyboard::Get()->Press(VK_RIGHT))
	{
		tempPos.x += PLAYER_SPEED * Time::Delta();
	}
	else if (Keyboard::Get()->Press(VK_LEFT))
	{
		tempPos.x -= PLAYER_SPEED * Time::Delta();
	}

	if (originPos != tempPos)
	{
		auto quaternion = Math::LookAt(originPos, tempPos);
		Vector3 rotation;
		FLOAT fRot;
		D3DXQuaternionToAxisAngle(&quaternion, &rotation,&fRot);
		t->Rotation(0, originPos.x > tempPos.x ? fRot: -fRot,0);
	}
	t->Position(tempPos);
}

void Player::PutWeapon(Model* weaponModel)
{
	if (equipItem != nullptr)
	{
		model->GetModel()->Detach(equipItem, 35);
		delete equipItem;
	}
		

	equipItem = weaponModel;

	Transform attachTransform;
	attachTransform.Position(-10, 0, -10);
	model->GetModel()->Attach(shader, weaponModel, 35, &attachTransform);
	model->CreateTexture();

	model->Pass(2);
}

