#pragma once

#include "Scene/Scene02/WeaponItem.h"

class Player : public WeaponHandler
{
public:
	Player(Shader* shader);
	~Player();
public:
	void Update();
	void Render();

	Collider* GetCollider() { return collider; }
private:
	void KeyboardControl();
private:
	Transform* t;
	ModelAnimator* model;
	Collider* collider;
	Shader* shader;

	bool running;

	// WeaponHandler을(를) 통해 상속됨
	virtual void PutWeapon(Model* model) override;
};