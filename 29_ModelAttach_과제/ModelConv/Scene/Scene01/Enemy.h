#pragma once

class Enemy
{
public:
	Enemy(Shader* shader);
	~Enemy();
public:
	void Update();
	void Render();

	void CollisionCheck(Collider* target);
	Collider* GetCollider() { return collider; }
private:
	void StartMoveEvent();
	bool MoveToward();
private:
	Transform* t;
	ModelAnimator* model;
	Collider* collider;

	float handsUpTimer;
	Vector3 destination;

	bool collisionCheck;
	UINT phase;
};