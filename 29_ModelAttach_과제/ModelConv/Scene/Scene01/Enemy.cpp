#include "stdafx.h"
#include "Enemy.h"

#define MODEL_HEIGHT	-2.0f
#define ENEMY_SPEED		5.0f

Enemy::Enemy(Shader* shader)
{
	t = new Transform();
	t->Position(Math::Random(-10.0f,10.0f), 2, Math::Random(-10.0f, 10.0f));
	t->Scale(1, 4, 1);
	model = new ModelAnimator(shader);

	model->ReadMaterial(L"Kachujin/Mesh");
	model->ReadMesh(L"Kachujin/Mesh");

	model->ReadClip(L"Kachujin/Idle");
	model->ReadClip(L"Kachujin/Running");
	model->ReadClip(L"Kachujin/Jump");


	Vector3 EnemyPos;
	t->Position(&EnemyPos);

	model->GetTransform()->Position(EnemyPos.x, EnemyPos.y + MODEL_HEIGHT, EnemyPos.z);
	model->GetTransform()->Scale(0.025f, 0.025f, 0.025f);
	model->Pass(2);

	collider = new Collider(t);

	collisionCheck = false;
	phase = 0;
}

Enemy::~Enemy()
{
	delete t;
	delete model;
	delete collider;
}

void Enemy::Update()
{
	switch (phase)
	{
		case 1:
		{
			if (handsUpTimer < Time::Delta())
			{
				model->PlayClip(1, 1.0f, 0.7f);
				phase = 2;
			}
			else
				handsUpTimer -= Time::Delta();
		}break;

		case 2:
		{
			if (MoveToward() == true)
			{
				phase = 0;
				model->PlayClip(0, 1.0f, 1.0f);
			}
				
		}break;

	}

	Vector3 EnemyPos;
	t->Position(&EnemyPos);

	Vector3 EnemyRot;
	t->Rotation(&EnemyRot);

	model->GetTransform()->Rotation(EnemyRot);
	model->GetTransform()->Position(EnemyPos.x, EnemyPos.y + MODEL_HEIGHT, EnemyPos.z);

	model->Update();
	collider->Update();
}

void Enemy::Render()
{
	model->Render();
	collider->Render(collisionCheck ? Color(1, 0, 0, 1) : Color(0, 1, 0, 1));
}

void Enemy::CollisionCheck(Collider* target)
{
	collisionCheck = collider->IsIntersect(target);

	if (phase == 0 && collisionCheck)
		StartMoveEvent();
}

void Enemy::StartMoveEvent()
{
	model->PlayClip(2, 1.0f, 1.0f);
	handsUpTimer = 1.0f;
	phase = 1;
	destination = Vector3(Math::Random(-10.0f, 10.0f), 2, Math::Random(-10.0f, 10.0f));
}

bool Enemy::MoveToward()
{
	Vector3 tempPosition;
	t->Position(&tempPosition);
	auto dest = Math::MoveTowards(tempPosition, destination, ENEMY_SPEED * Time::Delta());

	if (tempPosition != dest)
	{
		auto quaternion = Math::LookAt(tempPosition, dest);
		Vector3 rotation;
		FLOAT fRot;
		D3DXQuaternionToAxisAngle(&quaternion, &rotation, &fRot);
		t->Rotation(0, tempPosition.x > dest.x ? fRot : -fRot, 0);
	}

	t->Position(dest);

	if (Math::Distance(tempPosition, destination) <= 0.1f)
		return true;
	else
		return false;
}
