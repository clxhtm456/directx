#include "stdafx.h"
#include "Scene02.h"
#include "Scene01/Player.h"
#include "Scene/Scene02/WeaponItem.h"
#include "Viewer/Freedom.h"

void Scene02::Initialize()
{
	Context::Get()->GetCamera()->RotationDegree(50, 0, 0);
	Context::Get()->GetCamera()->Position(0, 17, -15);
	dynamic_cast<Freedom*>(Context::Get()->GetCamera())->Speed(20, 2);

	shader = new Shader(L"26_Animation.fx");
	floorShader = new Shader(L"25_Model.fx");

	gridMaterial = new Material(floorShader);
	gridMaterial->DiffuseMap(L"Floor.png");
	grid = new MeshGrid(floorShader);
	grid->GetTransform()->Scale(2.0f, 1.0f, 2.0f);

	player = new Player(shader);

	weaponShader = new Shader(L"25_Model.fx");

	CreateWeapon(L"Weapon/Sword");
	CreateWeapon(L"Weapon/Sword_epic");
	CreateWeapon(L"Weapon/Sword2");
}

void Scene02::Destroy()
{
	delete shader;
	delete player;
	delete gridMaterial;
	delete grid;

	for (auto weapon : weapons)
		delete weapon;
}

void Scene02::Update()
{
	grid->Update();
	player->Update();
	for (auto weapon : weapons)
		weapon->Update();
}

void Scene02::Render()
{
	gridMaterial->Render();
	grid->Render();

	player->Render();
	for (int i = 0; i < weapons.size(); i++)
	{
		weapons[i]->Render();
		if (weapons[i]->CollisionCheck(player))
		{
			delete weapons[i];
			weapons.erase(weapons.begin() + i);
		}
	}
}

void Scene02::CreateWeapon(wstring dir)
{
	auto weapon = new WeaponItem(weaponShader);
	weapon->ReadMaterial(dir);
	weapon->ReadMesh(dir);

	Vector3 pos = Vector3(Math::Random(-10.0f, 10.0f), 0, Math::Random(-10.0f, 10.0f));
	Vector3 rot = Vector3(0, Math::Random(0.0f, 359.0f), 0);
	weapon->GetTransform()->Position(pos);
	weapon->GetTransform()->RotationDegree(rot);

	weapons.push_back(weapon);
}
