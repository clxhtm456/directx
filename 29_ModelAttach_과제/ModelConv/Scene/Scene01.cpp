#include "stdafx.h"
#include "Scene01.h"
#include "Viewer/Freedom.h"
#include "Scene/Scene01/Player.h"
#include "Scene/Scene01/Enemy.h"

void Scene01::Initialize()
{
	Context::Get()->GetCamera()->RotationDegree(50, 0, 0);
	Context::Get()->GetCamera()->Position(0, 17, -15);
	dynamic_cast<Freedom*>(Context::Get()->GetCamera())->Speed(20, 2);

	shader = new Shader(L"26_Animation.fx");
	floorShader = new Shader(L"25_Model.fx");

	gridMaterial = new Material(floorShader);
	gridMaterial->DiffuseMap(L"Floor.png");
	grid = new MeshGrid(floorShader);
	grid->GetTransform()->Scale(2.0f, 1.0f, 2.0f);

	player = new Player(shader);

	
	for (int i = 0; i < 3; i++)
	{
		enemy[i] = new Enemy(shader);
	}
		
}

void Scene01::Destroy()
{
	delete shader;
	delete player;
	delete gridMaterial;
	delete grid;

	for (int i = 0; i < 3; i++)
	{
		delete enemy[i];
	}
}

void Scene01::Update()
{
	grid->Update();
	player->Update();
	for (int i = 0; i < 3; i++)
		enemy[i]->Update();
}

void Scene01::Render()
{
	gridMaterial->Render();
	grid->Render();

	player->Render();
	for (int i = 0; i < 3; i++)
	{
		enemy[i]->CollisionCheck(player->GetCollider());
		enemy[i]->Render();
	}
}
