#pragma once
#include "Systems/IExecute.h"

class Scene02 : public IExecute
{
public:
	virtual void Initialize()	override;
	virtual void Ready()		override {};
	virtual void Destroy()		override;
	virtual void Update()		override;
	virtual void PreRender()	override {};
	virtual void Render()		override;
	virtual void PostRender()	override {};
	virtual void ResizeScreen()	override {};
public:
	void CreateWeapon(wstring dir);

private:
	Shader * shader;
	Shader* weaponShader;
	Shader* floorShader;

	Material* gridMaterial;
	MeshGrid* grid;

	class Player* player;
	vector<class WeaponItem*> weapons;
};