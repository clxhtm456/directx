#include "stdafx.h"
#include "WeaponItem.h"
#include "Scene/Scene01/Player.h"

WeaponItem::WeaponItem(Shader* shader)
{
	t = new Transform();
	t->Position(0, 2, 0);
	t->Scale(1, 1, 3);
	model = new ModelRender(shader);

	Vector3 playerPos;
	t->Position(&playerPos);

	model->GetTransform()->Position(playerPos.x, playerPos.y, playerPos.z);
	model->GetTransform()->Scale(0.025f, 0.025f, 0.025f);
	model->Pass(1);

	collider = new Collider(t);
}

WeaponItem::~WeaponItem()
{
	delete model;
	delete collider;
	delete t;
}

void WeaponItem::Update()
{
	if (!render)
		return;
	{
		Vector3 playerPos;
		t->Position(&playerPos);

		Vector3 playerRot;
		t->Rotation(&playerRot);

		model->GetTransform()->Position(playerPos.x, playerPos.y, playerPos.z);
		model->GetTransform()->Rotation(playerRot);
	}

	model->Update();
	collider->Update();
}

void WeaponItem::Render()
{
	if (!render)
		return;
	model->Render();
	collider->Render(Color(0, 1, 0, 1));
}

void WeaponItem::ReadMaterial(wstring file)
{
	model->ReadMaterial(file);
	materialDir = file;
}

void WeaponItem::ReadMesh(wstring file)
{
	model->ReadMesh(file);
	meshDir = file;
	render = true;
}

bool WeaponItem::CollisionCheck(Player * target)
{
	collisionCheck = collider->IsIntersect(target->GetCollider());

	if (collisionCheck)
	{
		WeaponEvent(target);
		return true;
	}
	else
		return false;
}

void WeaponItem::WeaponEvent(WeaponHandler* handler)
{
	Model* tempModel = new Model();
	tempModel->ReadMaterial(materialDir);
	tempModel->ReadMesh(meshDir);
	handler->PutWeapon(tempModel);
}
