#pragma once

class WeaponHandler
{
protected:
	Model* equipItem = nullptr;
public:
	virtual void PutWeapon(Model* model) = 0;
};

class WeaponItem
{
public:
	WeaponItem(Shader* shader);
	~WeaponItem();

	void Update();
	void Render();

	void ReadMaterial(wstring file);
	void ReadMesh(wstring file);

	bool CollisionCheck(class Player* target);
	Collider* GetCollider() { return collider; }
	Transform* GetTransform() { return t; }

	void WeaponEvent(WeaponHandler* handler);

private:
	wstring materialDir;
	wstring meshDir;
private:
	Transform* t;
	ModelRender* model;
	Collider* collider;
	Model* equipModel;

	bool collisionCheck;
	bool render = false;
};