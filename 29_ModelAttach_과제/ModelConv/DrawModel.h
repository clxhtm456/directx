#pragma once

#include "Systems/IExecute.h"

class DrawModel : public IExecute
{
public:
	virtual void Initialize()	override;
	virtual void Ready()		override {};
	virtual void Destroy()		override;
	virtual void Update()		override;
	virtual void PreRender()	override {};
	virtual void Render()		override;
	virtual void PostRender()	override {};
	virtual void ResizeScreen()	override {};

private:
	void AirPlane();
	void Tower();
	void Tank();
private:
	Shader* shader;
	Shader* cubeShader;

	class ModelSelector* modelSelector;

	ModelRender* airPlane = nullptr;
	ModelRender* tower = nullptr;
	ModelRender* tank = nullptr;

	MeshCube* cube = nullptr;
};