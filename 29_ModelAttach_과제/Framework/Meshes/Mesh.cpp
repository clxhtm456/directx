#include "Framework.h"
#include "Mesh.h"

Mesh::Mesh(Shader * shader)
	: Renderer(shader)
{
	sDiffseMap = shader->AsSRV("DiffuseMap");
	
}

Mesh::~Mesh()
{
	SafeDeleteArray(vertices);
	SafeDeleteArray(indices);

	SafeDelete(vertexBuffer);
	SafeDelete(indexBuffer);
}

void Mesh::Render()
{
	if (vertexBuffer == NULL || indexBuffer == NULL)
	{
		Create();

		vertexBuffer = new VertexBuffer(vertices, vertexCount, sizeof(MeshVertex));
		indexBuffer = new IndexBuffer(indices, indexCount);
	}

	Super::Update();
	Super::Render();

	shader->AsVector("Color")->SetFloatVector(color);

	if (diffuseMap != NULL)
		sDiffseMap->SetResource(diffuseMap->SRV());

	shader->DrawIndexed(0, Pass(), indexCount);
}

void Mesh::DiffuseMap(wstring file)
{
	SafeDelete(diffuseMap);
	diffuseMap = new Texture(file);
}

void Mesh::SetColor(Color color)
{
	this->color = color;
}
