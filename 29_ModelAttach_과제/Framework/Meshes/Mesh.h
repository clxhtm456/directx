#pragma once

class Mesh : public Renderer
{
public:
	typedef VertexColorTextureNormal MeshVertex;

public:
	Mesh(Shader* shader);
	virtual ~Mesh();

	void Render();

	void DiffuseMap(wstring file);
	void SetColor(Color color);

protected:
	virtual void Create() = 0;
	
protected:
	MeshVertex* vertices;
	UINT* indices;

	Color color;

private:
	Texture * diffuseMap = NULL;
	ID3DX11EffectShaderResourceVariable* sDiffseMap;
};
