#include "Framework.h"
#include "MeshQuad.h"

MeshQuad::MeshQuad(Shader * shader)
	: Mesh(shader)
{

}

MeshQuad::~MeshQuad()
{
	//부모에서 지워주기 때문 필요 없음
}

void MeshQuad::Create()
{
	float w = 0.5f;
	float h = 0.5f;

	vector<MeshVertex> v;

	v.push_back(MeshVertex(-w, -h, 0, 0, 1, 0, 0, -1)); //좌하0
	v.push_back(MeshVertex(-w, +h, 0, 0, 0, 0, 0, -1)); //좌상1
	v.push_back(MeshVertex(+w, -h, 0, 1, 1, 0, 0, -1)); //우하2
	v.push_back(MeshVertex(+w, +h, 0, 1, 0, 0, 0, -1)); //우상3

	vertices = new MeshVertex[v.size()];
	vertexCount = v.size();

	//벡터를 배열로 복사(memcpy로는 불가능)
	copy
	(
		v.begin(), v.end(),
		//3번째 인자를 vertices로 넣으면 터짐. 왜냐면 vertices는 동적할당이므로 크기를 모름
		//시작과 끝을 찾아서 vertices를 iter로 만들어주는 함수 
		stdext::checked_array_iterator<MeshVertex *>(vertices, vertexCount)
	);

	indexCount = 6;
	indices = new UINT[indexCount]{ 0, 1, 2, 2, 1, 3 };
}
