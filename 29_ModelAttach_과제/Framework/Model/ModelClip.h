#pragma once

struct ModelKeyframeData;
struct ModelKeyframe;

class ModelClip
{
public:
	friend class Model;
private:
	ModelClip();
	~ModelClip();
public:
	float Duration() { return duration; }
	float FrameRate() { return frameRate; }
	UINT FrameCount() { return frameCount; }

	ModelKeyframe* Keyframe(wstring name);
private:
	wstring name;

	float duration;
	float frameRate;//30frame
	UINT frameCount;

	unordered_map<wstring, ModelKeyframe*> keyframeMap;
	/*-------------------------------
	*map은 레드블랙트리를 사용하여 속도가 느린지만
	*unordered 는 hashing 을 사용하여 검색속도가 빠름, 메모리를 많이사용, 삽입삭제가 느림
	*------------------------------*/

};

struct ModelKeyframeData
{
	float Time;

	Vector3 Scale;
	Quaternion Rotation;
	Vector3 Translation;
};

struct ModelKeyframe
{
	wstring BoneName;
	vector<ModelKeyframeData> Transforms;
};