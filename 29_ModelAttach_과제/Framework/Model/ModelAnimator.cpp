#include "Framework.h"
#include "ModelAnimator.h"
#include "ModelMesh.h"

ModelAnimator::ModelAnimator(Shader * shader) :
	shader(shader)
{
	model = new Model();
	transform = new Transform(shader);

	frameBuffer = new ConstantBuffer(&tweenDesc, sizeof(TweenDesc));
	sFrameBuffer = shader->AsConstantBuffer("CB_AnimationFrame");
}


ModelAnimator::~ModelAnimator()
{
	SafeDelete(transform);
	SafeDelete(model);

	SafeDelete(frameBuffer);
	//SafeDeleteArray(clipTransform);
}

void ModelAnimator::Update()
{
	TweenDesc& desc = tweenDesc;
	ModelClip* clip = model->ClipByIndex(desc.Curr.Clip);

	//현재 애니메이션
	{
		desc.Curr.RunningTime += Time::Delta();
		float time = 1.0f / clip->FrameRate() / desc.Curr.Speed;

		if (desc.Curr.Time >= 1.0f)
		{
			desc.Curr.RunningTime = 0.0f;

			desc.Curr.CurrFrame = (desc.Curr.CurrFrame + 1) % clip->FrameCount();
			desc.Curr.NextFrame = (desc.Curr.CurrFrame + 1) % clip->FrameCount();
		}
		desc.Curr.Time = desc.Curr.RunningTime / time;
	}

	if (desc.Next.Clip > -1)
	{
		ModelClip* nextClip = model->ClipByIndex(desc.Next.Clip);

		desc.RunningTime += Time::Delta();
		desc.TweenTime = desc.RunningTime / desc.TakeTime;

		if (desc.TweenTime >= 1.0f)
		{
			desc.Curr = desc.Next;

			desc.Next.Clip = -1;
			desc.Next.CurrFrame = 0;
			desc.Next.NextFrame = 0;
			desc.Next.Time = 0;
			desc.Next.RunningTime = 0.0f;

			desc.RunningTime = 0.0f;
			desc.TweenTime = 0.0f;
		}
		else
		{
			desc.Next.RunningTime += Time::Delta();
			float time = 1.0f / nextClip->FrameRate() / desc.Next.Speed;

			if (desc.Next.Time >= 1.0f)
			{
				desc.Next.RunningTime = 0.0f;

				desc.Next.CurrFrame = (desc.Next.CurrFrame + 1) % nextClip->FrameCount();
				desc.Next.NextFrame = (desc.Next.CurrFrame + 1) % nextClip->FrameCount();
			}
			desc.Next.Time = desc.Next.RunningTime / time;
		}
	}

	for (auto mesh : model->Meshes())
	{
		mesh->Update();
	}
}

void ModelAnimator::Render()
{
	if (texture == nullptr)
		CreateTexture();

	frameBuffer->Apply();
	sFrameBuffer->SetConstantBuffer(frameBuffer->Buffer());

	for (auto mesh : model->Meshes())
	{
		mesh->SetTransform(transform);
		mesh->Render();
	}
}

void ModelAnimator::ReadMaterial(wstring file)
{
	model->ReadMaterial(file);
}

void ModelAnimator::ReadMesh(wstring file)
{
	model->ReadMesh(file);

	for (auto mesh : model->Meshes())
	{
		mesh->SetShader(shader);
	}
}

void ModelAnimator::ReadClip(wstring file)
{
	model->ReadClip(file);
}

void ModelAnimator::PlayClip(UINT clip, float speed, float takeTime)
{
	tweenDesc.TakeTime = takeTime;
	tweenDesc.Next.Clip = clip;
	tweenDesc.Next.Speed = speed;
}

void ModelAnimator::Pass(UINT pass)
{
	for (auto mesh : model->Meshes())
	{
		mesh->Pass(pass);
	}
}

void ModelAnimator::CreateTexture()
{
	clipTransform = new ClipTransform[model->ClipCount()];

	for (UINT i = 0; i < model->ClipCount(); i++)
	{
		CreateClipTransform(i);
	}

	//CreateTexture
	{
		D3D11_TEXTURE2D_DESC desc;
		ZeroMemory(&desc, sizeof(D3D11_TEXTURE2D_DESC));
		desc.Width = MAX_MODEL_TRANSFORMS*4;//Matrix 값이 4*4 행렬이기때문에 4개의 값을 가지는 rgba값만으로는 matrix값을 전부넣을수 없으므로 *4
		desc.Height = MAX_MODEL_KEYFRAMES;
		desc.ArraySize = model->ClipCount();
		desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;//rgba 값에 0~1사이 값이 아닌 float값을 넣을수 있게됨
		desc.Usage = D3D11_USAGE_IMMUTABLE;
		desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		desc.MipLevels = 1;
		desc.SampleDesc.Count = 1;

		int colorSize = sizeof(Color);
		UINT pageSize = MAX_MODEL_TRANSFORMS*4 * MAX_MODEL_KEYFRAMES * colorSize;
		void* p = VirtualAlloc(NULL, pageSize*model->ClipCount(), MEM_RESERVE, PAGE_READWRITE);
		//가상메모리에 동적할당
		//MEM_RESERVE 메모리 선예약

		/*
		메모리 상황 체크
		MEMORY_BASIC_INFORMATION memory;
		VirtualQuery(p, &memory, sizeof(MEMORY_BASIC_INFORMATION));
		*/

		for (UINT c = 0; c < model->ClipCount(); c++)
		{
			for (UINT y = 0; y < MAX_MODEL_KEYFRAMES; y++)
			{
				UINT start = c * pageSize;

				void * temp = (BYTE*)p + start + (y*MAX_MODEL_TRANSFORMS * 4 * colorSize);

				VirtualAlloc(temp, MAX_MODEL_TRANSFORMS * 4 * colorSize, MEM_COMMIT, PAGE_READWRITE);
				//MEM_COMMIT 메모리 할당

				memcpy(temp, clipTransform[c].Transform[y], MAX_MODEL_TRANSFORMS * 4 * colorSize);
			}
		}
		D3D11_SUBRESOURCE_DATA* subResource = new D3D11_SUBRESOURCE_DATA[model->ClipCount()];

		for (UINT c = 0; c < model->ClipCount(); c++)
		{
			void* temp = (BYTE*)p + c*pageSize;

			subResource[c].pSysMem = temp;
			subResource[c].SysMemPitch = MAX_MODEL_TRANSFORMS * colorSize * 4;
			subResource[c].SysMemSlicePitch = pageSize;
		}

		Check(D3D::GetDevice()->CreateTexture2D(&desc, subResource, &texture));

		SafeDeleteArray(subResource);
		VirtualFree(p, NULL, MEM_RELEASE);
	}

	//CreateSRV
	{
		D3D11_TEXTURE2D_DESC desc;
		texture->GetDesc(&desc);

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		ZeroMemory(&srvDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
		srvDesc.Format = desc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION::D3D10_1_SRV_DIMENSION_TEXTURE2DARRAY;
		srvDesc.Texture2DArray.MipLevels = 1;//확대축소배율 1 쓰지않음 텍스쳐가 아닌 애니메이션
		srvDesc.Texture2DArray.ArraySize = model->ClipCount();

		Check(D3D::GetDevice()->CreateShaderResourceView(texture, &srvDesc, &srv));
	}

	for (auto mesh : model->Meshes())
	{
		mesh->TransformsSRV(srv);
	}

	SafeDeleteArray(clipTransform);
}

void ModelAnimator::CreateClipTransform(UINT index)
{
	Matrix* bones = new Matrix[MAX_MODEL_TRANSFORMS];

	ModelClip* clip = model->ClipByIndex(index);

	for (UINT f = 0; f < clip->FrameCount(); f++)
	{
		for (UINT b = 0; b < model->BoneCount(); b++)
		{
			ModelBone* bone = model->BoneByIndex(b);

			Matrix parent;
			Matrix invGlobal = bone->Transform();

			D3DXMatrixInverse(&invGlobal, NULL, &invGlobal);

			int parentIndex = bone->ParentIndex();

			if (parentIndex < 0)
				D3DXMatrixIdentity(&parent);
			else
				parent = bones[parentIndex];

			Matrix animation;
			ModelKeyframe* frame = clip->Keyframe(bone->Name());

			if (frame != NULL)
			{
				ModelKeyframeData& data = frame->Transforms[f];

				Matrix S, R, T;
				D3DXMatrixScaling(&S, data.Scale.x, data.Scale.y, data.Scale.z);
				D3DXMatrixRotationQuaternion(&R, &data.Rotation);
				D3DXMatrixTranslation(&T, data.Translation.x, data.Translation.y, data.Translation.z);

				animation = S * R*T;

				bones[b] = animation * parent;//자신의 위치를 부모위치에 붙힘
				clipTransform[index].Transform[f][b] = invGlobal * bones[b];
			}
			else//이름이 없는경우 ex)attach 를 통해 붙힌 weapon등
			{
				bones[b] = parent;

				clipTransform[index].Transform[f][b] = bone->Transform() * bones[b];
			}
		}
	}
}
