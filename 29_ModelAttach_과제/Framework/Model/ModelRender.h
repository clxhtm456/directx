#pragma once

class ModelRender
{
public:
	ModelRender(Shader* shader);
	~ModelRender();

	void Update();
	void Render();

public:
	Transform* GetTransform() { return transform; }
	Model* GetModel() { return model; }
public:
	void ReadMaterial(wstring file);
	void ReadMesh(wstring file);

	void Pass(UINT pass);

	void UpdateTransform(ModelBone* bone = nullptr, Matrix matrix = Matrix());

private:
	void UpdateBones(ModelBone* bone, Matrix& matrix);

private:
	Shader* shader;
	Model* model;

	Transform* transform;

	Matrix transforms[MAX_MODEL_TRANSFORMS];
};