#include "Framework.h"
#include "Model.h"
#include "ModelMesh.h"
#include "ModelClip.h"

#include "Utilities/Xml.h"
#include "Utilities/BinaryFile.h"

Model::Model()
{
}

Model::~Model()
{
	for (auto material : materials )
	{
		SafeDelete(material);
	}

	for (auto bone : bones)
	{
		SafeDelete(bone);
	}

	for (auto mesh : meshes)
	{
		SafeDelete(mesh);
	}

	for (auto clip : clips)
	{
		SafeDelete(clip);
	}
}

Material * Model::MaterialByName(wstring name)
{
	for (auto material : materials)
	{
		if (material->Name() == name)
		{
			return material;
		}
	}

	return nullptr;
}

ModelBone * Model::BoneByName(wstring name)
{
	for (auto bone : bones)
	{
		if (bone->Name() == name)
			return bone;
	}
	return nullptr;
}

ModelMesh * Model::MeshByName(wstring name)
{
	for (auto mesh : meshes)
	{
		if (mesh->Name() == name)
			return mesh;
	}
	return nullptr;
}

ModelClip * Model::ClipByName(wstring name)
{
	for (auto clip : clips)
	{
		if (clip->name == name)
			return clip;
	}
	return nullptr;
}

void Model::Attach(Shader * shader, Model * model, int parentBoneIndex, Transform * transform)
{
	//Copy Material
	for (auto material : model->Materials())
	{
		Material* newMaterial = new Material(shader);

		newMaterial->Name(material->Name());
		newMaterial->Ambient(material->Ambient());
		newMaterial->Diffuse(material->Diffuse());
		newMaterial->Specular(material->Specular());

		if (material->DiffuseMap() != NULL) newMaterial->DiffuseMap(material->DiffuseMap()->GetFile());
		if (material->SpecularMap() != NULL) newMaterial->SpecularMap(material->SpecularMap()->GetFile());
		if (material->NormalMap() != NULL) newMaterial->NormalMap(material->NormalMap()->GetFile());

		materials.push_back(newMaterial);
	}

	vector<pair<int, int>> changes;
	//Copy Bone
	{
		ModelBone* parentBone = BoneByIndex(parentBoneIndex);

		for (auto bone : model->Bones())
		{
			ModelBone* newBone = new ModelBone();

			newBone->name = bone->name;
			newBone->transform = bone->transform;

			if (transform != nullptr)
			{
				newBone->transform *= transform->World();
			}

			if (bone->parent != nullptr)
			{
				int parentIndex = bone->parentIndex;

				for (auto temp : changes)
				{
					if (temp.first == parentIndex)
					{
						newBone->parentIndex = temp.second;
						newBone->parent = bones[newBone->parentIndex];
						newBone->parent->childs.push_back(newBone);

						break;
					}
				}//for(temp)
			}
			else//최상위 노드일경우
			{
				newBone->parentIndex = parentBoneIndex;
				newBone->parent = parentBone;
				newBone->parent->childs.push_back(newBone);
			}
			newBone->index = bones.size();
			changes.push_back(pair<int, int>(bone->index, newBone->index));

			bones.push_back(newBone);
		}
	}
	//Copy Mesh
	{
		for (auto mesh : model->Meshes())
		{
			ModelMesh* newMesh = new ModelMesh();

			for (auto temp : changes)
			{
				if (temp.first == mesh->boneIndex)
				{
					newMesh->boneIndex = temp.second;
					break;
				}
			}//for(temp)

			newMesh->bone = bones[newMesh->boneIndex];
			newMesh->name = mesh->name;
			newMesh->materialName = mesh->materialName;

			newMesh->vertexCount = mesh->vertexCount;
			newMesh->indexCount = mesh->indexCount;

			UINT verticesSize = newMesh->vertexCount * sizeof(ModelVertex);
			newMesh->vertices = new ModelVertex[newMesh->vertexCount];
			memcpy_s(newMesh->vertices, verticesSize, mesh->vertices, verticesSize);

			UINT indicesSize = newMesh->indexCount * sizeof(UINT);
			newMesh->indices = new UINT[newMesh->indexCount];
			memcpy_s(newMesh->indices, indicesSize, mesh->indices, indicesSize);

			newMesh->Binding(this);
			newMesh->SetShader(shader);

			meshes.push_back(newMesh);
		}
	}

}

void Model::Detach(Model* model, int parentBoneIndex)
{
	for (auto mMaterial : model->Materials())
	{
		auto name = mMaterial->Name();
		

		for (int i = 0; i < materials.size(); i++)
		{
			if (materials[i]->Name() == name)
			{
				materials.erase(materials.begin()+i);
			}
		}
	}

	ModelBone* parentBone = BoneByIndex(parentBoneIndex);

	for (auto mBone : model->Bones())
	{
		auto name = mBone->Name();


		for (int i = 0; i < bones.size(); i++)
		{
			if (bones[i]->Index() == -1)
				continue;
			if (bones[i]->Name() == name)
			{
				bones.erase(bones.begin() + i);
			}
		}
	}
	for (auto mMesh : model->Meshes())
	{
		auto name = mMesh->Name();


		for (int i = 0; i < meshes.size(); i++)
		{
			if (meshes[i]->Name() == name)
			{
				meshes.erase(meshes.begin() + i);
			}
		}
	}

	//vector<pair<int, int>> changes;
	////Copy Bone
	//{
	//	ModelBone* parentBone = BoneByIndex(parentBoneIndex);

	//	for (auto bone : model->Bones())
	//	{
	//		ModelBone* newBone = new ModelBone();

	//		newBone->name = bone->name;
	//		newBone->transform = bone->transform;

	//		if (transform != nullptr)
	//		{
	//			newBone->transform *= transform->World();
	//		}

	//		if (bone->parent != nullptr)
	//		{
	//			int parentIndex = bone->parentIndex;

	//			for (auto temp : changes)
	//			{
	//				if (temp.first == parentIndex)
	//				{
	//					newBone->parentIndex = temp.second;
	//					newBone->parent = bones[newBone->parentIndex];
	//					newBone->parent->childs.push_back(newBone);

	//					break;
	//				}
	//			}//for(temp)
	//		}
	//		else//최상위 노드일경우
	//		{
	//			newBone->parentIndex = parentBoneIndex;
	//			newBone->parent = parentBone;
	//			newBone->parent->childs.push_back(newBone);
	//		}
	//		newBone->index = bones.size();
	//		changes.push_back(pair<int, int>(bone->index, newBone->index));

	//		bones.push_back(newBone);
	//	}
	//}
	////Copy Mesh
	//{
	//	for (auto mesh : model->Meshes())
	//	{
	//		ModelMesh* newMesh = new ModelMesh();

	//		for (auto temp : changes)
	//		{
	//			if (temp.first == mesh->boneIndex)
	//			{
	//				newMesh->boneIndex = temp.second;
	//				break;
	//			}
	//		}//for(temp)

	//		newMesh->bone = bones[newMesh->boneIndex];
	//		newMesh->name = mesh->name;
	//		newMesh->materialName = mesh->materialName;

	//		newMesh->vertexCount = mesh->vertexCount;
	//		newMesh->indexCount = mesh->indexCount;

	//		UINT verticesSize = newMesh->vertexCount * sizeof(ModelVertex);
	//		newMesh->vertices = new ModelVertex[newMesh->vertexCount];
	//		memcpy_s(newMesh->vertices, verticesSize, mesh->vertices, verticesSize);

	//		UINT indicesSize = newMesh->indexCount * sizeof(UINT);
	//		newMesh->indices = new UINT[newMesh->indexCount];
	//		memcpy_s(newMesh->indices, indicesSize, mesh->indices, indicesSize);

	//		newMesh->Binding(this);
	//		newMesh->SetShader(shader);

	//		meshes.push_back(newMesh);
	//	}
	//}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

void Model::ReadMaterial(wstring file)
{
	file = L"../../_Textures/" + file + L".material";

	Xml::XMLDocument* document = new Xml::XMLDocument();
	Xml::XMLError error = document->LoadFile(String::ToString(file).c_str());
	assert(error == Xml::XML_SUCCESS);

	Xml::XMLElement* root = document->FirstChildElement();
	Xml::XMLElement* materialNode = root->FirstChildElement();

	do
	{
		Material* material = new Material();

		Xml::XMLElement* node = nullptr;

		node = materialNode->FirstChildElement();
		material->Name(String::ToWString(node->GetText()));

		wstring directory = Path::GetDirectoryName(file);
		String::Replace(&directory, L"../../_Textures", L"");

		wstring texture = L"";

		node = node->NextSiblingElement();
		texture = String::ToWString(node->GetText());
		if (texture.length() > 0)
			material->DiffuseMap(directory + texture);

		node = node->NextSiblingElement();
		texture = String::ToWString(node->GetText());
		if (texture.length() > 0)
			material->SpecularMap(directory + texture);

		node = node->NextSiblingElement();
		texture = String::ToWString(node->GetText());
		if (texture.length() > 0)
			material->NormalMap(directory + texture);

		D3DXCOLOR color;

		node = node->NextSiblingElement();
		color.r = node->FloatAttribute("R");
		color.g = node->FloatAttribute("G");
		color.b = node->FloatAttribute("B");
		color.a = node->FloatAttribute("A");
		material->Ambient(color);

		node = node->NextSiblingElement();
		color.r = node->FloatAttribute("R");
		color.g = node->FloatAttribute("G");
		color.b = node->FloatAttribute("B");
		color.a = node->FloatAttribute("A");
		material->Diffuse(color);

		node = node->NextSiblingElement();
		color.r = node->FloatAttribute("R");
		color.g = node->FloatAttribute("G");
		color.b = node->FloatAttribute("B");
		color.a = node->FloatAttribute("A");
		material->Specular(color);

		materials.push_back(material);

		materialNode = materialNode->NextSiblingElement();

	} while (materialNode != NULL);

	int a = 0;

	delete document;
}

void Model::ReadMesh(wstring file)
{
	file = L"../../_Models/" + file + L".mesh";

	BinaryReader* r = new BinaryReader();
	r->Open(file);

	UINT count = 0;

	count = r->UInt();
	for (UINT i = 0; i < count; i++)
	{
		ModelBone* bone = new ModelBone();

		bone->index = r->Int();
		bone->name = String::ToWString(r->String());
		bone->parentIndex = r->Int();
		bone->transform = r->Matrix();

		bones.push_back(bone);
	}

	count = r->UInt();
	for (UINT i = 0; i < count; i++)
	{
		ModelMesh* mesh = new ModelMesh();

		mesh->name = String::ToWString(r->String());
		mesh->boneIndex = r->Int();

		mesh->materialName = String::ToWString(r->String());

		//VertexData
		{
			UINT count = r->UInt();
			vector<Model::ModelVertex> vertices;
			vertices.assign(count, Model::ModelVertex());

			void* ptr = (void*)&vertices[0];
			r->BYTE(&ptr, sizeof(Model::ModelVertex)*count);

			mesh->vertices = new Model::ModelVertex[count];
			mesh->vertexCount = count;

			copy(
				vertices.begin(),
				vertices.end(),
				stdext::checked_array_iterator<Model::ModelVertex*>(mesh->vertices,count)
			);//subResource 생성 mesh->vertices로 vertices를복사
		}
		//IndexData
		{
			UINT count = r->UInt();

			vector<UINT> indices;
			indices.assign(count, UINT());

			void* ptr = (void*)&indices[0];
			r->BYTE(&ptr, sizeof(UINT)*count);

			mesh->indices = new UINT[count];
			mesh->indexCount = count;

			copy
			(
				indices.begin(),
				indices.end(),
				stdext::checked_array_iterator<UINT*>(mesh->indices, count)
			);//subResource 생성
		}
		meshes.push_back(mesh);
	}

	r->Close();

	delete r;

	BindBone();
	BindMesh();
}

void Model::ReadClip(wstring file)
{
	file = L"../../_Models/" + file + L".clip";

	BinaryReader* r = new BinaryReader();

	r->Open(file);

	ModelClip* clip = new ModelClip();

	clip->name = String::ToWString(r->String());
	clip->duration = r->Float();
	clip->frameRate = r->Float();
	clip->frameCount = r->UInt();

	UINT count = r->UInt();
	for (UINT i = 0; i < count; i++)
	{
		ModelKeyframe* keyframe = new ModelKeyframe();
		keyframe->BoneName = String::ToWString(r->String());

		UINT size = r->UInt();
		if (size > 0)
		{
			keyframe->Transforms.assign(size, ModelKeyframeData());

			void* ptr = (void*)&keyframe->Transforms[0];
			r->BYTE(&ptr, sizeof(ModelKeyframeData)*size);
		}

		clip->keyframeMap[keyframe->BoneName] = keyframe;
	}

	r->Close();
	delete r;

	clips.push_back(clip);
}

void Model::BindBone()
{
	root = bones[0];
	for (ModelBone* bone: bones)
	{
		if (bone->parentIndex > -1)
		{
			bone->parent = bones[bone->parentIndex];
			bone->parent->childs.push_back(bone);
		}
		else
			bone->parent = nullptr;
	}
}

void Model::BindMesh()
{
	for (auto mesh: meshes)
	{
		for (auto bone:bones)
		{
			if (mesh->boneIndex == bone->index)
			{
				mesh->bone = bone;
				break;
			}
		}
		mesh->Binding(this);
	}
}
