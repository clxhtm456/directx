#include "Framework.h"
#include "ModelMesh.h"

//-------------------------------------------------------------------------
//Model_Bone
//-------------------------------------------------------------------------

ModelBone::ModelBone()
{
}

ModelBone::~ModelBone()
{
}

//-------------------------------------------------------------------------
//ModelMesh
//-------------------------------------------------------------------------

ModelMesh::ModelMesh()
{
	boneBuffer = new ConstantBuffer(&boneDesc, sizeof(BoneDesc));
}

ModelMesh::~ModelMesh()
{
	SafeDelete(transform);
	SafeDelete(perFrame);

	SafeDeleteArray(vertices);
	SafeDeleteArray(indices);

	SafeDelete(vertexBuffer);
	SafeDelete(indexBuffer);

	SafeDelete(boneBuffer);
	SafeDelete(material);
}

void ModelMesh::Binding(Model* model)//여기는 아직 주석달지말기
{
	vertexBuffer = new VertexBuffer(vertices, vertexCount, sizeof(Model::ModelVertex));
	indexBuffer = new IndexBuffer(indices, indexCount);

	Material* srcMatrial = model->MaterialByName(materialName);

	material = new Material();
	material->Ambient(srcMatrial->Ambient());
	material->Diffuse(srcMatrial->Diffuse());
	material->Specular(srcMatrial->Specular());

	if (srcMatrial->DiffuseMap() != nullptr)
		material->DiffuseMap(srcMatrial->DiffuseMap()->GetFile());

	if (srcMatrial->SpecularMap() != nullptr)
		material->SpecularMap(srcMatrial->SpecularMap()->GetFile());

	if (srcMatrial->NormalMap() != nullptr)
		material->NormalMap(srcMatrial->NormalMap()->GetFile());
}

void ModelMesh::SetShader(Shader * shader)
{
	this->shader = shader;

	SafeDelete(transform);
	transform = new Transform(shader);
	SafeDelete(perFrame);
	perFrame = new PerFrame(shader);

	material->SetShader(shader);
	sBoneBuffer = shader->AsConstantBuffer("CB_Bone");

	sTransformsSRV = shader->AsSRV("TransformsMap");
}

void ModelMesh::Update()
{
	boneDesc.Index = boneIndex;

	perFrame->Update();
	transform->Update();
}

void ModelMesh::Render()
{
	boneBuffer->Apply();
	sBoneBuffer->SetConstantBuffer(boneBuffer->Buffer());

	perFrame->Render();
	transform->Render();
	material->Render();//material render 주석처리시 검은 실루엣만 나옴

	if (transformsSRV != nullptr)
	{
		sTransformsSRV->SetResource(transformsSRV);
	}
	vertexBuffer->Render();
	indexBuffer->Render();
	D3D::GetDC()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	shader->DrawIndexed(0, pass, indexCount);
}

void ModelMesh::Transforms(Matrix * transform)
{
	memcpy(boneDesc.Transforms,transform,sizeof(Matrix)*MAX_MODEL_TRANSFORMS);
}

void ModelMesh::SetTransform(Transform * transform)
{
	this->transform->Set(transform);
}

void ModelMesh::TransformsSRV(ID3D11ShaderResourceView * srv)
{
	transformsSRV = srv;
}
