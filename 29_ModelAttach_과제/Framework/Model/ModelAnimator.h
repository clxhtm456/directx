#pragma once

class ModelAnimator
{
public:
	ModelAnimator(Shader* shader);
	~ModelAnimator();

	void Update();
	void Render();
public:
	void ReadMaterial(wstring file);
	void ReadMesh(wstring file);
	void ReadClip(wstring file);
	UINT CurrentClip() { return (UINT)tweenDesc.Curr.Clip; }

	void PlayClip(UINT clip, float speed = 1.0f, float takeTime = 1.0f);

	Transform* GetTransform() { return transform; }
	Model* GetModel() { return model; }

	void Pass(UINT pass);
public:
	void CreateTexture();
	void CreateClipTransform(UINT index);

private:
	struct ClipTransform//cbuffer가 아닌 Texture로 GPU로 넘김
	{
		Matrix** Transform;

		ClipTransform()
		{
			Transform = new Matrix*[MAX_MODEL_KEYFRAMES];
			for (int i = 0; i < MAX_MODEL_KEYFRAMES; i++)
			{
				Transform[i] = new Matrix[MAX_MODEL_TRANSFORMS];
			}
		}

		~ClipTransform()
		{
			for (int i = 0; i < MAX_MODEL_KEYFRAMES; i++)
			{
				delete[] Transform[i];
			}
			delete[] Transform;
		}

	};
	ClipTransform* clipTransform = nullptr;
	ID3D11Texture2D* texture = nullptr;
	ID3D11ShaderResourceView* srv = nullptr;

private:
	struct KeyframeDesc
	{
		int Clip = 0;
		UINT CurrFrame = 0;
		UINT NextFrame = 0;

		float Time = 0.0f;
		float RunningTime = 0.0f;

		float Speed = 1.0f;
		Vector2 Padding;
	}/*keyframeDesc*/;

	struct TweenDesc
	{
		float TakeTime = 1.0f;
		float TweenTime = 0.0f;
		float RunningTime = 0.0f;
		float Padding;

		KeyframeDesc Curr;
		KeyframeDesc Next;

		TweenDesc()
		{
			Curr.Clip = 0;
			Next.Clip = -1;
		}
	}tweenDesc;

	ConstantBuffer* frameBuffer;
	ID3DX11EffectConstantBuffer* sFrameBuffer;
private:
	bool bRead = false;
	Shader* shader;
	Model* model;

	Transform* transform;
};