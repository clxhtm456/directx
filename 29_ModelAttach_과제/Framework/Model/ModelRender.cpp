#include "Framework.h"
#include "ModelRender.h"
#include "ModelMesh.h"

ModelRender::ModelRender(Shader * shader):
	shader(shader)
{
	model = new Model();
	transform = new Transform(shader);
}

ModelRender::~ModelRender()
{
	SafeDelete(model);
	SafeDelete(transform);
}

void ModelRender::Update()
{
	for (auto mesh : model->Meshes())
	{
		mesh->Update();
	}
}

void ModelRender::Render()
{
	for (auto mesh : model->Meshes())
	{
		mesh->SetTransform(transform);
		mesh->Render();
	}
}

void ModelRender::ReadMaterial(wstring file)
{
	model->ReadMaterial(file);
}

void ModelRender::ReadMesh(wstring file)
{
	model->ReadMesh(file);

	for (auto mesh : model->Meshes())
	{
		mesh->SetShader(shader);
	}

	UpdateTransform();
}

void ModelRender::Pass(UINT pass)
{
	for (auto mesh : model->Meshes())
	{
		mesh->Pass(pass);
	}
}

void ModelRender::UpdateTransform(ModelBone * bone, Matrix matrix)//위치 업데이트
{
	if (bone != nullptr)//위치업데이트는 Bone의 위치를 따라가기때문에 Bone업데이트가 선행됨
		UpdateBones(bone, matrix);

	for (UINT i = 0; i < model->BoneCount(); i++)
	{
		Matrix matrix = model->BoneByIndex(i)->Transform();

		transforms[i] = matrix;
	}//bone Transform추출

	for (auto mesh : model->Meshes())
	{
		mesh->Transforms(transforms);
	}//bone Transform mesh에 적용
}

void ModelRender::UpdateBones(ModelBone * bone, Matrix & matrix)
{
	Matrix temp = bone->Transform();
	bone->Transform(temp * matrix);
	//변경되는 위치의 Matrix 데이터를 bone의 Transform에 곱연산

	for (auto child : bone->Childs())
	{
		UpdateBones(child, matrix);
	}//이후 bone의 연결된 자식들도 마찬가지로 업데이트
}
