#include "Framework.h"
#include "DebugLine.h"

DebugLine* DebugLine::instance = nullptr;

void DebugLine::Create()
{
	assert(instance == nullptr);

	instance = new DebugLine();
}

void DebugLine::Delete()
{
	SafeDelete(instance);
}

DebugLine * DebugLine::Get()
{
	assert(instance != nullptr);
	
	return instance;
}

void DebugLine::RenderLine(Vector3 start, Vector3 end)
{
	RenderLine(start, end, Color(0, 1, 0, 1));
}

void DebugLine::RenderLine(Vector3 start, Vector3 end, Color rgb)
{
	vertices[drawCount].Color = rgb;
	vertices[drawCount++].Position = start;

	vertices[drawCount].Color = rgb;
	vertices[drawCount++].Position = end;
}

void DebugLine::Render()
{
	perFrame->Update();
	transform->Update();

	perFrame->Render();
	transform->Render();

	D3D11_MAPPED_SUBRESOURCE subResource;
	D3D::GetDC()->Map(vertexBuffer->Buffer(), 0, D3D11_MAP_WRITE_DISCARD, 0, &subResource);
	{
		memcpy(subResource.pData, vertices, sizeof(VertexColor)*MAX_LINE_VERTEX);
	}
	D3D::GetDC()->Unmap(vertexBuffer->Buffer(), 0);

	vertexBuffer->Render();
	D3D::GetDC()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);

	shader->Draw(0, 0, drawCount * 2);
	drawCount = 0;
	ZeroMemory(vertices, sizeof(VertexColor)*MAX_LINE_VERTEX);
}

DebugLine::DebugLine()
{
	shader = new Shader(L"25_DebugLine.fx");
	vertices = new VertexColor[MAX_LINE_VERTEX];
	ZeroMemory(vertices, sizeof(VertexColor)*MAX_LINE_VERTEX);

	vertexBuffer = new VertexBuffer(vertices, MAX_LINE_VERTEX, sizeof(VertexColor),0,true);

	perFrame = new PerFrame(shader);
	transform = new Transform(shader);
}

DebugLine::~DebugLine()
{
	delete perFrame;
	delete transform;

	delete shader;

}
